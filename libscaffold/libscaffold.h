/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * libscaffold.h
 * 
 * Copyright (C) 2001, 2002 Dave Camp
 * Copyright (C) 2003 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __LIBSCAFFOLD_H__
#define __LIBSCAFFOLD_H__

#include "scaffold-utils.h"
#include "scaffold-tool.h"
#include "scaffold-doc.h"
#include "scaffold-session.h"

#define SCAFFOLD_WINDOW_LOC_TOP       GNOME_Development_Shell_LOC_TOP
#define SCAFFOLD_WINDOW_LOC_BOTTOM    GNOME_Development_Shell_LOC_BOTTOM
#define SCAFFOLD_WINDOW_LOC_LEFT      GNOME_Development_Shell_LOC_LEFT
#define SCAFFOLD_WINDOW_LOC_RIGHT     GNOME_Development_Shell_LOC_RIGHT
#define SCAFFOLD_WINDOW_LOC_CENTER    GNOME_Development_Shell_LOC_CENTER
#define SCAFFOLD_WINDOW_LOC_FLOATING  GNOME_Development_Shell_LOC_FLOATING
#define SCAFFOLD_WINDOW_LOC_LAST      GNOME_Development_Shell_LOC_LAST

#endif /* __LIBSCAFFOLD_H__ */
