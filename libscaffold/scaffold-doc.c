/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * scaffold-doc.c
 * 
 * Copyright (C) 2001 Dirk Vangestel
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <ctype.h>
#include <libbonobo.h>
#include <gdl/GDL.h>
#include <libscaffold/scaffold-document-manager.h>
#include <libscaffold/scaffold-tool.h>
#include "scaffold-doc.h"

CORBA_Object
scaffold_get_editor_interface (ScaffoldDocument *document,
			       const char *interface)
{
	CORBA_Environment ev;
	CORBA_Object ret;
	Bonobo_Control ctrl;
	
	CORBA_exception_init (&ev);
	ctrl = scaffold_document_get_control (document);
	ret = Bonobo_Unknown_queryInterface (ctrl, interface, &ev);
	bonobo_object_release_unref (ctrl, &ev);

	CORBA_exception_free (&ev);

	return ret;
}

CORBA_Object
scaffold_get_current_editor_interface (ScaffoldTool *tool,
				       const char *interface)
{
	ScaffoldDocument *document;
	
	scaffold_shell_get (tool->shell, "DocumentManager::CurrentDocument",
			    SCAFFOLD_TYPE_DOCUMENT, &document, NULL);
	
	if (document) {
		return scaffold_get_editor_interface (document, interface);
	}	  

	return CORBA_OBJECT_NIL;
}

GNOME_Development_EditorBuffer
scaffold_get_editor_buffer (ScaffoldTool *tool)
{
	return scaffold_get_current_editor_interface (tool,
						      "IDL:GNOME/Development/EditorBuffer:1.0");
}

gboolean
scaffold_show_file (ScaffoldTool *tool, const char *uri)
{
	ScaffoldDocumentManager *docman;
	
	scaffold_shell_get (tool->shell, "DocumentManager",
			    SCAFFOLD_TYPE_DOCUMENT_MANAGER, &docman, NULL);

	if (docman) {
		ScaffoldDocument *doc;

		doc = scaffold_document_manager_get_document_for_uri (docman,
								      uri,
								      FALSE,
								      NULL);
		if (doc) {
			scaffold_document_manager_show_document (docman, doc);
			return TRUE;
		}
	}

	return FALSE;
}

static long
get_document_property_long (ScaffoldTool *tool, const char *name)
{
	CORBA_Environment ev;
	Bonobo_Control ctrl;
	Bonobo_PropertyBag bag;
	long prop;

	CORBA_exception_init (&ev);

	ctrl = scaffold_get_current_editor_interface (tool, 
						      "IDL:Bonobo/Control:1.0");

	if (CORBA_Object_is_nil (ctrl, &ev)) {
		CORBA_exception_free (&ev);
		return -1;
	}
		
	bag = Bonobo_Control_getProperties (ctrl, &ev);
	prop = bonobo_pbclient_get_long (bag, name, &ev);
	if (BONOBO_EX (&ev)) {
		prop = -1;
	}

	bonobo_object_release_unref (ctrl, &ev);
	bonobo_object_release_unref (bag, &ev);

	CORBA_exception_free (&ev);

	return prop;	
}

static void
set_document_property_long (ScaffoldTool *tool, const char *name, long val)
{
	Bonobo_Control ctrl;
	Bonobo_PropertyBag bag;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	ctrl = scaffold_get_current_editor_interface (tool, 
						      "IDL:Bonobo/Control:1.0");

	if (CORBA_Object_is_nil (ctrl, &ev)) {
		CORBA_exception_free (&ev);
		return;
	}
		
	bag = Bonobo_Control_getProperties (ctrl, &ev);

	bonobo_pbclient_set_long (bag, name, val, NULL);

	bonobo_object_release_unref (ctrl, &ev);
	bonobo_object_release_unref (bag, &ev);

	CORBA_exception_free (&ev);
}

long
scaffold_get_cursor_pos (ScaffoldTool *tool)
{
	return get_document_property_long (tool, "position");
}

void
scaffold_set_cursor_pos (ScaffoldTool *tool, long pos)
{
	set_document_property_long (tool, "position", pos);
}

static int 
scaffold_find_word_begin (ScaffoldTool *tool, 
			  GNOME_Development_EditorBuffer buffer,
			  int pos)
{
	CORBA_Environment ev;
	GNOME_Development_EditorBuffer_iobuf *iobuf;
	
	CORBA_exception_init (&ev);
	GNOME_Development_EditorBuffer_getChars (buffer, pos, 1, &iobuf, &ev);

	while (!BONOBO_EX (&ev) && iobuf->_length == 1 && 
	       (isalpha (iobuf->_buffer[0]) || iobuf->_buffer[0] == '_') &&
	       pos >= 0) {
		pos--;
		CORBA_free (iobuf);
		GNOME_Development_EditorBuffer_getChars (buffer, 
							 pos, 1, &iobuf, &ev);
	}

	pos++;
	
	if (!BONOBO_EX (&ev))
		CORBA_free (iobuf);

	CORBA_exception_free (&ev);

	return pos;
}

static int
scaffold_find_word_end (ScaffoldTool *tool, 
			GNOME_Development_EditorBuffer buffer,
			int pos)
{
	CORBA_Environment ev;
	GNOME_Development_EditorBuffer_iobuf *iobuf;
	long size;

	CORBA_exception_init (&ev);
	size = GNOME_Development_EditorBuffer_getLength (buffer, &ev);
	GNOME_Development_EditorBuffer_getChars (buffer, pos, 1, &iobuf, &ev);

	while (!BONOBO_EX (&ev) && iobuf->_length == 1 && 
	       (isalpha (iobuf->_buffer[0]) || iobuf->_buffer[0] == '_') &&
	       pos < size) {
		pos++;
		CORBA_free (iobuf);
		GNOME_Development_EditorBuffer_getChars (buffer, 
							 pos, 1, &iobuf, &ev);
	}
	
	if (!BONOBO_EX (&ev))
		CORBA_free (iobuf);

	CORBA_exception_free (&ev);

	return pos;
}

char *
scaffold_get_current_word (ScaffoldTool *tool)
{
	char *str = NULL;
	CORBA_Environment ev;
	GNOME_Development_EditorBuffer buffer;

	CORBA_exception_init (&ev);
	buffer = scaffold_get_editor_buffer (tool);
	/* FIXME: This could be a lot lot more efficient than it is. */
	if (!CORBA_Object_is_nil (buffer, &ev)) {
		int begin, end, pos;
 		GNOME_Development_EditorBuffer_iobuf *iobuf;

		pos = scaffold_get_cursor_pos (tool);

		begin = scaffold_find_word_begin (tool, buffer, pos);
		end = scaffold_find_word_end (tool, buffer, pos);

		if (end - begin > 1) {
			GNOME_Development_EditorBuffer_getChars (buffer, 
								 begin,
								 end - begin, 
								 &iobuf,
								 &ev);
			if (!BONOBO_EX (&ev)) {
				if (iobuf->_length > 0) {
					str = g_strndup (iobuf->_buffer, 
							 iobuf->_length);
				}
				CORBA_free (iobuf);
			}
		}
	}
	
	CORBA_exception_free (&ev);
	return str;
}

long
scaffold_get_line_num (ScaffoldTool *tool)
{
	return get_document_property_long (tool, "line_num");
}

void
scaffold_set_line_num (ScaffoldTool *tool, long pos)
{
	return set_document_property_long (tool, "line_num", pos);
}

void
scaffold_insert_text_at_pos (ScaffoldTool *tool, long pos, const char *text)
{
	GNOME_Development_EditorBuffer buffer;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	buffer = scaffold_get_editor_buffer (tool);
	if (!buffer) {
		CORBA_exception_free(&ev);
		return;
	}

	/* Insert text */
	GNOME_Development_EditorBuffer_insert (buffer, pos, text, &ev);

	scaffold_set_cursor_pos (tool, pos + strlen (text));

	bonobo_object_release_unref (buffer, &ev);

	CORBA_exception_free (&ev);
}

void
scaffold_insert_text_at_cursor (ScaffoldTool *tool, const char *text)
{
	long pos;

	pos = scaffold_get_cursor_pos (tool);

	scaffold_insert_text_at_pos (tool, pos, text);
}

void
scaffold_delete_text (ScaffoldTool *tool, long startpos, long endpos)
{
	GNOME_Development_EditorBuffer buffer;
	CORBA_Environment ev;
	long temp;

	CORBA_exception_init (&ev);

	buffer = scaffold_get_editor_buffer (tool);
	if (!buffer) {
		CORBA_exception_free (&ev);
		return;
	}

	if (startpos > endpos) {
		temp = startpos;
		startpos = endpos;
		endpos = temp;
	}

	GNOME_Development_EditorBuffer_delete (buffer, startpos, endpos - startpos, &ev);

	bonobo_object_release_unref (buffer, &ev);
	CORBA_exception_free(&ev);
}

long
scaffold_get_document_length (ScaffoldTool *tool)
{
	GNOME_Development_EditorBuffer buffer;
	CORBA_Environment ev;
	long length;

	CORBA_exception_init (&ev);

	buffer = scaffold_get_editor_buffer (tool);
	if (!buffer)
		return 0;

	length = GNOME_Development_EditorBuffer_getLength (buffer, &ev);

	bonobo_object_release_unref (buffer, &ev);

	CORBA_exception_free (&ev);

	return length;
}

char *
scaffold_get_document_chars (ScaffoldTool *tool, long start_pos, long end_pos)
{
	GNOME_Development_EditorBuffer	buffer;
	CORBA_Environment		ev;
	long				length;
	long				temp;
	GNOME_Development_EditorBuffer_iobuf*	buf;
	char*				result;

	if (start_pos > end_pos) {
		temp = start_pos;
		start_pos = end_pos;
		end_pos = temp;
	}

	CORBA_exception_init(&ev);

	buffer = scaffold_get_editor_buffer (tool);
	if (!buffer)
		return NULL;

	length = GNOME_Development_EditorBuffer_getLength (buffer, &ev);

	if (start_pos > length)
		return NULL;

	if (end_pos > length)
		end_pos = length;

	GNOME_Development_EditorBuffer_getChars (buffer, start_pos,
						 end_pos - start_pos, 
						 &buf, &ev);

	result = g_strndup (buf->_buffer, buf->_length);

	CORBA_free (buf);

	bonobo_object_release_unref (buffer, &ev);
	
	CORBA_exception_free (&ev);

	return result;
}

char *
scaffold_get_current_uri (ScaffoldTool *tool)
{
	ScaffoldDocument *document;

	scaffold_shell_get (tool->shell, 
			  "DocumentManager::CurrentDocument", 
			  SCAFFOLD_TYPE_DOCUMENT,
			  &document,
			  NULL);
	
	if (document) {
		return g_strdup (scaffold_document_get_uri (document));
	}

	return NULL;
}
