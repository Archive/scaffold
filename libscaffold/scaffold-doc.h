/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * scaffold-doc.h
 * 
 * Copyright (C) 2001 Dirk Vangestel
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __SCAFFOLD_DOC_H__
#define __SCAFFOLD_DOC_H__

#include <glib.h>
#include <gdl/GDL.h>
#include "scaffold-tool.h"
#include "scaffold-document.h"

G_BEGIN_DECLS

CORBA_Object scaffold_get_editor_interface (ScaffoldDocument *document,
					    const char *interface);
CORBA_Object scaffold_get_current_editor_interface (ScaffoldTool *tool, 
						    const char *interface);
GNOME_Development_EditorBuffer scaffold_get_editor_buffer (ScaffoldTool *tool);

gboolean scaffold_show_file             (ScaffoldTool *tool,
					 const char   *uri);
char    *scaffold_get_current_uri       (ScaffoldTool *tool);
char    *scaffold_get_current_word      (ScaffoldTool *tool);
long     scaffold_get_document_length   (ScaffoldTool *tool);
char    *scaffold_get_document_chars    (ScaffoldTool *tool,
					 long          start_pos,
					 long          end_pos);
long     scaffold_get_line_num          (ScaffoldTool *tool);
void     scaffold_set_line_num          (ScaffoldTool *tool,
					 long          line);
long     scaffold_get_cursor_pos        (ScaffoldTool *tool);
void     scaffold_set_cursor_pos        (ScaffoldTool *tool,
					 long          pos);
void     scaffold_insert_text_at_pos    (ScaffoldTool *tool,
					 long          pos,
					 const char   *text);
void     scaffold_insert_text_at_cursor (ScaffoldTool *tool,
					 const char   *text);
void     scaffold_delete_text           (ScaffoldTool *tool,
					 long          startpos,
					 long          endpos);

G_END_DECLS

#endif /* __SCAFFOLD_DOC_H__ */
