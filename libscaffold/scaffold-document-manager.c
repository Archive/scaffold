/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * scaffold-document-manager.c
 * 
 * Copyright (C) 2002 Dave Camp
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "scaffold-document-manager.h"
#include "scaffold-marshal.h"

ScaffoldDocument * 
scaffold_document_manager_new_document (ScaffoldDocumentManager *docman,
					const char *mime_type,
					GError *error)
{
	g_return_val_if_fail (docman != NULL, NULL);
	g_return_val_if_fail (SCAFFOLD_IS_DOCUMENT_MANAGER (docman), NULL);
	g_return_val_if_fail (mime_type != NULL, NULL);

	return SCAFFOLD_DOCUMENT_MANAGER_GET_IFACE (docman)->new_document
		(docman, mime_type, error);
}

ScaffoldDocument * 
scaffold_document_manager_open (ScaffoldDocumentManager *docman,
				const char *uri,
				GError *error)
{
	g_return_val_if_fail (docman != NULL, NULL);
	g_return_val_if_fail (SCAFFOLD_IS_DOCUMENT_MANAGER (docman), NULL);
	g_return_val_if_fail (uri != NULL, NULL);

	return SCAFFOLD_DOCUMENT_MANAGER_GET_IFACE (docman)->open (docman, 
								   uri, 
								   error);
}

void
scaffold_document_manager_close (ScaffoldDocumentManager *docman,
				 ScaffoldDocument *document,
				 GError *error)
{
	g_return_if_fail (docman != NULL);
	g_return_if_fail (SCAFFOLD_IS_DOCUMENT_MANAGER (docman));
	g_return_if_fail (document != NULL);
	g_return_if_fail (SCAFFOLD_IS_DOCUMENT (document));

	SCAFFOLD_DOCUMENT_MANAGER_GET_IFACE (docman)->close (docman, 
							     document, 
							     error);
}

void
scaffold_document_manager_close_all (ScaffoldDocumentManager *docman,
				     GError *error)
{
	g_return_if_fail (docman != NULL);
	g_return_if_fail (SCAFFOLD_IS_DOCUMENT_MANAGER (docman));

	SCAFFOLD_DOCUMENT_MANAGER_GET_IFACE (docman)->close_all (docman, error);
}

void
scaffold_document_manager_save_all (ScaffoldDocumentManager *docman,
				    GError *error)
{
	g_return_if_fail (docman != NULL);
	g_return_if_fail (SCAFFOLD_IS_DOCUMENT_MANAGER (docman));

	SCAFFOLD_DOCUMENT_MANAGER_GET_IFACE (docman)->save_all (docman, 
								error);
}

int
scaffold_document_manager_num_documents (ScaffoldDocumentManager *docman)
{
	g_return_val_if_fail (docman != NULL, -1);
	g_return_val_if_fail (SCAFFOLD_IS_DOCUMENT_MANAGER (docman), -1);

	return SCAFFOLD_DOCUMENT_MANAGER_GET_IFACE (docman)->num_documents (docman);
}

ScaffoldDocument * 
scaffold_document_manager_get_nth_document (ScaffoldDocumentManager *docman,
					  int i)
{
	g_return_val_if_fail (docman != NULL, NULL);
	g_return_val_if_fail (SCAFFOLD_IS_DOCUMENT_MANAGER (docman), NULL);

	return SCAFFOLD_DOCUMENT_MANAGER_GET_IFACE (docman)->get_nth_document
		(docman, i);
}

ScaffoldDocument * 
scaffold_document_manager_get_document_for_uri (ScaffoldDocumentManager *docman,
						const char *uri,
						gboolean existing_only,
						GError *error)
{
	g_return_val_if_fail (docman != NULL, NULL);
	g_return_val_if_fail (SCAFFOLD_IS_DOCUMENT_MANAGER (docman), NULL);
	g_return_val_if_fail (uri != NULL, NULL);

	return SCAFFOLD_DOCUMENT_MANAGER_GET_IFACE (docman)->get_document_for_uri
		(docman, uri, existing_only, error);
}

void
scaffold_document_manager_show_document (ScaffoldDocumentManager *docman,
					 ScaffoldDocument *document)
{
	g_return_if_fail (docman != NULL);
	g_return_if_fail (SCAFFOLD_IS_DOCUMENT_MANAGER (docman));
	g_return_if_fail (document != NULL);
	g_return_if_fail (SCAFFOLD_IS_DOCUMENT (document));

	SCAFFOLD_DOCUMENT_MANAGER_GET_IFACE (docman)->show_document (docman, 
								     document);
}

ScaffoldDocument * 
scaffold_document_manager_get_current_document (ScaffoldDocumentManager *docman)
{
	g_return_val_if_fail (docman != NULL, NULL);
	g_return_val_if_fail (SCAFFOLD_IS_DOCUMENT_MANAGER (docman), NULL);

	return SCAFFOLD_DOCUMENT_MANAGER_GET_IFACE (docman)->get_current_document
		(docman);
	
}

static void
scaffold_document_manager_base_init (gpointer gclass)
{
	static gboolean initialized = FALSE;
	
	if (!initialized) {
		g_signal_new ("current_document_changed",
			      G_TYPE_FROM_CLASS (gclass),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (ScaffoldDocumentManagerIface, 
					       current_document_changed),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__OBJECT,
			      G_TYPE_NONE, 1,
			      SCAFFOLD_TYPE_DOCUMENT);

		initialized = TRUE;
	}
}

GType
scaffold_document_manager_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (ScaffoldDocumentManagerIface),
			scaffold_document_manager_base_init,
			NULL, 
			NULL,
			NULL,
			NULL,
			0,
			0,
			NULL
		};
		
		type = g_type_register_static (G_TYPE_INTERFACE, 
					       "ScaffoldDocumentManager", 
					       &info, 0);
		
		g_type_interface_add_prerequisite (type, G_TYPE_OBJECT);
	}
	
	return type;			
}
