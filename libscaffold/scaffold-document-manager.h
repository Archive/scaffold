/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * scaffold-document-manager.h
 * 
 * Copyright (C) 2002 Dave Camp
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __SCAFFOLD_DOCUMENT_MANAGER_H__
#define __SCAFFOLD_DOCUMENT_MANAGER_H__

#include <glib-object.h>
#include "scaffold-document.h"

G_BEGIN_DECLS

#define SCAFFOLD_TYPE_DOCUMENT_MANAGER            (scaffold_document_manager_get_type ())
#define SCAFFOLD_DOCUMENT_MANAGER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), SCAFFOLD_TYPE_DOCUMENT_MANAGER, ScaffoldDocumentManager))
#define SCAFFOLD_IS_DOCUMENT_MANAGER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), SCAFFOLD_TYPE_DOCUMENT_MANAGER))
#define SCAFFOLD_DOCUMENT_MANAGER_GET_IFACE(obj)  (G_TYPE_INSTANCE_GET_INTERFACE ((obj), SCAFFOLD_TYPE_DOCUMENT_MANAGER, ScaffoldDocumentManagerIface))

typedef struct _ScaffoldDocumentManager      ScaffoldDocumentManager;
typedef struct _ScaffoldDocumentManagerIface ScaffoldDocumentManagerIface;

struct _ScaffoldDocumentManagerIface {
	GTypeInterface g_iface;
	
	/* Signals */
	void (*current_document_changed) (ScaffoldDocumentManager *docman, 
					  ScaffoldDocument *document);

	/* Virtual Table */
	ScaffoldDocument *(*new_document)         (ScaffoldDocumentManager *docman,
						   const char              *mime_type,
						   GError                  *error);
	ScaffoldDocument *(*open)                 (ScaffoldDocumentManager *docman,
						   const char              *uri,
						   GError                  *error);
	void              (*close)                (ScaffoldDocumentManager *docman,
						   ScaffoldDocument        *document,
						   GError                  *error);
	void              (*close_all)            (ScaffoldDocumentManager *docman,
						   GError                  *error);
	void              (*save_all)             (ScaffoldDocumentManager *docman,
						   GError                  *error);
	int               (*num_documents)        (ScaffoldDocumentManager *docman);
	ScaffoldDocument *(*get_nth_document)     (ScaffoldDocumentManager *docman,
						   int                      n);
	ScaffoldDocument *(*get_document_for_uri) (ScaffoldDocumentManager *docman,
						   const char              *uri,
						   gboolean                 existing_only,
						   GError                  *error);
	void              (*show_document)        (ScaffoldDocumentManager *docman,
						   ScaffoldDocument        *document);
	ScaffoldDocument *(*get_current_document) (ScaffoldDocumentManager *docman);
};

GType             scaffold_document_manager_get_type             (void);
ScaffoldDocument *scaffold_document_manager_new_document         (ScaffoldDocumentManager *docman,
								  const char              *mime_type,
								  GError                  *error);
ScaffoldDocument *scaffold_document_manager_open                 (ScaffoldDocumentManager *docman,
								  const char              *uri,
								  GError                  *error);
void              scaffold_document_manager_close                (ScaffoldDocumentManager *docman,
								  ScaffoldDocument        *document,
								  GError                  *error);
void              scaffold_document_manager_close_all            (ScaffoldDocumentManager *docman,
								  GError                  *error);
void              scaffold_document_manager_save_all             (ScaffoldDocumentManager *docman,
								  GError                  *error);
int               scaffold_document_manager_num_documents        (ScaffoldDocumentManager *docman);
ScaffoldDocument *scaffold_document_manager_get_nth_document     (ScaffoldDocumentManager *docman,
								  int                      n);
ScaffoldDocument *scaffold_document_manager_get_document_for_uri (ScaffoldDocumentManager *docman,
								  const char              *uri,
								  gboolean                 existing_only,
								  GError                  *error);
void              scaffold_document_manager_show_document        (ScaffoldDocumentManager *docman,
								  ScaffoldDocument        *document);
ScaffoldDocument *scaffold_document_manager_get_current_document (ScaffoldDocumentManager *docman);


G_END_DECLS

#endif /* __SCAFFOLD_DOCUMENT_MANAGER_H__ */
