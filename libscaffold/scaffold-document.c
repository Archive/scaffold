/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * scaffold-document.c
 * 
 * Copyright (C) 2002 Dave Camp
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "scaffold-document.h"
#include "scaffold-marshal.h"

void 
scaffold_document_save (ScaffoldDocument *document,
			GError *error)
{
	g_return_if_fail (document != NULL);
	g_return_if_fail (SCAFFOLD_IS_DOCUMENT (document));

	SCAFFOLD_DOCUMENT_GET_IFACE (document)->save (document, error);
}

void 
scaffold_document_save_as (ScaffoldDocument *document,
			   const char *uri,
			   GError *error)
{
	g_return_if_fail (document != NULL);
	g_return_if_fail (SCAFFOLD_IS_DOCUMENT (document));
	g_return_if_fail (uri != NULL);

	SCAFFOLD_DOCUMENT_GET_IFACE (document)->save (document, error);
}

const char *
scaffold_document_get_uri (ScaffoldDocument *document)
{
	g_return_val_if_fail (document != NULL, NULL);
	g_return_val_if_fail (SCAFFOLD_IS_DOCUMENT (document), NULL);

	return SCAFFOLD_DOCUMENT_GET_IFACE (document)->get_uri (document);
}

const char *
scaffold_document_get_mime_type (ScaffoldDocument *document)
{
	g_return_val_if_fail (document != NULL, NULL);
	g_return_val_if_fail (SCAFFOLD_IS_DOCUMENT (document), NULL);

	return SCAFFOLD_DOCUMENT_GET_IFACE (document)->get_mime_type (document);
}

Bonobo_Control
scaffold_document_get_control (ScaffoldDocument *document)
{
	g_return_val_if_fail (document != NULL, CORBA_OBJECT_NIL);
	g_return_val_if_fail (SCAFFOLD_IS_DOCUMENT (document), CORBA_OBJECT_NIL);

	return SCAFFOLD_DOCUMENT_GET_IFACE (document)->get_control (document);
}

static void
scaffold_document_base_init (gpointer gclass)
{
	static gboolean initialized = FALSE;
	
	if (!initialized) {
		g_signal_new ("modified",
			      SCAFFOLD_TYPE_DOCUMENT,
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (ScaffoldDocumentIface, 
					       modified),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE, 0);

		g_signal_new ("unmodified",
			      SCAFFOLD_TYPE_DOCUMENT,
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (ScaffoldDocumentIface, 
					       unmodified),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE, 0);
		
		g_signal_new ("uri_changed",
			      SCAFFOLD_TYPE_DOCUMENT,
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (ScaffoldDocumentIface, 
					       uri_changed),
			      NULL, NULL,
			      scaffold_marshal_VOID__STRING,
			      G_TYPE_NONE, 1,
			      G_TYPE_STRING);
		initialized = TRUE;
	}
}

GType
scaffold_document_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (ScaffoldDocumentIface),
			scaffold_document_base_init,
			NULL, 
			NULL,
			NULL,
			NULL,
			0,
			0,
			NULL
		};
		
		type = g_type_register_static (G_TYPE_INTERFACE, 
					       "ScaffoldDocument", 
					       &info, 0);
		
		g_type_interface_add_prerequisite (type, G_TYPE_OBJECT);
	}
	
	return type;			
}
