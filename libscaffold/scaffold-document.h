/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * scaffold-document.h
 * 
 * Copyright (C) 2002 Dave Camp
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __SCAFFOLD_DOCUMENT_H__
#define __SCAFFOLD_DOCUMENT_H__

#include <glib-object.h>
#include <bonobo/bonobo-control.h>

G_BEGIN_DECLS

#define SCAFFOLD_TYPE_DOCUMENT            (scaffold_document_get_type ())
#define SCAFFOLD_DOCUMENT(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), SCAFFOLD_TYPE_DOCUMENT, ScaffoldDocument))
#define SCAFFOLD_IS_DOCUMENT(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), SCAFFOLD_TYPE_DOCUMENT))
#define SCAFFOLD_DOCUMENT_GET_IFACE(obj)  (G_TYPE_INSTANCE_GET_INTERFACE ((obj), SCAFFOLD_TYPE_DOCUMENT, ScaffoldDocumentIface))

typedef struct _ScaffoldDocument      ScaffoldDocument;
typedef struct _ScaffoldDocumentIface ScaffoldDocumentIface;

struct _ScaffoldDocumentIface {
	GTypeInterface g_iface;
	
	/* Signals */
	void            (*modified)      (ScaffoldDocument *document);
	void            (*unmodified)    (ScaffoldDocument *document);

	void            (*uri_changed)   (ScaffoldDocument *document,
					  const char       *uri);

	/* Virtual Table */
	void            (*save)          (ScaffoldDocument *document, 
				          GError           *error);
	void            (*save_as)       (ScaffoldDocument *document, 
				          const char       *filename,
				          GError           *error);
	const char     *(*get_uri)       (ScaffoldDocument *document);
	const char     *(*get_mime_type) (ScaffoldDocument *document);
	Bonobo_Control  (*get_control)   (ScaffoldDocument *document);
};

GType          scaffold_document_get_type      (void);

void           scaffold_document_save          (ScaffoldDocument *document,
						GError           *error);
void           scaffold_document_save_as       (ScaffoldDocument *document,
						const char       *uri,
						GError           *error);

const char    *scaffold_document_get_uri       (ScaffoldDocument *document);
const char    *scaffold_document_get_mime_type (ScaffoldDocument *document);
Bonobo_Control scaffold_document_get_control   (ScaffoldDocument *document);

G_END_DECLS

#endif /* __SCAFFOLD_DOCUMENT_H__ */
