/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * scaffold-session.c
 * 
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <sys/stat.h>
#include <libgnome/gnome-i18n.h>
#include <libgnome/gnome-macros.h>
#include <libgnome/gnome-util.h>
#include "scaffold-session.h"
#include "scaffold-utils.h"

enum {
	PROP_0,
	PROP_SESSION_NAME
};

struct _ScaffoldSessionPrivate {
	char *name;
	GHashTable *properties;
	GHashTable *groups;
};

static void dispose (GObject *object);
static void get_property (GObject      *object,
			  guint         prop_id,
			  GValue       *value,
			  GParamSpec   *pspec);
static void set_property (GObject      *object,
			  guint         prop_id,
			  const GValue *value,
			  GParamSpec   *pspec);

static void load_session (ScaffoldSession *session);

/* Boilerplate. */
GNOME_CLASS_BOILERPLATE (ScaffoldSession, scaffold_session, GObject, G_TYPE_OBJECT);

/* Private functions. */
static void
scaffold_session_class_init (ScaffoldSessionClass *klass) 
{
	GObjectClass *g_object_class;

	g_object_class = G_OBJECT_CLASS (klass);
	parent_class = g_type_class_peek_parent (klass);

	g_object_class->dispose = dispose;
	g_object_class->get_property = get_property;
	g_object_class->set_property = set_property;

	g_object_class_install_property 
		(g_object_class, PROP_SESSION_NAME,
		 g_param_spec_string ("name", 
				      _("Session name"),
				      _("The session name"),
				      "",
				      G_PARAM_READWRITE));
}

static void
scaffold_session_instance_init (ScaffoldSession *session)
{
	ScaffoldSessionPrivate *priv;

	priv = g_new0 (ScaffoldSessionPrivate, 1);
	session->priv = priv;

	priv->name = NULL;
	priv->properties = g_hash_table_new_full (g_str_hash, g_str_equal,
						  g_free, g_free);
	priv->groups = g_hash_table_new_full (g_str_hash, g_str_equal,
					      g_free, (GDestroyNotify) xmlFreeNode);
}

static void
dispose (GObject *object)
{
	ScaffoldSession *session = SCAFFOLD_SESSION (object);

	if (session->priv) {
		if (session->priv->name)
			g_free (session->priv->name);
		g_hash_table_destroy (session->priv->properties);
		g_hash_table_destroy (session->priv->groups);
		g_free (session->priv);
		session->priv = NULL;
	}
}

static void
get_property (GObject    *object,
	      guint       prop_id,
	      GValue     *value,
	      GParamSpec *pspec)
{
	ScaffoldSession *session = SCAFFOLD_SESSION (object);

	switch (prop_id) {
	case PROP_SESSION_NAME:
		g_value_set_string (value, session->priv->name);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void
set_property (GObject      *object,
	      guint         prop_id,
	      const GValue *value,
	      GParamSpec   *pspec)
{
	ScaffoldSession *session = SCAFFOLD_SESSION (object);

	switch (prop_id) {
	case PROP_SESSION_NAME:
		if (session->priv->name) {
			g_free (session->priv->name);
			session->priv->name = NULL;
		}
		session->priv->name = g_strdup (g_value_get_string (value));
		load_session (session);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}   
}

static void
parse_node (ScaffoldSession *session,
	    xmlNodePtr       node)
{
	ScaffoldSessionPrivate *priv = session->priv;
	xmlChar *name, *value;

	if (!xmlStrcmp ("property", (const xmlChar *) node->name)) {
		name = xmlGetProp (node, "name");
		value = xmlGetProp (node, "value");
		g_hash_table_insert (priv->properties, name, value);
	} else {
		g_hash_table_insert (priv->groups, g_strdup (node->name), node);
		xmlUnlinkNode (node);
	}
}

static void
load_session (ScaffoldSession *session)
{
	ScaffoldSessionPrivate *priv = session->priv;
	char *dir, *filename;
	xmlDocPtr doc;
	xmlNodePtr root, node;

	/* Check if ~/.scaffold exists and create if if not. */
	dir = gnome_util_prepend_user_home (".scaffold");
	if (!g_file_test (dir, G_FILE_TEST_IS_DIR)) {
		mkdir (dir, 0755);
		if (!g_file_test (dir, G_FILE_TEST_IS_DIR)) {
			g_free (dir);
			scaffold_dialog_error (_("Could not create ~/.scaffold directory."));
			return;
		}
	}
	g_free (dir);

	/* Check if ~/.scaffold/sessions/ exists and create it if not. */
	dir = gnome_util_prepend_user_home (".scaffold/sessions/");
	if (!g_file_test (dir, G_FILE_TEST_IS_DIR)) {
		mkdir (dir, 0755);
		if (!g_file_test (dir, G_FILE_TEST_IS_DIR)) {
			g_free (dir);
			scaffold_dialog_error (_("Could not create sessions directory."));
			return;
		}
	}

	filename = g_strconcat (dir, "/", priv->name, ".xml", NULL);
	g_free (dir);

	/* Check if session file exists. Fallback to default session if not.
	 * If default.xml doesn't exist either, just create an empty xml doc.*/
	if (!g_file_test (filename, G_FILE_TEST_EXISTS)) {
		if (!strcmp (priv->name, "default")) {
			g_free (filename);
			return;
		} else {
			char *msg = g_strdup_printf (_("Session '%s' does not exist, using default session instead."),
						     priv->name);
			scaffold_dialog_error (msg);
			g_free (msg);
			g_free (priv->name);
			priv->name = g_strdup ("default");
			load_session (session);
		}
	}

	/* Load the session file. */
	doc = xmlParseFile (filename);
	if (!doc) {
		char *msg = g_strdup_printf (_("Unable to parse session file '%s'"),
					     filename);
		scaffold_dialog_error (msg);
		g_free (filename);
		g_free (msg);
		return;
	}
	g_free (filename);

	/* Parse nodes. */
	root = xmlDocGetRootElement (doc);
	for (node = root->xmlChildrenNode; node; node = node->next) {
		if (node->type == XML_ELEMENT_NODE)
			parse_node (session, node);
	}

	xmlFreeDoc (doc);
}

static void
foreach_property (gpointer key,
		  gpointer value,
		  gpointer user_data)
{
	xmlNodePtr node = user_data;
	xmlNodePtr child;

	child = xmlNewChild (node, NULL, "property", NULL);
	xmlSetProp (child, "name", (xmlChar *)key);
	xmlSetProp (child, "value", (xmlChar *)value);
}

static void
foreach_group (gpointer key,
	       gpointer value,
	       gpointer user_data)
{
	xmlNodePtr node = user_data;
	xmlNodePtr group = value;

	xmlAddChild (node, xmlCopyNode (group, TRUE));
}

/* ----------------------------------------------------------------------
 * Public interface 
 * ---------------------------------------------------------------------- */

ScaffoldSession *
scaffold_session_new (const char *session_name)
{
	ScaffoldSession *session;

	session = SCAFFOLD_SESSION (g_object_new (SCAFFOLD_TYPE_SESSION,
						  "name", session_name, NULL));

	return session;
}

void
scaffold_session_save (ScaffoldSession *session)
{
	ScaffoldSessionPrivate *priv = session->priv;
	char *dir, *filename;
	xmlDocPtr doc;

	/* Create new xml doc. */
	doc = xmlNewDoc ("1.0");
	doc->children = xmlNewDocNode (doc, NULL, "scaffold-session", NULL);
	xmlSetProp (doc->children, "name", priv->name);

	/* Save properties & groups.. */
	g_hash_table_foreach (priv->properties, foreach_property, doc->children);
	g_hash_table_foreach (priv->groups, foreach_group, doc->children);

	/* Save to file. */
	dir = gnome_util_prepend_user_home (".scaffold/sessions/");
	filename = g_strconcat (dir, "/", priv->name, ".xml", NULL);
	g_free (dir);
	xmlKeepBlanksDefault (0);
	xmlSaveFormatFile (filename, doc, TRUE);
	g_free (filename);
	xmlFreeDoc (doc);
}

void
scaffold_session_get (ScaffoldSession *session,
		      const char      *first_name,
		      char           **first_value,
		      ...)
{
	va_list var_args;

	g_return_if_fail (SCAFFOLD_IS_SESSION (session));
	g_return_if_fail (first_name != NULL);
	g_return_if_fail (first_value != NULL);

	va_start (var_args, first_value);
	scaffold_session_get_valist (session, first_name, first_value, var_args);
	va_end (var_args);
}

void
scaffold_session_get_valist (ScaffoldSession *session,
			     const char      *first_name,
			     char           **first_value,
			     va_list          var_args)
{
	const char *name;
	char **value;

	g_return_if_fail (SCAFFOLD_IS_SESSION (session));
	g_return_if_fail (first_name != NULL);
	g_return_if_fail (first_value != NULL);

	name = first_name;
	value = first_value;

	while (name) {
		*value = g_hash_table_lookup (session->priv->properties, name);

		name = va_arg (var_args, char *);
		if (name)
			value = va_arg (var_args, char **);
	}
}

void
scaffold_session_set (ScaffoldSession *session,
		      const char      *first_name,
		      const char      *first_value,
		      ...)
{
	va_list var_args;

	g_return_if_fail (SCAFFOLD_IS_SESSION (session));
	g_return_if_fail (first_name != NULL);

	va_start (var_args, first_value);
	scaffold_session_set_valist (session, first_name, first_value, var_args);
	va_end (var_args);
}

void
scaffold_session_set_valist (ScaffoldSession *session,
			     const char      *first_name,
			     const char      *first_value,
			     va_list          var_args)
{
	ScaffoldSessionPrivate *priv;
	const char *name;
	const char *value;

	g_return_if_fail (SCAFFOLD_IS_SESSION (session));
	g_return_if_fail (first_name != NULL);

	priv = session->priv;
	name = first_name;
	value = first_value;

	while (name) {
		if (value == NULL) {
			g_hash_table_remove (priv->properties, name);
		} else if (g_hash_table_lookup (priv->properties, name)) {
			g_hash_table_replace (priv->properties,
					      g_strdup (name),
					      g_strdup (value));
		} else {
			g_hash_table_insert (priv->properties,
					     g_strdup (name),
					     g_strdup (value));
		}

		name = va_arg (var_args, char *);
		if (name)
			value = va_arg (var_args, char *);
	}
}

void
scaffold_session_remove (ScaffoldSession *session,
		         const char      *name)
{
	g_return_if_fail (SCAFFOLD_IS_SESSION (session));
	g_return_if_fail (name != NULL);

	g_hash_table_remove (session->priv->properties, name);
}

void
scaffold_session_set_group (ScaffoldSession *session,
			    xmlNodePtr       group)
{
	g_return_if_fail (SCAFFOLD_IS_SESSION (session));
	g_return_if_fail (group != NULL);

	/* <property> is not a valid group name. */
	if (!xmlStrcmp (group->name, (const xmlChar *)"property")) {
		g_warning ("'property' is a restricted name in the session XML");
		return;
	}

	/* Check if such a group already exists and remove it. */
	if (g_hash_table_lookup (session->priv->groups, group->name)) {
		g_hash_table_remove (session->priv->groups, group->name); 
	}

	g_hash_table_insert (session->priv->groups,
			     g_strdup (group->name), group);
}

xmlNodePtr
scaffold_session_get_group (ScaffoldSession *session,
			    const char      *name)
{
	g_return_val_if_fail (SCAFFOLD_IS_SESSION (session), NULL);
	g_return_val_if_fail (name != NULL, NULL);

	return (xmlNodePtr)g_hash_table_lookup (session->priv->groups, name);
}

void
scaffold_session_remove_group (ScaffoldSession *session,
			       const char      *name)
{
	g_return_if_fail (SCAFFOLD_IS_SESSION (session));
	g_return_if_fail (name != NULL);

	g_hash_table_remove (session->priv->groups, name);
}
