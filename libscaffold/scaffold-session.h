/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * scaffold-session.h
 *
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __SCAFFOLD_SESSION_H__
#define __SCAFFOLD_SESSION_H__

#include <glib-object.h>
#include <libxml/tree.h>

G_BEGIN_DECLS

#define SCAFFOLD_TYPE_SESSION		 (scaffold_session_get_type ())
#define SCAFFOLD_SESSION(obj)		 (G_TYPE_CHECK_INSTANCE_CAST ((obj), SCAFFOLD_TYPE_SESSION, ScaffoldSession))
#define SCAFFOLD_SESSION_CLASS(obj)	 (G_TYPE_CHECK_CLASS_CAST ((klass), SCAFFOLD_TYPE_SESSION, ScaffoldSessionClass))
#define SCAFFOLD_IS_SESSION(obj)	 (G_TYPE_CHECK_INSTANCE_TYPE ((obj), SCAFFOLD_TYPE_SESSION))
#define SCAFFOLD_IS_SESSION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((obj), SCAFFOLD_TYPE_SESSION))
#define SCAFFOLD_SESSION_GET_CLASS(obj)	 (G_TYPE_INSTANCE_GET_CLASS ((obj), SCAFFOLD_TYPE_SESSION, ScaffoldSessionClass))

typedef struct _ScaffoldSession		ScaffoldSession;
typedef struct _ScaffoldSessionClass	ScaffoldSessionClass;
typedef struct _ScaffoldSessionPrivate	ScaffoldSessionPrivate;

struct _ScaffoldSession {
	GObject parent;

	ScaffoldSessionPrivate *priv;
};

struct _ScaffoldSessionClass {
	GObjectClass parent_class;
};

/* Creation. */
GType            scaffold_session_get_type     (void);
ScaffoldSession *scaffold_session_new          (const char      *session_name);

void             scaffold_session_save         (ScaffoldSession *session);

/* One or more values. */
void             scaffold_session_get          (ScaffoldSession *session,
						const char      *fist_name,
						char           **fist_value,
						...);
void             scaffold_session_get_valist   (ScaffoldSession *session,
						const char      *first_name,
						char           **first_value,
						va_list          var_args);
void             scaffold_session_set          (ScaffoldSession *session,
						const char      *first_name,
						const char      *first_value,
						...);
void             scaffold_session_set_valist   (ScaffoldSession *session,
						const char      *first_name,
						const char      *first_value,
						va_list          var_args);
void             scaffold_session_remove       (ScaffoldSession *session,
						const char      *name);

/* Groups. */
void             scaffold_session_set_group    (ScaffoldSession *session,
						xmlNodePtr       group);
xmlNodePtr       scaffold_session_get_group    (ScaffoldSession *session,
						const char      *group);
void             scaffold_session_remove_group (ScaffoldSession *session,
						const char      *name);

G_END_DECLS

#endif /* __SCAFFOLD_SESSION_H__ */
