/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * scaffold-shell.c
 * 
 * Copyright (C) 2001, 2002 Dave Camp
 * Copyright (C) 2003 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <bonobo/bonobo-control-frame.h>
#include <bonobo/bonobo-widget.h>
#include <bonobo/bonobo-window.h>
#include <gobject/gvaluecollector.h>
#include "scaffold-marshal.h"
#include "scaffold-shell.h"

GQuark 
scaffold_shell_error_quark (void)
{
	static GQuark quark = 0;
	
	if (quark == 0) {
		quark = g_quark_from_static_string ("scaffold-shell-quark");
	}
	
	return quark;
}

void
scaffold_shell_add_widget (ScaffoldShell *shell,
			   GtkWidget *widget,
			   const char *name,
			   const char *title,
			   GError **error)
{
	g_return_if_fail (shell != NULL);
	g_return_if_fail (SCAFFOLD_IS_SHELL (shell));
	g_return_if_fail (widget != NULL);
	g_return_if_fail (GTK_IS_WIDGET (widget));
	g_return_if_fail (name != NULL);
	g_return_if_fail (title != NULL);

	SCAFFOLD_SHELL_GET_IFACE (shell)->add_widget (shell, widget, name,
						    title, error);
}

void
scaffold_shell_add_control (ScaffoldShell   *shell,
			    Bonobo_Control ctrl,
			    const char    *name,
			    const char    *title,
			    GError       **error)
{
	GtkWidget *widget;
	BonoboControlFrame *frame;
	
	g_return_if_fail (shell != NULL);
	g_return_if_fail (SCAFFOLD_IS_SHELL (shell));
	g_return_if_fail (ctrl != CORBA_OBJECT_NIL);
	g_return_if_fail (name != NULL);
	g_return_if_fail (title != NULL);	

	widget = bonobo_widget_new_control_from_objref (ctrl,
							BONOBO_OBJREF (bonobo_window_get_ui_container (BONOBO_WINDOW (shell))));
	frame = bonobo_widget_get_control_frame (BONOBO_WIDGET (widget));
	
	bonobo_control_frame_set_autoactivate (frame, FALSE);
	bonobo_control_frame_control_activate (frame);
	
	gtk_widget_show (widget);

	scaffold_shell_add_widget (shell, widget, name, title, error);
}

void
scaffold_shell_add_preferences (ScaffoldShell *shell,
				GtkWidget *page,
				const char *name,
				const char *category,
				const char *title,
				GError **error)
{
	g_return_if_fail (shell != NULL);
	g_return_if_fail (SCAFFOLD_IS_SHELL (shell));
	g_return_if_fail (page != NULL);
	g_return_if_fail (GTK_IS_WIDGET (page));
	g_return_if_fail (category != NULL);
	g_return_if_fail (name != NULL);
	g_return_if_fail (title != NULL);
	g_return_if_fail (title != NULL);

	SCAFFOLD_SHELL_GET_IFACE (shell)->add_preferences (shell, 
							   page, 
							   name,
							   category,
							   title, 
							   error);
}

void
scaffold_shell_add_value (ScaffoldShell *shell,
			  const char *name,
			  const GValue *value,
			  GError **error)
{
	g_return_if_fail (shell != NULL);
	g_return_if_fail (SCAFFOLD_IS_SHELL (shell));
	g_return_if_fail (name != NULL);
	g_return_if_fail (value != NULL);

	SCAFFOLD_SHELL_GET_IFACE (shell)->add_value (shell, name, value, error);
}

void
scaffold_shell_add_valist (ScaffoldShell *shell,
			   const char *first_name,
			   GType first_type,
			   va_list var_args)
{
	const char *name;
	GType type;

	g_return_if_fail (shell != NULL);
	g_return_if_fail (SCAFFOLD_IS_SHELL (shell));
	g_return_if_fail (first_name != NULL);
	
	name = first_name;
	type = first_type;

	while (name) {
		GValue value = {0, };
		GError *err = NULL;
		char *error;
		
		g_value_init (&value, type);
		
		G_VALUE_COLLECT (&value, var_args, 0, &error);

		if (error){
			g_warning ("%s: %s", G_STRLOC, error);
			g_free (error);
			break;
		}
		
		scaffold_shell_add_value (shell, name, &value, &err);

		g_value_unset (&value);

		if (err) {
			g_warning ("Could not set value: %s\n", err->message);
			g_error_free (err);
			break;
		}

		name = va_arg (var_args, char *);
		if (name) {
			type = va_arg (var_args, GType);
		}
	}
}

void
scaffold_shell_add (ScaffoldShell *shell,
		    const char *first_name,
		    GType first_type,
		    ...)
{
	va_list var_args;

	g_return_if_fail (shell != NULL);
	g_return_if_fail (SCAFFOLD_IS_SHELL (shell));
	g_return_if_fail (first_name != NULL);

	va_start (var_args, first_type);
	scaffold_shell_add_valist (shell, first_name, first_type, var_args);
	va_end (var_args);
}

void
scaffold_shell_get_value (ScaffoldShell *shell,
			  const char *name,
			  GValue *value,
			  GError **error)
{
	g_return_if_fail (shell != NULL);
	g_return_if_fail (SCAFFOLD_IS_SHELL (shell));
	g_return_if_fail (name != NULL);
	g_return_if_fail (value != NULL);

	SCAFFOLD_SHELL_GET_IFACE (shell)->get_value (shell, name, value, error);
}

void
scaffold_shell_get_valist (ScaffoldShell *shell,
			   const char *first_name,
			   GType first_type,
			   va_list var_args)
{
	const char *name;
	GType type;

	g_return_if_fail (shell != NULL);
	g_return_if_fail (SCAFFOLD_IS_SHELL (shell));
	g_return_if_fail (first_name != NULL);
	
	name = first_name;
	type = first_type;

	while (name) {
		GValue value = {0, };
		GError *err = NULL;
		char *error;
		
		g_value_init (&value, type);
		
		scaffold_shell_get_value (shell, name, &value, &err);

		if (err) {
			g_warning ("Could not get value: %s", err->message);
			g_error_free (err);
			break;
		}

		G_VALUE_LCOPY (&value, var_args, 0, &error);
		
		if (error){
			g_warning ("%s: %s", G_STRLOC, error);
			g_free (error);
			break;
		}

		g_value_unset (&value);

		name = va_arg (var_args, char *);
		if (name) {
			type = va_arg (var_args, GType);
		}
	}
}

void
scaffold_shell_get (ScaffoldShell  *shell,
		    const char *first_name,
		    GType first_type,
		    ...)
{
	va_list var_args;

	g_return_if_fail (shell != NULL);
	g_return_if_fail (SCAFFOLD_IS_SHELL (shell));
	g_return_if_fail (first_name != NULL);

	va_start (var_args, first_type);
	scaffold_shell_get_valist (shell, first_name, first_type, var_args);
	va_end (var_args);
}

void
scaffold_shell_remove_value (ScaffoldShell *shell,
			     const char *name,
			     GError **error)
{
	g_return_if_fail (shell != NULL);
	g_return_if_fail (SCAFFOLD_IS_SHELL (shell));
	g_return_if_fail (name != NULL);

	SCAFFOLD_SHELL_GET_IFACE (shell)->remove_value (shell, name, error);
}

static void
scaffold_shell_base_init (gpointer gclass)
{
	static gboolean initialized = FALSE;
	
	if (!initialized) {
		g_signal_new ("value_added",
			      SCAFFOLD_TYPE_SHELL,
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (ScaffoldShellIface, value_added),
			      NULL, NULL,
			      scaffold_marshal_VOID__STRING_BOXED,
			      G_TYPE_NONE, 2,
			      G_TYPE_STRING, G_TYPE_VALUE);
		
		g_signal_new ("value_removed",
			      SCAFFOLD_TYPE_SHELL,
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (ScaffoldShellIface, value_added),
			      NULL, NULL,
			      scaffold_marshal_VOID__STRING,
			      G_TYPE_NONE, 1,
			      G_TYPE_STRING);

		g_signal_new ("session_load",
			      SCAFFOLD_TYPE_SHELL,
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (ScaffoldShellIface, session_load),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE, 0,
			      NULL);
		g_signal_new ("session_save",
			      SCAFFOLD_TYPE_SHELL,
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (ScaffoldShellIface, session_save),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE, 0,
			      NULL);
		initialized = TRUE;
	}
}

GType
scaffold_shell_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (ScaffoldShellIface),
			scaffold_shell_base_init,
			NULL, 
			NULL,
			NULL,
			NULL,
			0,
			0,
			NULL
		};
		
		type = g_type_register_static (G_TYPE_INTERFACE, 
					       "ScaffoldShell", 
					       &info, 0);
		
		g_type_interface_add_prerequisite (type, G_TYPE_OBJECT);
	}
	
	return type;			
}
