/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * scaffold-shell.h
 * 
 * Copyright (C) 2001, 2002 Dave Camp
 * Copyright (C) 2003 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __SCAFFOLD_SHELL_H__
#define __SCAFFOLD_SHELL_H__

#include <glib-object.h>
#include <gtk/gtkwidget.h>
#include <bonobo/bonobo-ui-container.h>

G_BEGIN_DECLS

#define SCAFFOLD_TYPE_SHELL            (scaffold_shell_get_type ())
#define SCAFFOLD_SHELL(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), SCAFFOLD_TYPE_SHELL, ScaffoldShell))
#define SCAFFOLD_IS_SHELL(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), SCAFFOLD_TYPE_SHELL))
#define SCAFFOLD_SHELL_GET_IFACE(obj)  (G_TYPE_INSTANCE_GET_INTERFACE ((obj), SCAFFOLD_TYPE_SHELL, ScaffoldShellIface))

#define SCAFFOLD_SHELL_ERROR scaffold_shell_error_quark()

typedef struct _ScaffoldShell      ScaffoldShell;
typedef struct _ScaffoldShellIface ScaffoldShellIface;
typedef enum   _ScaffoldShellError ScaffoldShellError;

struct _ScaffoldShellIface {
	GTypeInterface g_iface;
	
	/* Signals */
	void (*value_added)       (ScaffoldShell *shell,
				   const char    *name,
				   GValue        *value);
	void (*value_removed)     (ScaffoldShell *shell,
				   const char    *name);
	void (*session_load)      (ScaffoldShell *shell);
	void (*session_save)      (ScaffoldShell *shell);

	/* Virtual Table */
	void (*add_widget)        (ScaffoldShell *shell,
				   GtkWidget     *widget,
				   const char    *name,
				   const char    *title,
				   GError       **error);
	void (*add_preferences)   (ScaffoldShell *shell,
				   GtkWidget     *page,
				   const char    *name,
				   const char    *category,
				   const char    *title,
				   GError       **error);
	void (*add_value)         (ScaffoldShell *shell,
				   const char    *name,
				   const GValue  *value,
				   GError       **error);
	void (*get_value)         (ScaffoldShell *shell,
				   const char    *name,
				   GValue        *value,
				   GError       **error);
	void (*remove_value)      (ScaffoldShell *shell,
				   const char    *name,
				   GError       **error);
};

enum _ScaffoldShellError {
	SCAFFOLD_SHELL_ERROR_DOESNT_EXIST,
};

GQuark scaffold_shell_error_quark     (void);
GType  scaffold_shell_get_type        (void);
void   scaffold_shell_add_widget      (ScaffoldShell *shell,
				       GtkWidget     *widget,
				       const char    *name,
				       const char    *title,
				       GError       **error);
void   scaffold_shell_add_control     (ScaffoldShell *shell,
				       Bonobo_Control ctrl,
				       const char    *name,
				       const char    *title,
				       GError       **error);
void   scaffold_shell_add_preferences (ScaffoldShell *shell,
				       GtkWidget     *widget,
				       const char    *name,
				       const char    *category,
				       const char    *title,
				       GError       **error);
void   scaffold_shell_add_value       (ScaffoldShell *shell,
				       const char    *name,
				       const GValue  *value,
				       GError       **error);
void   scaffold_shell_add_valist      (ScaffoldShell *shell,
				       const char    *first_name,
				       GType          first_type,
				       va_list        var_args);
void   scaffold_shell_add             (ScaffoldShell *shell,
				       const char    *first_name,
				       GType          first_type,
				       ...);
void   scaffold_shell_get_value       (ScaffoldShell *shell,
				       const char    *name,
				       GValue        *value,
				       GError       **error);
void   scaffold_shell_get_valist      (ScaffoldShell *shell,
				       const char    *first_name,
				       GType          first_type,
				       va_list        var_args);
void   scaffold_shell_get             (ScaffoldShell *shell,
				       const char    *first_name,
				       GType          first_type,
				       ...);
void   scaffold_shell_remove_value    (ScaffoldShell *shell,
				       const char    *name,
				       GError       **error);

G_END_DECLS

#endif /* __SCAFFOLD_SHELL_H__ */ 
