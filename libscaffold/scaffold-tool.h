/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * scaffold-tool.h
 * 
 * Copyright (C) 2000 Dave Camp
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __SCAFFOLD_TOOL_H__
#define __SCAFFOLD_TOOL_H__

#include <glib.h>
#include <glib-object.h>
#include <string.h>
#include <bonobo/bonobo-ui-component.h>
#include "scaffold-shell.h"
#include "glue-plugin.h"

G_BEGIN_DECLS

typedef struct _ScaffoldTool        ScaffoldTool;
typedef struct _ScaffoldToolClass   ScaffoldToolClass;
typedef struct _ScaffoldToolPrivate ScaffoldToolPrivate;

typedef void (*ScaffoldToolValueAdded)   (ScaffoldTool *tool, 
					  const char   *name,
					  const GValue *value,
					  gpointer      data);
typedef void (*ScaffoldToolValueRemoved) (ScaffoldTool *tool, 
					  const char   *name,
					  gpointer      data);


#define SCAFFOLD_TYPE_TOOL         (scaffold_tool_get_type ())
#define SCAFFOLD_TOOL(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), SCAFFOLD_TYPE_TOOL, ScaffoldTool))
#define SCAFFOLD_TOOL_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), SCAFFOLD_TYPE_TOOL, ScaffoldToolClass))
#define SCAFFOLD_IS_TOOL(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), SCAFFOLD_TYPE_TOOL))
#define SCAFFOLD_IS_TOOL_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), SCAFFOLD_TYPE_TOOL))
#define SCAFFOLD_TOOL_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), SCAFFOLD_TYPE_TOOL, ScaffoldToolClass))

struct _ScaffoldTool {
	GObject parent;	

	ScaffoldShell *shell;
	BonoboUIComponent *uic;
	BonoboUIComponent *editor_uic;

	ScaffoldToolPrivate *priv;
};

struct _ScaffoldToolClass {
	GObjectClass parent_class;

	void     (*shell_set) (ScaffoldTool *tool);
	gboolean (*shutdown)  (ScaffoldTool *tool);
};

GType scaffold_tool_get_type             (void);

void  scaffold_tool_merge_ui             (ScaffoldTool *tool,
				          const char   *name,
				          const char   *datadir,
				          const char   *xmlfile,
				          BonoboUIVerb *verbs,
				          gpointer      user_data);
void  scaffold_tool_unmerge_ui           (ScaffoldTool *tool);

guint scaffold_tool_add_watch            (ScaffoldTool *tool, 
				          const char   *name,
				          ScaffoldToolValueAdded added,
				          ScaffoldToolValueRemoved removed,
				          gpointer      user_data);
void scaffold_tool_remove_watch          (ScaffoldTool *tool,
				          guint         id,
				          gboolean      send_remove);


#define SCAFFOLD_TOOL_BOILERPLATE(class_name, prefix) \
static GType \
prefix##_get_type (GluePlugin *plugin) \
{ \
	static GType type = 0; \
	if (!type) { \
		static const GTypeInfo type_info = { \
			sizeof (class_name##Class), \
			NULL, \
			NULL, \
			(GClassInitFunc)prefix##_class_init, \
			NULL, \
			NULL, \
			sizeof (class_name), \
			0, \
			(GInstanceInitFunc)prefix##_instance_init \
		}; \
		type = g_type_module_register_type (G_TYPE_MODULE (plugin), \
						    SCAFFOLD_TYPE_TOOL, \
						    #class_name, \
						    &type_info, 0); \
	} \
	return type; \
}

#define SCAFFOLD_SIMPLE_PLUGIN(class_name, prefix) \
G_MODULE_EXPORT void glue_register_components (GluePlugin *plugin); \
G_MODULE_EXPORT GType glue_get_component_type (GluePlugin *plugin, const char *name); \
G_MODULE_EXPORT void \
glue_register_components (GluePlugin *plugin) \
{ \
	prefix##_get_type (plugin); \
} \
G_MODULE_EXPORT GType \
glue_get_component_type (GluePlugin *plugin, const char *name) \
{ \
	if (!strcmp (name, #class_name)) { \
		return prefix##_get_type (plugin); \
	} else { \
		return G_TYPE_INVALID;  \
	} \
}

G_END_DECLS

#endif /* __SCAFFOLD_TOOL_H__ */
