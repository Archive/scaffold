/*
 * Scaffold docstat plugin
 *
 * Shows statistics about the current document
 */

#include <config.h>

#include <libscaffold/libscaffold.h>
#include <unistd.h>
#include <sys/stat.h>
#include <ctype.h>

#define DOCSTAT_COMPONENT_IID "OAFIID:GNOME_Development_Plugin:docstat"
#define PLUGIN_NAME			"scaffold-docstat-plugin"
#define PLUGIN_XML			"scaffold-docstat-plugin.xml"

static void
docstat(
	GtkWidget*			widget,
	gpointer			data
)
{
	ScaffoldTool*			tool = (ScaffoldTool*)data;
	gchar				tmp[255];
	gchar*				c;
	gint				in_word = 0;
	glong				words = 0;
	glong				chars = 0;
	glong				chars_ns = 0;
	GtkWidget*			dlg;
	GtkWidget*			label;
	glong				i;
	glong				length;

	length = scaffold_get_document_length(tool);

	/* go thru document... */
	for(i = 1; i <= length; i++)
	{
		c = scaffold_get_document_chars(tool, i - 1, i);

		/* Character/Word Counters */
		chars++;
		if(!isspace(*c))
		{
			chars_ns++;

			if(*c == ',' || *c == ';' || *c == ':' || *c == '.')
			{
				if(in_word)
				{
					in_word = 0;
				}
			}
			else
			{
				if(!in_word)
				{
					in_word = 1;
					words++;
				}
			}
		}
		else
		{
			if(in_word)
			{
				in_word = 0;
			}
		}

		g_free(c);
	}

	/* Dialog */
	dlg = gnome_dialog_new("Tools - Word Count", GNOME_STOCK_BUTTON_OK,
		NULL);

	g_snprintf(tmp, 255, _("Characters: %ld"), chars);
	label = gtk_label_new(tmp);
	gtk_widget_show(label);
	gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(dlg)->vbox), label, FALSE,
		TRUE, 5);

	g_snprintf(tmp, 255, _("Non-Space Characters: %ld"), chars_ns);
	label = gtk_label_new(tmp);
	gtk_widget_show(label);
	gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(dlg)->vbox), label, FALSE,
		TRUE, 5);

	g_snprintf(tmp, 255, _("Words: %ld"), words);
	label = gtk_label_new(tmp);
	gtk_widget_show(label);
	gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(dlg)->vbox), label, FALSE,
		TRUE, 5);

	gnome_dialog_run_and_close(GNOME_DIALOG(dlg));
}

/*
 * Define the verbs in this plugin
 */
static BonoboUIVerb verbs[] = {
	BONOBO_UI_UNSAFE_VERB("DocStat", docstat),
	BONOBO_UI_VERB_END
};



SCAFFOLD_SHLIB_TOOL_SIMPLE (DOCSTAT_COMPONENT_IID, "Scaffold Docstat Plugin",
			  PLUGIN_NAME, SCAFFOLD_DATADIR, PLUGIN_XML,
			  verbs, NULL);
