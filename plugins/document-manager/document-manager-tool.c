/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * document-manager-tool.c
 * 
 * Copyright (C) 2000 Dave Camp
 * Copyright (C) 2002, 2003 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <bonobo/bonobo-file-selector-util.h>
#include <gconf/gconf-client.h>
#include <gdl/gdl-combo-button.h>
#include <gdl/gdl-tools.h>
#include <glade/glade-xml.h>
#include <libscaffold/scaffold-document-manager.h>
#include <libscaffold/scaffold-tool.h>
#include <libscaffold/scaffold-session.h>
#include <libgnome/gnome-i18n.h>
#include <libgnomevfs/gnome-vfs-uri.h>
#include "scaffold-document-manager.h"
#include "scaffold-document.h"

#define DEFAULT_PATH_KEY "/apps/scaffold/plugins/document_manager/default_path"

enum {
	COL_SAVE,
	COL_NAME,
	COL_DOCUMENT,
	NUM_COLS
};

typedef struct {
	ScaffoldTool parent;
	
	ScaffoldDocumentManager *docman;
	ScaffoldDocument *current_document;

	gchar *default_path;
	GSList *docs;

	GSList *files;
	int file_index;
	
	GtkWidget *open_combo;
	GtkWidget *open_menu;
} DocumentTool;

typedef struct {
	ScaffoldToolClass parent;
} DocumentToolClass;


static void
file_new_cb (BonoboUIComponent *uic, DocumentTool *tool)
{
	scaffold_document_manager_new_document (tool->docman, "text/plain", NULL);
}

static void
file_open_cb (BonoboUIComponent *uic, ScaffoldTool *tool)
{
	DocumentTool *doc_tool = (DocumentTool *)tool;
	char **files;
	
	files = bonobo_file_selector_open_multi (GTK_WINDOW (tool->shell), TRUE,
						 _("Open File"), NULL,
						 doc_tool->default_path);
	
	if (files) {
		GnomeVFSURI *uri;
		char *file;
		int i = 0;
		
		/* set default_path to the path of the first file */
		if (doc_tool->default_path) {
			g_free (doc_tool->default_path);
			doc_tool->default_path = NULL;
		}
		uri = gnome_vfs_uri_new (files [0]);
		if (uri) {
			doc_tool->default_path = gnome_vfs_uri_extract_dirname (uri);
			gnome_vfs_uri_unref (uri);
		}
		
		file = files[i++];
		while (file) {
			scaffold_document_manager_open (doc_tool->docman, file, NULL);
			g_free (file);
			file = files[i++];
		}
		g_free (files);
	}
}

static void
file_reload_cb (BonoboUIComponent *uic, DocumentTool *tool)
{
	if (tool->current_document) {
		scaffold_bonobo_document_reload 
			(SCAFFOLD_BONOBO_DOCUMENT (tool->current_document));
	}
}

static void
file_save_cb (BonoboUIComponent *uic, DocumentTool *tool)
{
	if (tool->current_document) {
		scaffold_document_save (tool->current_document, NULL);
	}
}

static void 
file_save_as_cb (BonoboUIComponent *uic, DocumentTool *tool)
{
	if (tool->current_document) {
		scaffold_bonobo_document_save_as_dialog 
			(SCAFFOLD_BONOBO_DOCUMENT (tool->current_document));
	}
}

static void 
file_save_all_cb (BonoboUIComponent *uic, DocumentTool *tool)
{
	scaffold_document_manager_save_all (tool->docman, NULL);
}

static void
file_close_cb (BonoboUIComponent *uic, DocumentTool *tool)
{
	if (tool->current_document) {
		scaffold_document_manager_close (tool->docman,
						 tool->current_document, NULL);
	}
}

static void
file_close_all_cb (BonoboUIComponent *uic, DocumentTool *tool)
{
	scaffold_document_manager_close_all (tool->docman, NULL);
}

static BonoboUIVerb verbs [] = {
	BONOBO_UI_UNSAFE_VERB ("FileNew", file_new_cb),
	BONOBO_UI_UNSAFE_VERB ("FileOpen", file_open_cb),
        BONOBO_UI_UNSAFE_VERB ("FileReload", file_reload_cb),
        BONOBO_UI_UNSAFE_VERB ("FileSave", file_save_cb), 
        BONOBO_UI_UNSAFE_VERB ("FileSaveAs", file_save_as_cb),
        BONOBO_UI_UNSAFE_VERB ("FileSaveAll", file_save_all_cb),
	BONOBO_UI_UNSAFE_VERB ("FileClose", file_close_cb),
        BONOBO_UI_UNSAFE_VERB ("FileCloseAll", file_close_all_cb),
	BONOBO_UI_VERB_END
};

static void
set_current_document (ScaffoldTool *tool, ScaffoldDocument *doc)
{
	DocumentTool *doctool = (DocumentTool*)tool;

	doctool->current_document = doc;

	scaffold_shell_remove_value (tool->shell, 
				     "DocumentManager::CurrentDocument",
				     NULL);
	if (doc) {
		scaffold_shell_add (tool->shell,
				    "DocumentManager::CurrentDocument",
				    SCAFFOLD_TYPE_BONOBO_DOCUMENT, doc, NULL);
	}
}

static void 
current_document_changed_cb (ScaffoldNotebookDocumentManager *docman, 
			     ScaffoldDocument *doc,
			     ScaffoldTool *tool)
{
	set_current_document (tool, doc);
}

static void
document_added_cb (ScaffoldNotebookDocumentManager *docman,
		   ScaffoldDocument *doc,
		   ScaffoldTool *tool)
{
	DocumentTool *doc_tool = (DocumentTool *)tool;

	doc_tool->docs = g_slist_append (doc_tool->docs, doc);
}

static void
document_removed_cb (ScaffoldNotebookDocumentManager *docman,
		     ScaffoldDocument *doc,
		     ScaffoldTool *tool)
{
	DocumentTool *doc_tool = (DocumentTool *)tool;

	doc_tool->docs = g_slist_remove (doc_tool->docs, doc);
}

static gboolean
idle_load_cb (ScaffoldTool *tool)
{
	DocumentTool *doc_tool = (DocumentTool *)tool;
	int i = doc_tool->file_index;

	if (g_slist_nth (doc_tool->files, i) != NULL) {
		scaffold_document_manager_open (doc_tool->docman,
						g_slist_nth_data (doc_tool->files, i),
						NULL);
		doc_tool->file_index++;
	} else {
		/* NOTE: program arguments are g_strdup so g_free is ok, but
		 * session arguments are xmlNodeGetContents and should really
		 * be xmlFree'ed. Just g_free them for now. */
		g_slist_foreach (doc_tool->files, (GFunc)g_free, NULL);
		g_slist_free (doc_tool->files);
		return FALSE;
	}

	return TRUE;
}

static void
session_load_cb (ScaffoldShell *shell,
		 ScaffoldTool *tool)
{
	DocumentTool *doc_tool = (DocumentTool *)tool;
	const char **args;
	int i;
	GSList *files = NULL;
	ScaffoldSession *session;
	xmlNodePtr docs, doc;

	/* If the user started scaffold with the intention to display a source
	 * file then don't load the previous session but only the files on the
	 * commandline. */
	scaffold_shell_get (tool->shell,
			    "Shell::ProgramArguments",
			    G_TYPE_POINTER,
			    &args,
			    NULL);
	if (args) {
		for (i = 0; args[i] != NULL; i++) {
			/* Ignore options or files ending with ".scaffold". */
			if (args[i][0] == '-' ||
			    g_str_has_suffix (args[i], ".scaffold"))
				continue;
			else
				files = g_slist_append (files, g_strdup (args[i]));
		}
		if (g_slist_length (files) > 0) {
			doc_tool->files = files;
			doc_tool->file_index = 0;
			g_idle_add ((GSourceFunc)idle_load_cb, tool);
		}
		return;
	}

	/* Load the source files from the previous session. */
	scaffold_shell_get (tool->shell,
			    "Shell::CurrentSession",
			    SCAFFOLD_TYPE_SESSION,
			    &session,
			    NULL);
	docs = scaffold_session_get_group (session, "documents");
	if (docs) {
		for (doc = docs->xmlChildrenNode; doc != NULL; doc = doc->next) {
			if (doc->type == XML_ELEMENT_NODE)
				files = g_slist_append (files,
							xmlNodeGetContent (doc));
		}
		doc_tool->files = files;
		doc_tool->file_index = 0;
		g_idle_add ((GSourceFunc)idle_load_cb, tool);
	}
}

static void
session_save_cb (ScaffoldShell *shell,
		 ScaffoldTool *tool)
{
	DocumentTool *doc_tool = (DocumentTool *)tool;
	ScaffoldSession *session;
	xmlNodePtr group, child;
	GSList *l;

	scaffold_shell_get (tool->shell,
			    "Shell::CurrentSession",
			    SCAFFOLD_TYPE_SESSION,
			    &session,
			    NULL);

	group = xmlNewNode (NULL, "documents");

	for (l = doc_tool->docs; l != NULL; l = l->next) {
		ScaffoldBonoboDocument *doc = SCAFFOLD_BONOBO_DOCUMENT (l->data);
		if (scaffold_bonobo_document_is_untitled (doc))
			continue;

		/* Create new <document> node with session info. */
		child = xmlNewChild (group, NULL, "document",
				     scaffold_document_get_uri (SCAFFOLD_DOCUMENT (doc)));
	}

	scaffold_session_set_group (session, group);
}

static void
shell_set (ScaffoldTool *scaffold_tool)
{
	DocumentTool *tool = (DocumentTool*)scaffold_tool;
	BonoboUIContainer *container;
	GtkWidget *preferences;
	GConfClient *gconf_client;
	GdkPixbuf *icon;
	BonoboControl *control;

	g_signal_connect (G_OBJECT (scaffold_tool->shell), "session_load",
			  G_CALLBACK (session_load_cb), tool);
	g_signal_connect (G_OBJECT (scaffold_tool->shell), "session_save",
			  G_CALLBACK (session_save_cb), tool);

	scaffold_tool_merge_ui (scaffold_tool,
				"scaffold-document-manager",
				SCAFFOLD_DATADIR,
				"scaffold-document-manager.xml",
				verbs,
				tool);

	container = bonobo_window_get_ui_container (
		BONOBO_WINDOW (scaffold_tool->shell));

	tool->docman = 
		SCAFFOLD_DOCUMENT_MANAGER (scaffold_notebook_document_manager_new
					   (BONOBO_OBJREF (container),
					   scaffold_tool->uic));

	g_signal_connect (G_OBJECT (tool->docman), "current_document_changed",
			  G_CALLBACK (current_document_changed_cb), tool);
	g_signal_connect (G_OBJECT (tool->docman), "document_added",
			  G_CALLBACK (document_added_cb), tool);
	g_signal_connect (G_OBJECT (tool->docman), "document_removed",
			  G_CALLBACK (document_removed_cb), tool);

	scaffold_document_manager_new_document (tool->docman, "text/plain", NULL);

	gtk_widget_show (GTK_WIDGET (tool->docman));

	scaffold_shell_add_widget (scaffold_tool->shell,
				   GTK_WIDGET (tool->docman), 
				   "DocumentManager",
				   _("Documents"),
				   NULL);

	/* Create a new Build GdlComboButton. */
	tool->open_combo = gdl_combo_button_new ();
	tool->open_menu = gtk_menu_new ();
	gdl_combo_button_set_label (GDL_COMBO_BUTTON (tool->open_combo),
				    _("Open"));
	gdl_combo_button_set_menu (GDL_COMBO_BUTTON (tool->open_combo),
				   GTK_MENU (tool->open_menu));
	icon = gtk_widget_render_icon (tool->open_combo, GTK_STOCK_OPEN,
				       GTK_ICON_SIZE_LARGE_TOOLBAR, NULL);
	gdl_combo_button_set_icon (GDL_COMBO_BUTTON (tool->open_combo), icon);
	gdk_pixbuf_unref (icon);
	gtk_widget_show (tool->open_combo);
	g_signal_connect (G_OBJECT (tool->open_combo), "activate_default",
			  G_CALLBACK (file_open_cb), tool);

	/* Create new BonoboControl and add it to the toolbar. */
	control = bonobo_control_new (tool->open_combo);
	bonobo_ui_component_object_set (scaffold_tool->uic,
					"/File/DocumentOps/FileOpenComboButton",
					BONOBO_OBJREF (control), NULL);
	bonobo_object_unref (control);

	preferences = scaffold_notebook_document_manager_get_prefs_page ();
	scaffold_shell_add_preferences (scaffold_tool->shell,
					preferences,
					"DocumentManager::Preferences",
					_("General"),
					_("Documents"),
					NULL);

	/* Get default path for open file dialog. */
	gconf_client = gconf_client_get_default ();
	tool->default_path = gconf_client_get_string (gconf_client,
						      DEFAULT_PATH_KEY, NULL);
	g_object_unref (gconf_client);
}

static const char *
get_doc_label (ScaffoldBonoboDocument *document)
{
	GtkWidget *label;
	const char *str;

	label = g_object_get_data (G_OBJECT (document),
				   "ScaffoldNotebookDocumentManager::label");
	str = gtk_label_get_text (GTK_LABEL (label));

	return str;
}

static void
save_toggled_cb (GtkCellRendererToggle *cell, char *path_str, gpointer data)
{
	GtkListStore *store = GTK_LIST_STORE (data);
	GtkTreePath *path;
	GtkTreeIter iter;
	gboolean save;

	path = gtk_tree_path_new_from_string (path_str);

	gtk_tree_model_get_iter (GTK_TREE_MODEL (store), &iter, path);
	gtk_tree_model_get (GTK_TREE_MODEL (store), &iter, COL_SAVE, &save, -1);
	
	save = !save;

	gtk_list_store_set (store, &iter, COL_SAVE, save, -1);

	gtk_tree_path_free (path);
}

static void
populate_file_model (GtkListStore *store, GSList *docs)
{
	GSList *l;

	for (l = docs; l != NULL; l = l->next) {
		ScaffoldBonoboDocument *doc = SCAFFOLD_BONOBO_DOCUMENT (l->data);
		GtkTreeIter iter;

		if (!scaffold_bonobo_document_is_changed (doc))
			continue;

		gtk_list_store_append (store, &iter);
		gtk_list_store_set (store, &iter, COL_SAVE, TRUE, COL_NAME,
				    get_doc_label (doc), COL_DOCUMENT, doc, -1);
	}
}

static void
save_file_model (DocumentTool *tool,
		 GtkListStore *store,
		 gboolean save_all)
{
	GtkTreeIter iter;
	ScaffoldNotebookDocumentManager *docman;
	ScaffoldDocument *doc;
	gboolean save;
	
	docman = SCAFFOLD_NOTEBOOK_DOCUMENT_MANAGER (tool->docman);

	gtk_tree_model_get_iter_first (GTK_TREE_MODEL (store), &iter);
	do {
		gtk_tree_model_get (GTK_TREE_MODEL (store), &iter,
				    COL_SAVE, &save, COL_DOCUMENT, &doc, -1);
		if (save || save_all)
			scaffold_document_save (doc, NULL);
		scaffold_notebook_document_manager_remove_doc (docman, doc);
	} while (gtk_tree_model_iter_next (GTK_TREE_MODEL (store), &iter));
} 

static GtkResponseType
close_files_dialog (ScaffoldTool *tool)
{
	DocumentTool *doc_tool = (DocumentTool *)tool;
	GladeXML *gui;
	GtkWidget *dialog, *tree;
	GtkListStore *store;
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	GtkResponseType response;

	gui = glade_xml_new (GLADEDIR "scaffold-document-manager.glade", 
			     "save-files-dialog", NULL);

	if (!gui) {
		g_warning ("Could not load scaffold-document-manager.glade, reinstall scaffold");
		return 0;
	}

	dialog = glade_xml_get_widget (gui, "save-files-dialog");
	tree = glade_xml_get_widget (gui, "files-treeview");
	g_object_unref (G_OBJECT (gui));

	store = gtk_list_store_new (NUM_COLS, G_TYPE_BOOLEAN, G_TYPE_STRING,
				    G_TYPE_POINTER);
	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (store),
					      COL_NAME, GTK_SORT_ASCENDING);
	gtk_tree_view_set_model (GTK_TREE_VIEW (tree), GTK_TREE_MODEL (store));
	g_object_unref (G_OBJECT (store));

	renderer = gtk_cell_renderer_toggle_new ();
	g_signal_connect (G_OBJECT (renderer), "toggled",
			  G_CALLBACK (save_toggled_cb), store);
	column = gtk_tree_view_column_new_with_attributes (_("Save"),
							   renderer,
							   "active", 
							   COL_SAVE, NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (tree), column);

	renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes (_("Name"),
							   renderer,
							   "text", 
							   COL_NAME, NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (tree), column);

	populate_file_model (store, doc_tool->docs);

	gtk_window_set_transient_for (GTK_WINDOW (dialog),
				      GTK_WINDOW (tool->shell));
	gtk_widget_show_all (dialog);
	
	response = gtk_dialog_run (GTK_DIALOG (dialog));
	switch (response) {
		case GTK_RESPONSE_APPLY: /* Save selected files. */
			save_file_model (doc_tool, store, FALSE);
			break;
		case GTK_RESPONSE_YES: /* Save all files. */
			save_file_model (doc_tool, store, TRUE);
			break;
		default:
			break;
	}
	gtk_widget_destroy (dialog);

	return response;
}

static gboolean
tool_shutdown (ScaffoldTool *tool)
{
	DocumentTool *doc_tool = (DocumentTool *)tool;
	gboolean unsaved_files = FALSE;
	GSList *l;

	for (l = doc_tool->docs; l != NULL; l = l->next) {
		ScaffoldBonoboDocument *doc = SCAFFOLD_BONOBO_DOCUMENT (l->data);
		unsaved_files = unsaved_files ||
				scaffold_bonobo_document_is_changed (doc);
	}

	if (unsaved_files)
		return (close_files_dialog (tool) != GTK_RESPONSE_CANCEL);
	else
		return TRUE;
}

static void
dispose (GObject *obj)
{
	DocumentTool *tool = (DocumentTool*)obj;

	if (tool->current_document) {
		set_current_document (SCAFFOLD_TOOL (tool), NULL);
	}

	if (tool->docs) {
		g_slist_free (tool->docs);
		tool->docs = NULL;
	}

	if (tool->docman) {
		scaffold_shell_remove_value (SCAFFOLD_TOOL (tool)->shell, 
					     "DocumentManager::Preferences",
					     NULL);
		scaffold_shell_remove_value (SCAFFOLD_TOOL (tool)->shell, 
					     "DocumentManager", NULL);
		
		gtk_widget_destroy (GTK_WIDGET (tool->docman));
		scaffold_tool_unmerge_ui (SCAFFOLD_TOOL (tool));
		tool->docman = NULL;
	}

	if (tool->default_path) {
		GConfClient *gconf_client = gconf_client_get_default ();
		gconf_client_set_string (gconf_client, DEFAULT_PATH_KEY,
					 tool->default_path, NULL);
		g_object_unref (gconf_client);
		g_free (tool->default_path);
		tool->default_path = NULL;
	}
}

static void
document_tool_instance_init (GObject *object)
{
	DocumentTool *doc_tool = (DocumentTool *)object;

	doc_tool->docs = NULL;
}

static void
document_tool_class_init (GObjectClass *klass)
{
	ScaffoldToolClass *tool_class = SCAFFOLD_TOOL_CLASS (klass);

	tool_class->shell_set = shell_set;
	tool_class->shutdown = tool_shutdown;
	klass->dispose = dispose;
}

SCAFFOLD_TOOL_BOILERPLATE (DocumentTool, document_tool);

SCAFFOLD_SIMPLE_PLUGIN (DocumentTool, document_tool);
