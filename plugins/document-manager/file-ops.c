/* Scaffold
 * Copyright (C) 1998-2000 Steffen Kern
 *               2000 Dave Camp
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <config.h>
#include <string.h>
#include <libscaffold/scaffold-utils.h>
#include <gconf/gconf-client.h>
#include <bonobo/bonobo-file-selector-util.h>
#include <gdl/gdl-tools.h>
#include "file-ops.h"
#include "scaffold-document-manager.h"
#include "scaffold-document.h"

ScaffoldDocument *
file_open (GtkWidget *widget,
	   gpointer   data)
{
	char **files;

	g_return_val_if_fail (SCAFFOLD_IS_DOCUMENT_MANAGER (data), NULL);

	files = bonobo_file_selector_open_multi (NULL, TRUE, 
						 _("Open File..."), 
						 NULL, NULL);

	if (files) {
		char *file;
		int i = 0;
		ScaffoldDocument *document = NULL;

		file = files[i++];
		while (file) {
			document = scaffold_document_manager_open (
				SCAFFOLD_DOCUMENT_MANAGER (data), file);
			g_free (file);
			file = files[i++];
		}
		g_free (files);
		return document;
	}

	return NULL;
}

void
file_reload (GtkWidget *widget, gpointer data)
{
	ScaffoldDocument *current;
	gchar *filename;

	g_return_if_fail (SCAFFOLD_IS_DOCUMENT_MANAGER (data));

	current = scaffold_document_manager_get_current_doc( SCAFFOLD_DOCUMENT_MANAGER(data) );
	
	if( !current )
		return;

    	if( !scaffold_document_get_filename(current) )
        	return;

	if ( !scaffold_document_is_changed(current) )
		return;
		
	if (scaffold_dialog_question (_("The file has been changed,\nDo You want to reload it?")) == GTK_RESPONSE_NO) {
		return;
	}

	filename = g_strdup (scaffold_document_get_filename (current));
	scaffold_document_load_uri (current, filename);
	g_free (filename);    	
}

void
file_save (GtkWidget *widget, gpointer data)
{
	ScaffoldDocumentManager *docman;
	ScaffoldDocument *document;

	g_return_if_fail (SCAFFOLD_IS_DOCUMENT_MANAGER (data));

	docman = SCAFFOLD_DOCUMENT_MANAGER(data);
	
	document = scaffold_document_manager_get_current_doc( docman );
	if( !document )
		return;

	if (!scaffold_document_get_filename(document) ||
	    scaffold_document_is_untitled (document)) {
        	file_save_as( widget, data );
        	return;
    	}
    
	scaffold_document_save_uri (document,
				  scaffold_document_get_filename(document));
}

void
file_save_as (GtkWidget *widget,
	      gpointer   data)
{
	ScaffoldDocumentManager *docman;
	ScaffoldDocument *document;
	char *old_name, *new_name;

	g_return_if_fail (SCAFFOLD_IS_DOCUMENT_MANAGER (data));

	docman = SCAFFOLD_DOCUMENT_MANAGER (data);
	document = scaffold_document_manager_get_current_doc (docman);

	old_name = scaffold_document_get_filename (document);

	new_name = bonobo_file_selector_save (NULL, TRUE, NULL, NULL, 
					      NULL, old_name);

	if (new_name) {
		scaffold_document_save_uri (document, new_name);
		g_free (new_name);
	}
}

void
file_close( GtkWidget *widget, gpointer data)
{
	ScaffoldDocumentManager *docman;
	ScaffoldDocument *document;

	g_return_if_fail (SCAFFOLD_IS_DOCUMENT_MANAGER (data));

	docman = SCAFFOLD_DOCUMENT_MANAGER(data);

	document = scaffold_document_manager_get_current_doc( docman );

	if( !document )
		return;

    	if( scaffold_document_is_changed( document ) ) { 
		if (file_close_dialog (docman, document) == GTK_RESPONSE_CANCEL)
			return; 
    	}

	scaffold_document_manager_remove_doc (docman, document);
}

GtkResponseType
file_close_dialog (ScaffoldDocumentManager *docman,
		   ScaffoldDocument        *current)
{
	char *filename;
	GtkResponseType ret;
	GtkWidget *dialog;
	GtkWidget *button;

	if (!scaffold_document_get_filename (current)) {
		char *label;

		label = g_strdup (scaffold_document_manager_get_doc_label (docman, current));
		filename = g_path_get_basename (label);
	} else {
		filename = g_strdup (scaffold_document_get_filename (current));
	}

	dialog = gtk_message_dialog_new (NULL,
					 GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
					 GTK_MESSAGE_QUESTION,
					 GTK_BUTTONS_NONE,
					 _("Do you want to save the changes you made to the document \"%s\"? \n\n"
					   "Your changes will be lost if you don't save them."),
					 filename);

	/* Add "Don't save" button. */
	button = gdl_button_new_with_stock_image (_("Do_n't save"), GTK_STOCK_NO);
	g_return_val_if_fail (button != NULL, GTK_RESPONSE_CANCEL);
	gtk_dialog_add_action_widget (GTK_DIALOG (dialog),
				      button,
				      GTK_RESPONSE_NO);
	gtk_widget_show (button);

	gtk_dialog_add_button (GTK_DIALOG (dialog),
			       GTK_STOCK_CANCEL,
			       GTK_RESPONSE_CANCEL);

	gtk_dialog_add_button (GTK_DIALOG (dialog),
			       GTK_STOCK_SAVE,
			       GTK_RESPONSE_YES);

	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_YES);

	ret = gtk_dialog_run (GTK_DIALOG (dialog));

	gtk_widget_destroy (dialog);

	if (ret == GTK_RESPONSE_YES) {
		if (!scaffold_document_get_filename (current) ||
		    scaffold_document_is_untitled (current)) {
			char *new_name;

			new_name = bonobo_file_selector_save (NULL, TRUE,
							      NULL, NULL,
							      NULL, filename);

			if (new_name) {
				scaffold_document_save_uri (current, new_name);
				g_free (new_name);
			} else {
				ret = GTK_RESPONSE_CANCEL;
			}
		} else {
			file_save (NULL, docman);
		}
	}

	g_free (filename);

	return ret;
}

void
file_close_all(GtkWidget *widget, gpointer data)
{
	int i;
	ScaffoldDocumentManager *docman;

	g_return_if_fail (SCAFFOLD_IS_DOCUMENT_MANAGER (data));

	docman = SCAFFOLD_DOCUMENT_MANAGER (data);

	for(i = 0; i < scaffold_document_manager_num_docs(docman); i++)
	{
		gtk_notebook_set_current_page(GTK_NOTEBOOK(docman), i);
		file_close(NULL, docman);
	}
}

void
file_save_all(GtkWidget *widget, gpointer data)
{
	int i;
	ScaffoldDocumentManager *docman;
	int nod;
	ScaffoldDocument *document;

	g_return_if_fail(SCAFFOLD_IS_DOCUMENT_MANAGER(data));

	docman = SCAFFOLD_DOCUMENT_MANAGER(data);

	nod = scaffold_document_manager_num_changed_docs(docman);

	if(nod > 0) {
		for(i = 0; i < scaffold_document_manager_num_docs(docman); i++) {
			gtk_notebook_set_current_page(GTK_NOTEBOOK(docman), i);
			document = scaffold_document_manager_get_current_doc(docman);
			if (scaffold_document_is_changed(document)) {
				file_save(NULL, docman);
			}
		}
	}
}
