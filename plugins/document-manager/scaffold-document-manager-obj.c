/* Scaffold
 * Copyright 2000 Dave Camp
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#include <config.h>
#include <gnome.h>
#include <bonobo.h>

#include <gdl/gdl.h>
#include <libscaffold/libscaffold.h>
#include "scaffold-document-manager-obj.h"
#include "scaffold-document-manager.h"
#include "file-ops.h"

struct _ScaffoldDocumentManagerObjPriv
{
	char dummy;
};

static void scaffold_document_manager_obj_finalize (GObject *object);

ScaffoldDocumentManagerObj *
scaffold_document_manager_obj_new (ScaffoldDocumentManager *dm)
{
	ScaffoldDocumentManagerObj *docman;
	
	g_return_val_if_fail (dm != NULL, NULL);

	docman = g_object_new (scaffold_document_manager_obj_get_type (),
			       NULL);
	docman->dm = dm;

	return docman;
}

BONOBO_BOILERPLATE (ScaffoldDocumentManagerObj, scaffold_document_manager_obj,
		    GNOME_Development_DocumentManager,
		    BonoboObject, BONOBO_TYPE_OBJECT,
		    BONOBO_REGISTER_TYPE_FULL);

static void
scaffold_document_manager_obj_finalize (GObject *object) 
{
	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static GNOME_Development_Document
impl_get_document (PortableServer_Servant servant, 
		   const CORBA_char *filename,
		   CORBA_boolean existing_only,
		   CORBA_Environment *ev)
{
	ScaffoldDocumentManagerObj *docman = SCAFFOLD_DOCUMENT_MANAGER_OBJ (bonobo_object_from_servant (servant));
	ScaffoldDocument *doc;

	doc = scaffold_document_manager_get_doc_by_name (docman->dm, filename);

	if (!doc) {
		if (!existing_only) {
			doc = scaffold_document_manager_open (docman->dm, filename);
			if (!doc) {
				bonobo_exception_set
					(ev, 
					 "IDL:GNOME/Development/DocumentManager/UnknownError:1.0");
				return NULL;
			}
		}
	}
	return bonobo_object_dup_ref (BONOBO_OBJREF (doc->docobj), ev);

}

static GNOME_Development_DocumentManager_DocumentList*
impl_get_open_files (PortableServer_Servant servant,
		     CORBA_Environment *ev)
{
	GNOME_Development_DocumentManager_DocumentList *ret;
	int i;
	gint numdocs;

	ScaffoldDocumentManagerObj *docman = SCAFFOLD_DOCUMENT_MANAGER_OBJ (bonobo_object_from_servant (servant));

	numdocs = scaffold_document_manager_num_docs(docman->dm);

	ret = GNOME_Development_DocumentManager_DocumentList__alloc ();

	ret->_length = numdocs;
	ret->_maximum = numdocs;
	ret->_buffer = CORBA_sequence_GNOME_Development_Document_allocbuf(numdocs);
	CORBA_sequence_set_release(ret, TRUE);

	for(i = 0; i < numdocs; i++)
	{
		ScaffoldDocument *doc = scaffold_document_manager_get_nth_doc(docman->dm, i);

// ?????
		ret->_buffer[i] = bonobo_object_dup_ref(BONOBO_OBJREF(doc->docobj), ev);
	}

	return ret;
}

static void
scaffold_document_manager_obj_class_init (ScaffoldDocumentManagerObjClass *class) 
{
	GObjectClass *object_class = (GObjectClass*) class;
	parent_class = g_type_class_peek_parent (class);
    
	object_class->finalize = scaffold_document_manager_obj_finalize;
    
	class->epv.getDocument = impl_get_document;
	class->epv.getOpenFiles = impl_get_open_files;
}

static void
scaffold_document_manager_obj_instance_init (ScaffoldDocumentManagerObj *docman)
{
	docman->priv = NULL;
}

