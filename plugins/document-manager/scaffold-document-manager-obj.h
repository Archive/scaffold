/* Scaffold
 * Copyright (C) 2000 Dave Camp
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#ifndef SCAFFOLD_DOCUMENT_MANAGER_OBJ_H
#define SCAFFOLD_DOCUMENT_MANAGER_OBJ_H

#include <libbonobo.h>

G_BEGIN_DECLS

typedef struct _ScaffoldDocumentManagerObj      ScaffoldDocumentManagerObj;
typedef struct _ScaffoldDocumentManagerObjClass ScaffoldDocumentManagerObjClass;
typedef struct _ScaffoldDocumentManagerObjPriv  ScaffoldDocumentManagerObjPriv;

struct _ScaffoldDocumentManager;

#define SCAFFOLD_DOCUMENT_MANAGER_OBJ_TYPE        (scaffold_document_manager_obj_get_type ())
#define SCAFFOLD_DOCUMENT_MANAGER_OBJ(o)          (GTK_CHECK_CAST ((o), SCAFFOLD_DOCUMENT_MANAGER_OBJ_TYPE, ScaffoldDocumentManagerObj))
#define SCAFFOLD_DOCUMENT_MANAGER_OBJ_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), SCAFFOLD_DOCUMENT_MANAGER_OBJ_TYPE, ScaffoldDocumentManagerObjClass))
#define SCAFFOLD_IS_DOCUMENT_MANAGER_OBJ(o)       (GTK_CHECK_TYPE ((o), SCAFFOLD_DOCUMENT_MANAGER_OBJ_TYPE))
#define SCAFFOLD_IS_DOCUMENT_MANAGER_OBJ_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), SCAFFOLD_DOCUMENT_MANAGER_OBJ_TYPE))

struct _ScaffoldDocumentManagerObj {
	BonoboObject parent;
	
	struct _ScaffoldDocumentManager *dm;

	ScaffoldDocumentManagerObjPriv *priv;
};

struct _ScaffoldDocumentManagerObjClass {
	BonoboObjectClass parent_class;

	POA_GNOME_Development_DocumentManager__epv epv;
};

GtkType                 scaffold_document_manager_obj_get_type (void);
ScaffoldDocumentManagerObj *scaffold_document_manager_obj_new      (struct _ScaffoldDocumentManager *dm);

G_END_DECLS

#endif
