/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * scaffold-document-manager.c
 * 
 * Copyright (C) 1998-2000 Steffen Kern
 * Copyright (C) 2000 Dave Camp
 * Copyright (C) 2002, 2003 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <libscaffold/scaffold-document-manager.h>
#include <libscaffold/scaffold-utils.h>
#include <libscaffold/gconf-property-editor.h>
#include <libgnome/gnome-macros.h>
#include <libgnomeui/gnome-uidefs.h>
#include <libgnomevfs/gnome-vfs-directory.h>
#include <libgnomevfs/gnome-vfs-mime-handlers.h>
#include <libgnomevfs/gnome-vfs-ops.h>
#include <gconf/gconf-client.h>
#include <gconf/gconf-value.h>
#include <glade/glade-xml.h>
#include <gdl/gdl-icons.h>
#include <gdl/gdl-recent.h>
#include <gdl/gdl-tools.h>
#include "scaffold-document.h"
#include "scaffold-document-manager.h"

#define SCAFFOLD_DOCMAN_PREFIX             "/apps/scaffold/plugins/document_manager"
#define SCAFFOLD_DOCMAN_RECENT_FILES       SCAFFOLD_DOCMAN_PREFIX "/recent_files"
#define SCAFFOLD_DOCMAN_RECENT_FILES_LIMIT SCAFFOLD_DOCMAN_PREFIX "/recent_files_limit"
#define SCAFFOLD_DOCMAN_TAB_LOCATION       SCAFFOLD_DOCMAN_PREFIX "/tab_location"
#define SCAFFOLD_DOCMAN_DEFAULT_MIME_TYPE  SCAFFOLD_DOCMAN_PREFIX "/default_mime_type"

struct _ScaffoldNotebookDocumentManagerPriv {
	unsigned long untitled_count;

	guint recent_notify;
	guint tab_location_notify;

	GdlRecent *recent_files;
	GdlIcons *icons;

	GHashTable *templates;
	GHashTable *verbs;

	GHashTable *popups;

	GConfClient *client;

	GtkTooltips *tooltips;
};

typedef struct {
	char *uri;
	char *mime_type;
	char *label;
	char *tip;
} NewItemMenuData;

typedef struct {
	char *mime_types;
	char *datadir;
	char *xmlfile;
	BonoboUIVerb *verbs;
	gpointer user_data;
} PopupMenuData;

static void docman_finalize (GObject *object);

static void docman_switch_notebookpage(GtkWidget* widget,
				       GtkNotebookPage* page,
				       gint page_num,
				       gpointer data);
static void docman_recent_files (GdlRecent  *recent,
				 const char *uri,
				 gpointer    data);

static void docman_doc_modified (GtkWidget *widget, gpointer data);
static void docman_doc_unmodified (GtkWidget *widget, gpointer data);
static void docman_doc_uri_changed (GtkWidget *widget, 
				    const char *uri, 
				    gpointer data);
static void docman_doc_destroy (GtkWidget *widget, gpointer data);
static void set_current_document (ScaffoldNotebookDocumentManager *docman,
				  ScaffoldDocument *doc);
static void load_new_menu (ScaffoldNotebookDocumentManager *docman);

gpointer parent_class;

/* public routines */

GtkWidget *
scaffold_notebook_document_manager_new (Bonobo_UIContainer ui_container,
				      BonoboUIComponent *ui_component)
{
	ScaffoldNotebookDocumentManager *dm;

	dm = g_object_new (SCAFFOLD_TYPE_NOTEBOOK_DOCUMENT_MANAGER, NULL);
 
	dm->ui_container = ui_container;
	dm->ui_component = ui_component;

	load_new_menu (dm);

	gdl_recent_set_ui_component (dm->priv->recent_files, ui_component);

	return GTK_WIDGET (dm);
}

/* Document Manipulation */


static const char *
get_doc_label (ScaffoldNotebookDocumentManager *docman, 
	       ScaffoldDocument *document)
{
	GtkWidget *label;
	const char *str;

	g_return_val_if_fail (docman != NULL, NULL);
	g_return_val_if_fail (SCAFFOLD_IS_DOCUMENT_MANAGER (docman), NULL);
	g_return_val_if_fail (document != NULL, NULL);
	g_return_val_if_fail (SCAFFOLD_IS_DOCUMENT (document), NULL);
	
	label = g_object_get_data (G_OBJECT (document), 
				    "ScaffoldNotebookDocumentManager::label");
	str = gtk_label_get_text (GTK_LABEL (label));
	
	return str;
}

static void
set_doc_uri (ScaffoldNotebookDocumentManager *docman,
	       ScaffoldDocument *document,
	       const char *filename)
{
	gchar *basename;
	GdkPixbuf *pixbuf;
	GtkWidget *tooltip;
	GtkWidget *label;
	GtkWidget *icon;

	g_return_if_fail (SCAFFOLD_IS_DOCUMENT_MANAGER (docman));
	g_return_if_fail (SCAFFOLD_IS_DOCUMENT (document));
	g_return_if_fail (filename != NULL);
	
	basename = g_path_get_basename (filename);
	label = g_object_get_data (G_OBJECT (document), 
				   "ScaffoldNotebookDocumentManager::label");
	gtk_label_set_text (GTK_LABEL (label), basename);
	g_free (basename);

	tooltip = g_object_get_data (G_OBJECT (document), 
				   "ScaffoldNotebookDocumentManager::tooltip");
	gtk_tooltips_set_tip (docman->priv->tooltips, tooltip, filename, NULL);

	pixbuf = gdl_icons_get_uri_icon (docman->priv->icons, filename);
	icon = g_object_get_data (G_OBJECT (document),
				  "ScaffoldNotebookDocumentManager::icon");
	gtk_image_set_from_pixbuf (GTK_IMAGE (icon), pixbuf);
	g_object_unref (pixbuf);
}

void
scaffold_notebook_document_manager_remove_doc (ScaffoldNotebookDocumentManager *docman,
					     ScaffoldDocument *document)
{
	int index;
	
	g_object_ref (document);
	index = gtk_notebook_page_num (GTK_NOTEBOOK (docman), 
				       GTK_WIDGET (document));
	if (index > -1) {
		gtk_notebook_remove_page (GTK_NOTEBOOK (docman), index);
		
		if (scaffold_bonobo_document_is_untitled (SCAFFOLD_BONOBO_DOCUMENT (document))) {
			docman->priv->untitled_count--;
		}

		g_signal_emit_by_name (docman, "document_removed", document);

		docman->documents = g_list_remove (docman->documents, document);
	}

	g_object_unref (document);
}

static void
merge_popup_menu (gpointer key, gpointer value, gpointer user_data)
{
	char *name = key;
	PopupMenuData *pmd = value;
	BonoboUIComponent *uic = BONOBO_UI_COMPONENT (user_data);
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	bonobo_ui_util_set_ui (uic, pmd->datadir, pmd->xmlfile, name, &ev);
	if (BONOBO_EX (&ev)) {
		g_warning ("exception: %s", CORBA_exception_id (&ev));
		CORBA_exception_free (&ev);
	}

	bonobo_ui_component_add_verb_list_with_data (uic, pmd->verbs,
						     pmd->user_data);
}

static void
update_popup_menus (ScaffoldNotebookDocumentManager *dm)
{
	ScaffoldBonoboDocument *doc;
	CORBA_Environment ev;
	BonoboUIComponent *uic;

	doc = SCAFFOLD_BONOBO_DOCUMENT (dm->current_document);

	CORBA_exception_init (&ev);

	uic = bonobo_control_frame_get_popup_component (doc->control_frame, &ev);
	if (uic == NULL || BONOBO_EX (&ev)) {
		g_warning ("Failed to get popup component from bonobo control");
		CORBA_exception_free (&ev);
		return;
	}

	CORBA_exception_free (&ev);

	g_hash_table_foreach (dm->priv->popups, merge_popup_menu, uic); 
}

void
scaffold_notebook_document_manager_add_editor_ui (ScaffoldNotebookDocumentManager *dm,
						  const char *name,
						  const char *mime_types,
						  const char *datadir,
						  const char *xmlfile,
						  BonoboUIVerb *verbs,
						  gpointer user_data)
{
	PopupMenuData *pmd;

	g_return_if_fail (dm != NULL);
	g_return_if_fail (SCAFFOLD_IS_NOTEBOOK_DOCUMENT_MANAGER (dm));
	g_return_if_fail (name != NULL);
	g_return_if_fail (datadir != NULL);
	g_return_if_fail (xmlfile != NULL);
	g_return_if_fail (verbs != NULL);

	g_message ("add_editor_ui: %s", name);

	pmd = g_new0 (PopupMenuData, 1);
	pmd->mime_types	= g_strdup (mime_types);
	pmd->datadir = g_strdup (datadir);
	pmd->xmlfile = g_strdup (xmlfile);
	pmd->verbs = verbs;
	pmd->user_data = user_data;

	g_hash_table_insert (dm->priv->popups, g_strdup (name), pmd);
	update_popup_menus (dm);
}

void
scaffold_notebook_document_manager_remove_editor_ui (ScaffoldNotebookDocumentManager *dm,
						     const char *name)
{
	PopupMenuData *pmd;

	g_return_if_fail (dm != NULL);
	g_return_if_fail (SCAFFOLD_IS_NOTEBOOK_DOCUMENT_MANAGER (dm));
	g_return_if_fail (name != NULL);

	pmd = g_hash_table_lookup (dm->priv->popups, name);
	if (pmd) {
		g_free (pmd->mime_types);
		g_free (pmd->datadir);
		g_free (pmd->xmlfile);
		g_free (pmd);
		g_hash_table_remove (dm->priv->popups, name);
		update_popup_menus (dm);
	} else {
		g_warning ("No popup menus registered under the name '%s'", name);
	}
}

static GtkResponseType
file_close_dialog (ScaffoldNotebookDocumentManager *docman, 
		   ScaffoldDocument *current)
{
	char *filename;
	GtkResponseType ret;
	GtkWidget *dialog;
	GtkWidget *button;

	if (!scaffold_document_get_uri (SCAFFOLD_DOCUMENT (current))) {
		char *label;

		label = g_strdup (get_doc_label (docman, current));
		filename = g_path_get_basename (label);
		g_free (label);
	} else {
		filename = g_strdup (scaffold_document_get_uri (SCAFFOLD_DOCUMENT (current)));
	}

	dialog = gtk_message_dialog_new (NULL,
					 GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
					 GTK_MESSAGE_QUESTION,
					 GTK_BUTTONS_NONE,
					 _("Do you want to save the changes you made to the document \"%s\"? \n\n"
					   "Your changes will be lost if you don't save them."),
					 filename);

	/* Add "Don't save" button. */
	button = gdl_button_new_with_stock_image (_("Do_n't save"), GTK_STOCK_NO);
	g_return_val_if_fail (button != NULL, GTK_RESPONSE_CANCEL);
	gtk_dialog_add_action_widget (GTK_DIALOG (dialog),
				      button,
				      GTK_RESPONSE_NO);
	gtk_widget_show (button);

	gtk_dialog_add_button (GTK_DIALOG (dialog),
			       GTK_STOCK_CANCEL,
			       GTK_RESPONSE_CANCEL);

	gtk_dialog_add_button (GTK_DIALOG (dialog),
			       GTK_STOCK_SAVE,
			       GTK_RESPONSE_YES);

	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_YES);

	ret = gtk_dialog_run (GTK_DIALOG (dialog));

	gtk_widget_destroy (dialog);

	if (ret == GTK_RESPONSE_YES) {
		scaffold_document_save (SCAFFOLD_DOCUMENT (current), NULL);
	}

	g_free (filename);

	return ret;
}

static void
close_document_cb (GtkButton *button,
		   ScaffoldNotebookDocumentManager *docman)
{
	ScaffoldDocument *document;

	document = g_object_get_data (G_OBJECT (button), "document");

	if (scaffold_bonobo_document_is_changed (SCAFFOLD_BONOBO_DOCUMENT (document))) {
		if (file_close_dialog (docman, document) == GTK_RESPONSE_CANCEL)
			return; 
	}

	scaffold_notebook_document_manager_remove_doc (docman, document);

	if (!docman->documents) {
		scaffold_document_manager_new_document (SCAFFOLD_DOCUMENT_MANAGER (docman),
						      "text/plain",
						      NULL);
	}
}

static void
add_doc (ScaffoldNotebookDocumentManager *docman, ScaffoldDocument *document)
{
	GtkWidget *event_box;
	GtkWidget *tab_hbox;
	GtkWidget *label;
	GtkWidget *button;
	GtkWidget *pixmap;
	GdkPixbuf *pixbuf;
	char *label_str;
	const char *uri;
	const char *mime_type;
	GtkWidget *icon;
	
	g_return_if_fail (docman != NULL);
	g_return_if_fail (SCAFFOLD_IS_DOCUMENT_MANAGER (docman));
	g_return_if_fail (document != NULL);
	g_return_if_fail (SCAFFOLD_IS_DOCUMENT (document));

	/* Get or create filename & label string. */
	uri = scaffold_document_get_uri (SCAFFOLD_DOCUMENT (document));
	if (uri) {
		label_str = g_path_get_basename (uri);
	} else {
		label_str = g_strdup_printf (_("Untitled %ld"),
					     ++docman->priv->untitled_count);
		SCAFFOLD_BONOBO_DOCUMENT (document)->untitled = TRUE;
	}

	mime_type = scaffold_document_get_mime_type (document);
	pixbuf = gdl_icons_get_mime_icon (docman->priv->icons, mime_type);
	icon = gtk_image_new_from_pixbuf (pixbuf);
	g_object_unref (pixbuf);

	/* Remove untitled document first if it's the current active document. */
	if (docman->current_document && uri &&
	    scaffold_bonobo_document_is_untitled (SCAFFOLD_BONOBO_DOCUMENT (docman->current_document)) &&
	    !scaffold_bonobo_document_is_changed (SCAFFOLD_BONOBO_DOCUMENT (docman->current_document))) {
		scaffold_notebook_document_manager_remove_doc (docman, docman->current_document);
	}

	/* Add the document to the list */
	docman->documents = g_list_append (docman->documents, document);

	label = gtk_label_new (label_str);
	g_object_set_data (G_OBJECT (document),
			   "ScaffoldNotebookDocumentManager::label", label);
	g_object_set_data (G_OBJECT (document),
			   "ScaffoldNotebookDocumentManager::icon", icon);

	g_free (label_str);

	/* Build the tab widgets and tooltip */
	event_box = gtk_event_box_new ();
	g_object_set_data (G_OBJECT (document),
			   "ScaffoldNotebookDocumentManager::tooltip",
			   event_box);

	if (uri)
		gtk_tooltips_set_tip (docman->priv->tooltips, event_box,
				      uri, NULL);

	tab_hbox = gtk_hbox_new (FALSE, 2);
	pixmap = gtk_image_new_from_stock (GTK_STOCK_CLOSE, GTK_ICON_SIZE_MENU);;
	button = gtk_button_new ();
	gtk_button_set_relief (GTK_BUTTON (button), GTK_RELIEF_NONE);
	gtk_container_add (GTK_CONTAINER (button), pixmap);
	gtk_widget_set_size_request (button, 18, 18);

	g_object_set_data (G_OBJECT (button), "document", document);
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (close_document_cb), docman);

	gtk_box_pack_start (GTK_BOX (tab_hbox), icon, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (tab_hbox), label, TRUE, FALSE, 0);
	gtk_box_pack_end (GTK_BOX (tab_hbox), button, FALSE, FALSE, 0);

	gtk_container_add (GTK_CONTAINER (event_box), tab_hbox);
	gtk_widget_show_all (event_box);

	gtk_notebook_append_page (GTK_NOTEBOOK (docman),
				  GTK_WIDGET (document), event_box);
	
	g_signal_connect (G_OBJECT (document), "uri_changed",
			  G_CALLBACK (docman_doc_uri_changed), docman);
	g_signal_connect (G_OBJECT (document), "destroy",
			  G_CALLBACK (docman_doc_destroy), docman);

	gtk_widget_show (GTK_WIDGET (document));
	gtk_widget_grab_focus (GTK_WIDGET (document));

	/* Add the document to the recent files list. */
	if (!scaffold_bonobo_document_is_untitled (SCAFFOLD_BONOBO_DOCUMENT (document)))
		gdl_recent_add (docman->priv->recent_files, uri);

	/* Add popup menuitems to the control's popup menu. */
	update_popup_menus (docman);

	/* Flip to the new page, will set off the signal */
	scaffold_document_manager_show_document 
		(SCAFFOLD_DOCUMENT_MANAGER (docman), document);

	/* Well, it won't always set off the signal.  Not the first time. */
	if (!docman->current_document)
		set_current_document (docman, document);

	g_signal_emit_by_name (docman, "document_added", document);
}

/* private routines */

static void
docman_dispose (GObject *object)
{
	ScaffoldNotebookDocumentManager *docman = SCAFFOLD_NOTEBOOK_DOCUMENT_MANAGER (object);
	GList *l;

	if (docman->priv->client) {
		gconf_client_notify_remove (docman->priv->client,
					    docman->priv->recent_notify);
		gconf_client_notify_remove (docman->priv->client,
					    docman->priv->tab_location_notify);
		g_object_unref (docman->priv->client);
		docman->priv->client = NULL;
	}

	if (docman->priv->recent_files) {
		g_object_unref (docman->priv->recent_files);
		docman->priv->recent_files = NULL;
	}

	if (docman->priv->icons) {
		g_object_unref (docman->priv->icons);
		docman->priv->icons = NULL;
	}

	if (docman->documents) {
		for (l = docman->documents; l != NULL; l = l->next) {
			ScaffoldBonoboDocument *doc = SCAFFOLD_BONOBO_DOCUMENT (l->data);
			gtk_object_destroy (GTK_OBJECT (doc));
		}
		g_list_free (docman->documents);
		docman->documents = NULL;
	}

	if (docman->priv->tooltips) {
		g_object_unref (docman->priv->tooltips);
		docman->priv->tooltips = NULL;
	}
}

static void
docman_finalize (GObject *object) 
{
	ScaffoldNotebookDocumentManager *docman = SCAFFOLD_NOTEBOOK_DOCUMENT_MANAGER (object);

	g_hash_table_destroy (docman->priv->templates);
	g_hash_table_destroy (docman->priv->verbs);

	g_free (docman->priv);

        G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
scaffold_notebook_document_manager_class_init (ScaffoldNotebookDocumentManagerClass *klass) 
{
	GObjectClass *object_class = (GObjectClass*) klass;
	parent_class = g_type_class_peek_parent (klass);
	
	object_class->dispose = docman_dispose;
	object_class->finalize = docman_finalize;

	g_signal_new ("document_added",
		      G_TYPE_FROM_CLASS (klass),
		      G_SIGNAL_RUN_LAST,
		      G_STRUCT_OFFSET (ScaffoldNotebookDocumentManagerClass,
				       document_added),
		      NULL, NULL,
		      g_cclosure_marshal_VOID__OBJECT,
		      G_TYPE_NONE, 1,
		      SCAFFOLD_TYPE_DOCUMENT);
	g_signal_new ("document_removed",
		      G_TYPE_FROM_CLASS (klass),
		      G_SIGNAL_RUN_LAST,
		      G_STRUCT_OFFSET (ScaffoldNotebookDocumentManagerClass,
				       document_removed),
		      NULL, NULL,
		      g_cclosure_marshal_VOID__OBJECT,
		      G_TYPE_NONE, 1,
		      SCAFFOLD_TYPE_DOCUMENT);
}

static void
recent_limit_notify_cb (GConfClient *client,
			guint cnxn_id,
			GConfEntry *entry,
			gpointer user_data)
{
	GConfValue *value;
	ScaffoldNotebookDocumentManager *dm = SCAFFOLD_NOTEBOOK_DOCUMENT_MANAGER (user_data);
	
	value = gconf_entry_get_value (entry);
	if (value) {
		gdl_recent_set_limit (dm->priv->recent_files,
				      gconf_value_get_int (value));
	}
}

static void
set_tab_pos_from_gconf (ScaffoldNotebookDocumentManager *dm)
{
	char *str;

	str = gconf_client_get_string (dm->priv->client,
				       SCAFFOLD_DOCMAN_TAB_LOCATION, NULL);
	
	if (str) {
		GEnumClass *class;
		GEnumValue *val;
		int position;

		class = g_type_class_ref (gtk_position_type_get_type());
		
		val = g_enum_get_value_by_name (class, str);
		if (!val) {
			position = GTK_POS_BOTTOM;
		} else {
			position = val->value;
		}

		g_type_class_unref (class);

		gtk_notebook_set_tab_pos (GTK_NOTEBOOK (dm), position);
	}
}	

static void
tab_location_notify_cb (GConfClient *client,
			guint cnxn_id,
			GConfEntry *entry,
			gpointer user_data)
{
	set_tab_pos_from_gconf (SCAFFOLD_NOTEBOOK_DOCUMENT_MANAGER (user_data));
}

static void
scaffold_notebook_document_manager_instance_init (ScaffoldNotebookDocumentManager *dm)
{
	int limit;

	dm->priv = g_new0 (ScaffoldNotebookDocumentManagerPriv, 1);

	dm->priv->untitled_count = 0;
	dm->priv->popups = g_hash_table_new_full (g_str_hash, g_str_equal,
						  g_free, NULL);

	dm->priv->client = gconf_client_get_default ();
	gconf_client_add_dir (dm->priv->client, 
			      SCAFFOLD_DOCMAN_PREFIX,
			      GCONF_CLIENT_PRELOAD_NONE,
			      NULL);

	limit = gconf_client_get_int (dm->priv->client,
				  SCAFFOLD_DOCMAN_RECENT_FILES_LIMIT,
				  NULL);
	limit = (limit > 0) ? limit : 10;
	dm->priv->recent_files = gdl_recent_new (SCAFFOLD_DOCMAN_RECENT_FILES,
						 "/menu/File/FileRecentFilesPlaceholder/FileRecent/RecentFiles",
						 limit,
						 GDL_RECENT_LIST_NUMERIC);

	dm->priv->icons = gdl_icons_new (16);

	dm->priv->recent_notify = gconf_client_notify_add
		(dm->priv->client,
		 SCAFFOLD_DOCMAN_RECENT_FILES_LIMIT,
		 recent_limit_notify_cb,
		 dm, NULL, NULL);
	dm->priv->tab_location_notify = gconf_client_notify_add
		(dm->priv->client,
		 SCAFFOLD_DOCMAN_TAB_LOCATION,
		 tab_location_notify_cb,
		 dm, NULL, NULL);

	gtk_notebook_set_scrollable (GTK_NOTEBOOK (dm), TRUE);

	g_signal_connect_after (G_OBJECT (dm), "switch_page",
				G_CALLBACK (docman_switch_notebookpage),
				dm);

	set_tab_pos_from_gconf (dm);

	g_signal_connect (G_OBJECT (dm->priv->recent_files),
			  "activate",
			  G_CALLBACK (docman_recent_files),
			  dm);

	dm->priv->tooltips = gtk_tooltips_new ();
	g_object_ref (G_OBJECT (dm->priv->tooltips));
	gtk_object_sink (GTK_OBJECT (dm->priv->tooltips));
}

static void
docman_switch_notebookpage (GtkWidget *widget,
			    GtkNotebookPage *page,
			    gint page_num,
			    gpointer data)
{
        ScaffoldDocument *document;
	ScaffoldNotebookDocumentManager *docman;
	
	g_assert (SCAFFOLD_IS_DOCUMENT_MANAGER (data));
	
        docman = SCAFFOLD_NOTEBOOK_DOCUMENT_MANAGER (data);
        
        g_return_if_fail (docman);

        document = scaffold_document_manager_get_nth_document
		(SCAFFOLD_DOCUMENT_MANAGER (docman), page_num);
	
	if (!document) {
		g_warning ("Couldn't find document");
		return;
	}
	
        gtk_widget_grab_focus (GTK_WIDGET(document));
	
	set_current_document (docman, document);
}

static void
docman_recent_files (GdlRecent *recent,
		     const char *uri,
		     gpointer data)
{
	ScaffoldNotebookDocumentManager *docman;

	g_return_if_fail (data != NULL);
	g_return_if_fail (SCAFFOLD_IS_DOCUMENT_MANAGER (data));
	g_return_if_fail (uri != NULL);

	docman = SCAFFOLD_NOTEBOOK_DOCUMENT_MANAGER (data);

	scaffold_document_manager_open (SCAFFOLD_DOCUMENT_MANAGER (docman), 
					uri, NULL);
}

static void
docman_doc_modified (GtkWidget *widget, gpointer data)
{
	ScaffoldNotebookDocumentManager *docman;
	ScaffoldDocument *document;
	GtkWidget *label;
	const gchar *filename;
	gchar *new_name;

	g_return_if_fail (data != NULL);
	g_return_if_fail (SCAFFOLD_IS_DOCUMENT_MANAGER (data));

	g_message ("docman_doc_modified");

	docman = SCAFFOLD_NOTEBOOK_DOCUMENT_MANAGER (data);
	document = docman->current_document;

	/* Update commands. */
	bonobo_ui_component_set_prop (docman->ui_component,
				      "/commands/FileSave",
				      "sensitive", "1", NULL);
	bonobo_ui_component_set_prop (docman->ui_component, 
				      "/commands/FileRevert",
				      "sensitive", "1", NULL);

	/* Update label. */
	label = g_object_get_data (G_OBJECT (document), 
				   "ScaffoldNotebookDocumentManager::label");
	filename = gtk_label_get_label (GTK_LABEL (label));

	new_name = g_strdup_printf ("%s*", filename);
	gtk_label_set_label (GTK_LABEL (label), new_name);
	g_free (new_name);
}

static void
docman_doc_unmodified (GtkWidget *widget, gpointer data)
{
	ScaffoldNotebookDocumentManager *docman;
	ScaffoldDocument *document;
	GtkWidget *label;
	const gchar *filename;
	gchar *new_name;
	int length;

	g_return_if_fail (data != NULL);
	g_return_if_fail (SCAFFOLD_IS_DOCUMENT_MANAGER (data));

	docman = SCAFFOLD_NOTEBOOK_DOCUMENT_MANAGER (data);
	document = docman->current_document;

	/* Update commands. */
	bonobo_ui_component_set_prop (docman->ui_component,
				      "/commands/FileSave",
				      "sensitive", "0", NULL);
	bonobo_ui_component_set_prop (docman->ui_component, 
				      "/commands/FileRevert",
				      "sensitive", "0", NULL);

	/* Update label. */
	label = g_object_get_data (G_OBJECT (document), 
				   "ScaffoldNotebookDocumentManager::label");
	filename = gtk_label_get_label (GTK_LABEL (label));

	length = strlen (filename);
	if (length > 1 && filename[length - 1] == '*') {
		new_name = g_strndup (filename, length - 1);
		gtk_label_set_label (GTK_LABEL (label), new_name);
		g_free (new_name);
	}
}

static void
docman_doc_uri_changed (GtkWidget *widget, 
			const char *filename, 
			gpointer data)
{
        ScaffoldNotebookDocumentManager *docman;
        ScaffoldDocument *document;

        g_return_if_fail (SCAFFOLD_IS_DOCUMENT_MANAGER (data));
        g_return_if_fail (SCAFFOLD_IS_DOCUMENT (widget));

        docman = SCAFFOLD_NOTEBOOK_DOCUMENT_MANAGER (data);
        document = SCAFFOLD_DOCUMENT (widget);

        if (filename) {
                set_doc_uri (docman, document, filename);
		
		if (SCAFFOLD_BONOBO_DOCUMENT (document)->untitled) {
			SCAFFOLD_BONOBO_DOCUMENT (document)->untitled = FALSE;
			docman->priv->untitled_count--;
		}
        }
}

static void
docman_doc_destroy (GtkWidget *widget, gpointer data)
{
        ScaffoldNotebookDocumentManager *docman;
        ScaffoldDocument *document;
        gint length, i;
        
        g_return_if_fail(data);
        
        g_assert (SCAFFOLD_IS_DOCUMENT_MANAGER (data));
        g_assert (SCAFFOLD_IS_DOCUMENT (widget));	

        docman = SCAFFOLD_NOTEBOOK_DOCUMENT_MANAGER (data);
        document = SCAFFOLD_DOCUMENT (widget);

        length = scaffold_document_manager_num_documents
		(SCAFFOLD_DOCUMENT_MANAGER (docman));
        for (i=0; i < length; i++) {
                if ((gpointer)document
                     == g_list_nth_data(docman->documents, i)) {
                        docman->documents =
                                g_list_remove (docman->documents, document);
                        break;
                }
        }

	if (scaffold_document_manager_num_documents (SCAFFOLD_DOCUMENT_MANAGER (docman)) == 0) {
		set_current_document (docman, NULL);
		/*document = SCAFFOLD_DOCUMENT (scaffold_bonobo_document_new (docman->ui_container));
		scaffold_bonobo_document_make_temp (SCAFFOLD_BONOBO_DOCUMENT (document), "text/plain");
		add_doc (docman, document);*/
	}
}

static void
set_current_document (ScaffoldNotebookDocumentManager *docman, 
		      ScaffoldDocument *doc)
{
	BonoboControlFrame *frame;
	BonoboUIComponent *uic;
	CORBA_Environment ev;

	if (docman->current_document == doc) {
		return;
	}

	CORBA_exception_init (&ev);

	if (docman->current_document) {
		if (scaffold_bonobo_document_supports_modified (SCAFFOLD_BONOBO_DOCUMENT (docman->current_document))) {
			/* Disconnect from modified & unmodified signals. */
			g_signal_handlers_disconnect_matched (G_OBJECT (docman->current_document),
							      G_SIGNAL_MATCH_FUNC,
							      0,
							      0,
							      NULL,
							      docman_doc_modified,
							      NULL);

			g_signal_handlers_disconnect_matched (G_OBJECT (docman->current_document),
							      G_SIGNAL_MATCH_FUNC,
							      0,
							      0,
							      NULL,
							      docman_doc_unmodified,
							      NULL);
		}

		frame = bonobo_widget_get_control_frame (BONOBO_WIDGET (SCAFFOLD_BONOBO_DOCUMENT (docman->current_document)->bonobo_widget));

		/* Deactivate control. */
		bonobo_control_frame_control_deactivate (frame);
	}

	docman->current_document = doc;

	uic = docman->ui_component;

	if (doc && SCAFFOLD_BONOBO_DOCUMENT (doc)->bonobo_widget) {
		/* Activate control. */
		frame = bonobo_widget_get_control_frame (BONOBO_WIDGET (SCAFFOLD_BONOBO_DOCUMENT (doc)->bonobo_widget));
		bonobo_control_frame_control_activate (frame);

		if (scaffold_bonobo_document_supports_modified (SCAFFOLD_BONOBO_DOCUMENT (doc))) {
			/* Connect to modified & unmodified signals. */
			g_signal_connect (G_OBJECT (doc),
					  "modified",
					  G_CALLBACK (docman_doc_modified),
					  docman);

			g_signal_connect (G_OBJECT (doc),
					  "unmodified",
					  G_CALLBACK (docman_doc_unmodified),
					  docman);
		}

		if (uic) {
			char *value = (scaffold_bonobo_document_is_changed (SCAFFOLD_BONOBO_DOCUMENT (doc)) ||
				       !scaffold_bonobo_document_supports_modified (SCAFFOLD_BONOBO_DOCUMENT (doc))) ? "1" : "0";
			bonobo_ui_component_set_prop (uic, "/commands/FileSave",
						      "sensitive", value, NULL);
			bonobo_ui_component_set_prop (uic, "/commands/FileSaveAs",
						      "sensitive", "1", NULL);
			bonobo_ui_component_set_prop (uic, "/commands/FileSaveAll",
						      "sensitive", "1", NULL);
			bonobo_ui_component_set_prop (uic, "/commands/FileRevert",
						      "sensitive", value, NULL);
			bonobo_ui_component_set_prop (uic, "/commands/FileClose",
						      "sensitive", "1", NULL);
			bonobo_ui_component_set_prop (uic, "/commands/FileCloseAll",
						      "sensitive", "1", NULL);
		}
	} else if (uic) {
		bonobo_ui_component_set_prop (uic, "/commands/FileSave",
					      "sensitive", "0", NULL);
		bonobo_ui_component_set_prop (uic, "/commands/FileSaveAs",
					      "sensitive", "0", NULL);
		bonobo_ui_component_set_prop (uic, "/commands/FileSaveAll",
					      "sensitive", "0", NULL);
		bonobo_ui_component_set_prop (uic, "/commands/FileRevert",
					      "sensitive", "0", NULL);
		bonobo_ui_component_set_prop (uic, "/commands/FileClose",
					      "sensitive", "0", NULL);
		bonobo_ui_component_set_prop (uic, "/commands/FileCloseAll",
					      "sensitive", "0", NULL);
	}

	g_signal_emit_by_name (docman, "current_document_changed", doc);

	CORBA_exception_free (&ev);
}

static NewItemMenuData *
make_menu_data (ScaffoldNotebookDocumentManager *dm,
		const char *parent_uri,
		GnomeVFSFileInfo *file)
{
	NewItemMenuData *data;
	data = g_new0 (NewItemMenuData, 1);

	data->uri = g_strdup_printf ("%s/%s", parent_uri, file->name);
	data->label = g_strdup (
		gnome_vfs_mime_get_description 
		(file->mime_type));
	data->tip = g_strdup (_("Create a new file"));
	data->mime_type = g_strdup (file->mime_type);
	
	return data;
}

static void
new_item_menu_data_free (NewItemMenuData *data)
{
	g_free (data->uri);
	g_free (data->label);
	g_free (data->tip);
	
	g_free (data);
}

static void
load_template_dir (ScaffoldNotebookDocumentManager *dm, 
		   const char *uri)
{
	GnomeVFSDirectoryHandle *dir;
	GnomeVFSResult result;

	result = gnome_vfs_directory_open (&dir, uri, 
					   GNOME_VFS_FILE_INFO_GET_MIME_TYPE);
	if (result == GNOME_VFS_OK) {
		GnomeVFSFileInfo *file_info = gnome_vfs_file_info_new ();

		
		while (gnome_vfs_directory_read_next (dir, file_info)
		       == GNOME_VFS_OK) {

			if (file_info->type == GNOME_VFS_FILE_TYPE_REGULAR 
			    && file_info->name[0] != '.'
			    && file_info->name[strlen (file_info->name) - 1] != '~') {
				
				NewItemMenuData *old_data;
				NewItemMenuData *data = 
					make_menu_data (dm, uri, file_info);

				old_data = 
					g_hash_table_lookup (dm->priv->templates,
							     file_info->name);

				if (old_data) {
					new_item_menu_data_free (old_data);
				}
				
				g_hash_table_insert (dm->priv->templates,
						     file_info->name,
						     data);
			}
		}

		gnome_vfs_file_info_unref (file_info);
		gnome_vfs_directory_close (dir);
	}
}

static void
new_file_cb (BonoboUIComponent *component, 
	     gpointer user_data, 
	     const char *cname)
{
	ScaffoldNotebookDocumentManager *dm = SCAFFOLD_NOTEBOOK_DOCUMENT_MANAGER (user_data);
	NewItemMenuData *data;
	
	data = g_hash_table_lookup (dm->priv->verbs, cname);
	if (data) {
		ScaffoldDocument *doc;
		doc = SCAFFOLD_DOCUMENT (scaffold_bonobo_document_new (dm->ui_container));
		
		if (scaffold_bonobo_document_load_template (SCAFFOLD_BONOBO_DOCUMENT (doc), data->uri)) {
			add_doc (dm, doc);
		} else {
			g_object_unref (doc);
		}
	}
}

static void
add_to_menu (char *name, NewItemMenuData *data, ScaffoldNotebookDocumentManager *dm)
{
	static unsigned long verbnum = 0;
	char *xml;
	GdkPixbuf *pixbuf;
	char *pixdata = NULL;
	char *verb = g_strdup_printf ("New%ld", verbnum++);

	pixbuf = gdl_icons_get_mime_icon (dm->priv->icons, data->mime_type);
	if (pixbuf) {
		pixdata = bonobo_ui_util_pixbuf_to_xml (pixbuf);
		g_object_unref (pixbuf);
	}

	xml = g_strdup_printf ("<menuitem name=\"%s\" verb=\"\" label=\"%s\""
			       " tip=\"%s\" %s pixname=\"%s\"/>", 
			       verb, data->label, data->tip,
			       pixdata ? "pixtype=\"pixbuf\"" : "",
			       pixdata ? pixdata : "");
	g_free (pixdata);
	
	bonobo_ui_component_set (dm->ui_component,
				 "/menu/File/FileNew/FileNewTemplates",
				 xml,
				 NULL);

	bonobo_ui_component_add_verb (dm->ui_component,
				      verb, (BonoboUIVerbFn)new_file_cb,
				      dm);

	g_hash_table_insert (dm->priv->verbs, verb, data);
	
	g_free (xml);
}

static void
make_new_menu (ScaffoldNotebookDocumentManager *dm)
{
	g_hash_table_foreach (dm->priv->templates, (GHFunc)add_to_menu, dm);
}

static void
load_new_menu (ScaffoldNotebookDocumentManager *dm)
{
	char *dir;
	
	/* FIXME: You should be able to unload and reload this menu, but
	 * at the moment you can't */
	g_return_if_fail (dm->priv->templates == NULL);
	
	dm->priv->templates = g_hash_table_new (g_str_hash, g_str_equal);
	dm->priv->verbs = g_hash_table_new (g_str_hash, g_str_equal);

	load_template_dir (dm, "file://" SCAFFOLD_DATADIR "/scaffold/templates");

	dir = g_strdup_printf ("%s/.scaffold/templates", g_get_home_dir ());
	load_template_dir (dm, dir);
	g_free (dir);

	make_new_menu (dm);
}

GtkWidget *
scaffold_notebook_document_manager_get_prefs_page (void)
{
	GladeXML *gui;
	GtkWidget *vbox, *tab, *backup, *autosave, *interval;

	gui = glade_xml_new (GLADEDIR "scaffold-document-manager.glade", 
			     "document-preferences", NULL);

	if (!gui) {
		g_warning ("Could not load scaffold-document-manager.glade, reinstall scaffold");
		return NULL;
	}

	vbox = glade_xml_get_widget (gui, "document-preferences");
	tab = glade_xml_get_widget (gui, "tab-optionmenu");
	backup = glade_xml_get_widget (gui, "backup-checkbutton");
	autosave = glade_xml_get_widget (gui, "autosave-checkbutton");
	interval = glade_xml_get_widget (gui, "autosave-spinbutton");
	g_object_unref (gui);

	gconf_peditor_new_select_menu_with_enum (NULL, SCAFFOLD_DOCMAN_TAB_LOCATION,
						 tab, gtk_position_type_get_type (),
						 NULL);

	gtk_widget_show_all (vbox);

	return vbox;
}

static ScaffoldDocument *
scaffold_notebook_document_manager_new_document (ScaffoldDocumentManager *docman,
					       const char *mime_type,
					       GError *error)
{
	ScaffoldDocument *doc;
	
	doc = SCAFFOLD_DOCUMENT (scaffold_bonobo_document_new (SCAFFOLD_NOTEBOOK_DOCUMENT_MANAGER (docman)->ui_container));
	scaffold_bonobo_document_make_temp (SCAFFOLD_BONOBO_DOCUMENT (doc), 
					  mime_type);
	add_doc (SCAFFOLD_NOTEBOOK_DOCUMENT_MANAGER (docman), doc );
	
	return doc;
}

static ScaffoldDocument *
scaffold_notebook_document_manager_open (ScaffoldDocumentManager *scaffold_docman,
				       const char *string_uri,
				       GError *error)
{
	ScaffoldNotebookDocumentManager *docman = SCAFFOLD_NOTEBOOK_DOCUMENT_MANAGER (scaffold_docman);
	ScaffoldDocument *document;
	GnomeVFSURI *uri;
	
	uri = gnome_vfs_uri_new (string_uri);
	if (!gnome_vfs_uri_exists (uri)) {
		char *msg;
		msg = g_strdup_printf (_("Could not find file '%s'"), string_uri);
		g_warning ("Could not find file '%s'", string_uri);
		scaffold_dialog_error (msg);
		g_free (msg);
		gnome_vfs_uri_unref (uri);

		return NULL;
	}
	gnome_vfs_uri_unref (uri);

	/* Only open the file if its not already opened. */
	document = scaffold_document_manager_get_document_for_uri (scaffold_docman,
								 string_uri,
								 TRUE,
								 NULL);
	if (document) {
		scaffold_document_manager_show_document (scaffold_docman, 
						       document);
		return document;
	}

	document = docman->current_document;

	if (document != NULL && (!scaffold_bonobo_document_is_untitled (SCAFFOLD_BONOBO_DOCUMENT (document)) ||
				 scaffold_bonobo_document_is_changed (SCAFFOLD_BONOBO_DOCUMENT (document)))) {
		document = SCAFFOLD_DOCUMENT (scaffold_bonobo_document_new (docman->ui_container));
		if (scaffold_bonobo_document_load_uri (SCAFFOLD_BONOBO_DOCUMENT (document), string_uri)) {
			add_doc (docman, document);
		} else {
			g_object_unref (G_OBJECT (document));
			document = NULL;
		}
	} else {
		scaffold_bonobo_document_load_uri
			(SCAFFOLD_BONOBO_DOCUMENT (document), string_uri);
		SCAFFOLD_BONOBO_DOCUMENT (document)->untitled = FALSE;
		docman->priv->untitled_count--;
		set_current_document(docman, document);
	}

	return document;
}

static void
scaffold_notebook_document_manager_close (ScaffoldDocumentManager *docman,
					  ScaffoldDocument *doc,
					  GError *error)
{
	ScaffoldNotebookDocumentManager *nbdocman;

	nbdocman = SCAFFOLD_NOTEBOOK_DOCUMENT_MANAGER (docman);

	if (scaffold_bonobo_document_is_changed (SCAFFOLD_BONOBO_DOCUMENT (doc))) {
		if (file_close_dialog (nbdocman, doc) == GTK_RESPONSE_CANCEL) {
			return;
		}
	}

	scaffold_notebook_document_manager_remove_doc (nbdocman, doc);

	/* Always have at least 1 document open. */
	if (!nbdocman->documents) {
		scaffold_document_manager_new_document (docman,
							"text/plain",
							NULL);
	}
}

static void
scaffold_notebook_document_manager_close_all (ScaffoldDocumentManager *docman,
					      GError *error)
{
	ScaffoldNotebookDocumentManager *nbdocman;
	GList *l;
	ScaffoldDocument *doc;

	nbdocman = SCAFFOLD_NOTEBOOK_DOCUMENT_MANAGER (docman);

	for (l = nbdocman->documents; l; l = l->next) {
		doc = SCAFFOLD_DOCUMENT (l->data);

		if (scaffold_bonobo_document_is_changed (SCAFFOLD_BONOBO_DOCUMENT (doc))) {
			if (file_close_dialog (nbdocman, doc) == GTK_RESPONSE_CANCEL) {
				return;
			}
		}
		scaffold_notebook_document_manager_close (docman, doc, error);
	}
}

static void
scaffold_notebook_document_manager_save_all (ScaffoldDocumentManager *docman,
					     GError *error)
{
	ScaffoldNotebookDocumentManager *nbdocman;
	GList *l;

	nbdocman = SCAFFOLD_NOTEBOOK_DOCUMENT_MANAGER (docman);

	for (l = nbdocman->documents; l != NULL; l = l->next) {
		ScaffoldDocument *doc = l->data;
		
		if (scaffold_bonobo_document_is_changed (SCAFFOLD_BONOBO_DOCUMENT (doc))) {
			scaffold_document_manager_show_document (docman, doc);
			scaffold_document_save (doc, NULL);
		}
	}
}

static int
scaffold_notebook_document_manager_num_documents (ScaffoldDocumentManager *docman)
{
	g_return_val_if_fail (docman != NULL, -1);
	g_return_val_if_fail (SCAFFOLD_IS_DOCUMENT_MANAGER (docman), -1);

	return g_list_length (SCAFFOLD_NOTEBOOK_DOCUMENT_MANAGER (docman)->documents);
}

static ScaffoldDocument *
scaffold_notebook_document_manager_get_nth_document (ScaffoldDocumentManager *docman,
						   int index)
{
	return SCAFFOLD_DOCUMENT (gtk_notebook_get_nth_page (GTK_NOTEBOOK (docman), index));
}

static ScaffoldDocument *
scaffold_notebook_document_manager_get_document_for_uri   (ScaffoldDocumentManager *docman,
							 const char *uri,
							 gboolean existing_only,
							 GError *error)
{
	GList *l;
	
 	g_return_val_if_fail (docman != NULL, NULL);
	g_return_val_if_fail (SCAFFOLD_IS_DOCUMENT_MANAGER (docman), NULL);
	g_return_val_if_fail (uri != NULL, NULL);

	for (l = SCAFFOLD_NOTEBOOK_DOCUMENT_MANAGER (docman)->documents; 
	     l != NULL;
	     l = l->next) {
		ScaffoldDocument *document;
		const char *doc_uri;

		document = SCAFFOLD_DOCUMENT (l->data);
		doc_uri = scaffold_document_get_uri (document);

		if (doc_uri && !strcmp(doc_uri, uri)) {
			return document;
		}
	}
	
	if (existing_only) {
		return NULL;
	} else {
		return scaffold_document_manager_open (docman, uri, error);
	}
}

static void
scaffold_notebook_document_manager_show_document (ScaffoldDocumentManager *docman, 
						ScaffoldDocument *document)
{
	int index;
	
 	g_return_if_fail (docman != NULL);
	g_return_if_fail (SCAFFOLD_IS_DOCUMENT_MANAGER (docman));
	g_return_if_fail (document != NULL);
	g_return_if_fail (SCAFFOLD_IS_DOCUMENT (document));
	
	index = gtk_notebook_page_num (GTK_NOTEBOOK (docman), 
				       GTK_WIDGET (document));
	if (index > -1) {
		gtk_notebook_set_current_page (GTK_NOTEBOOK (docman), index);
	}
}

static ScaffoldDocument *
scaffold_notebook_document_manager_get_current_document (ScaffoldDocumentManager *docman)
{
	return SCAFFOLD_NOTEBOOK_DOCUMENT_MANAGER (docman)->current_document;
}

static void
scaffold_document_manager_iface_init (ScaffoldDocumentManagerIface *iface)
{
	iface->new_document = scaffold_notebook_document_manager_new_document;
	iface->open = scaffold_notebook_document_manager_open;
	iface->close = scaffold_notebook_document_manager_close;
	iface->close_all = scaffold_notebook_document_manager_close_all;
	iface->save_all = scaffold_notebook_document_manager_save_all;
	iface->num_documents = scaffold_notebook_document_manager_num_documents;
	iface->get_nth_document = scaffold_notebook_document_manager_get_nth_document;
	iface->get_document_for_uri = scaffold_notebook_document_manager_get_document_for_uri;
	iface->show_document = scaffold_notebook_document_manager_show_document;
	iface->get_current_document = scaffold_notebook_document_manager_get_current_document;
}

SCAFFOLD_TYPE_BEGIN (ScaffoldNotebookDocumentManager, 
		   scaffold_notebook_document_manager,
		   GTK_TYPE_NOTEBOOK)
SCAFFOLD_INTERFACE (scaffold_document_manager, SCAFFOLD_TYPE_DOCUMENT_MANAGER)
SCAFFOLD_TYPE_END
