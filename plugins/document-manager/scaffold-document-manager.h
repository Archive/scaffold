/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * scaffold-document-manager.h
 * 
 * Copyright (C) 2000-2002 Dave Camp
 * Copyright (C) 2003 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __SCAFFOLD_NOTEBOOK_DOCUMENT_MANAGER_H__
#define __SCAFFOLD_NOTEBOOK_DOCUMENT_MANAGER_H__

#include <gtk/gtknotebook.h>
#include <libbonoboui.h>
#include <libscaffold/scaffold-document.h>

G_BEGIN_DECLS
 
typedef struct _ScaffoldNotebookDocumentManager      ScaffoldNotebookDocumentManager;
typedef struct _ScaffoldNotebookDocumentManagerClass ScaffoldNotebookDocumentManagerClass;
typedef struct _ScaffoldNotebookDocumentManagerPriv  ScaffoldNotebookDocumentManagerPriv;

#define SCAFFOLD_TYPE_NOTEBOOK_DOCUMENT_MANAGER        (scaffold_notebook_document_manager_get_type ())
#define SCAFFOLD_NOTEBOOK_DOCUMENT_MANAGER(o)          (GTK_CHECK_CAST ((o), SCAFFOLD_TYPE_NOTEBOOK_DOCUMENT_MANAGER, ScaffoldNotebookDocumentManager))
#define SCAFFOLD_NOTEBOOK_DOCUMENT_MANAGER_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), SCAFFOLD_TYPE_NOTEBOOK_DOCUMENT_MANAGER, ScaffoldNotebookDocumentManagerClass))
#define SCAFFOLD_IS_NOTEBOOK_DOCUMENT_MANAGER(o)       (GTK_CHECK_TYPE ((o), SCAFFOLD_TYPE_NOTEBOOK_DOCUMENT_MANAGER))
#define SCAFFOLD_IS_NOTEBOOK_DOCUMENT_MANAGER_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), SCAFFOLD_TYPE_NOTEBOOK_DOCUMENT_MANAGER))

struct _ScaffoldNotebookDocumentManager {
	GtkNotebook parent;

	ScaffoldDocument *current_document;

	GList *documents;
	BonoboUIComponent *ui_component;
	Bonobo_UIContainer ui_container;

	ScaffoldNotebookDocumentManagerPriv *priv;
};

struct _ScaffoldNotebookDocumentManagerClass {
	GtkNotebookClass parent_class;

	void (* document_added)   (ScaffoldNotebookDocumentManager *docman,
				   ScaffoldDocument                *document);
	void (* document_removed) (ScaffoldNotebookDocumentManager *docman,
				   ScaffoldDocument                *document);
};

GType      scaffold_notebook_document_manager_get_type       (void);
GtkWidget *scaffold_notebook_document_manager_new            (Bonobo_UIContainer ui_container,
							      BonoboUIComponent *ui_component);

GtkWidget *scaffold_notebook_document_manager_get_prefs_page (void);

void       scaffold_notebook_document_manager_remove_doc     (ScaffoldNotebookDocumentManager *docman,
							      ScaffoldDocument  *document);

void       scaffold_notebook_document_manager_add_editor_ui  (ScaffoldNotebookDocumentManager *docman,
							      const char        *name,
							      const char        *mime_types,
							      const char        *datadir,
							      const char        *xmlfile,
							      BonoboUIVerb      *verbs,
							      gpointer           user_data);
void       scaffold_notebook_document_manager_remove_editor_ui (ScaffoldNotebookDocumentManager *docman,
								const char      *name);

G_END_DECLS

#endif /* __SCAFFOLD_NOTEBOOK_DOCUMENT_MANAGER_H__ */
