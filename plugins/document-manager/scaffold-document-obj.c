/* Scaffold
 * 
 * Copyright (C) 2001 Dave Camp <dave@ximian.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */

#include <config.h>
#include "scaffold-document-obj.h"
#include <libscaffold/libscaffold.h>
#include "scaffold-document.h"
#include <libbonobo.h>
#include <libgnome/gnome-macros.h>

#define PARENT_TYPE BONOBO_X_OBJECT_TYPE

BONOBO_BOILERPLATE (ScaffoldDocumentObj, scaffold_document_obj, 
		    GNOME_Development_Document, 
		    BonoboObject, BONOBO_TYPE_OBJECT, 
		    BONOBO_REGISTER_TYPE_FULL);

ScaffoldDocumentObj *
scaffold_document_obj_construct (ScaffoldDocumentObj *docobj, ScaffoldDocument *doc)
{
	docobj->doc = doc;

	return docobj;
}

ScaffoldDocumentObj *
scaffold_document_obj_new (ScaffoldDocument *doc)
{
	ScaffoldDocumentObj *docobj = g_object_new (SCAFFOLD_DOCUMENT_OBJ_TYPE,
						  NULL);

	return scaffold_document_obj_construct (docobj, doc);
}

/* implementation functions */

static char *
impl_get_filename (PortableServer_Servant servant,
		   CORBA_Environment *ev)
{
	ScaffoldDocumentObj *docobj = SCAFFOLD_DOCUMENT_OBJ (bonobo_object_from_servant (servant));
	char *filename = scaffold_document_get_filename (docobj->doc);
	filename = filename ? filename : "";
	
	return CORBA_string_dup (filename);
}

static char *
impl_get_mime_type (PortableServer_Servant servant,
		    CORBA_Environment *ev)
{
	ScaffoldDocumentObj *docobj = SCAFFOLD_DOCUMENT_OBJ (bonobo_object_from_servant (servant));

	return CORBA_string_dup (scaffold_document_get_mime_type (docobj->doc));
}

static Bonobo_Control
impl_get_editor (PortableServer_Servant servant,
		 CORBA_Environment *ev)
{
	ScaffoldDocumentObj *docobj = SCAFFOLD_DOCUMENT_OBJ (bonobo_object_from_servant (servant));
	Bonobo_Control ctrl;
	
	ctrl = scaffold_document_get_control (SCAFFOLD_DOCUMENT (docobj->doc));
	
	return bonobo_object_dup_ref (ctrl, ev);
}

static Bonobo_Unknown
impl_get_editor_interface (PortableServer_Servant servant,
			   const CORBA_char *repo_id,
			   CORBA_Environment *ev)
{
	ScaffoldDocumentObj *docobj = SCAFFOLD_DOCUMENT_OBJ (bonobo_object_from_servant (servant));
	Bonobo_Control ctrl;
	Bonobo_Unknown obj;
	

	ctrl = scaffold_document_get_control (SCAFFOLD_DOCUMENT (docobj->doc));
	obj = Bonobo_Unknown_queryInterface (ctrl, repo_id, ev);
	
	return obj;
}

static void 
scaffold_document_obj_finalize (GObject *object)
{
	GNOME_CALL_PARENT (G_OBJECT_CLASS, finalize, (object));
}

static void
scaffold_document_obj_class_init (ScaffoldDocumentObjClass *klass)
{
	GObjectClass *object_class = (GObjectClass *)klass;
	POA_GNOME_Development_Document__epv *epv = &klass->epv;
	parent_class = g_type_class_peek_parent (klass);
	object_class->finalize = scaffold_document_obj_finalize;

	epv->getFilename = impl_get_filename;
	epv->getMimeType = impl_get_mime_type;
	epv->getEditor = impl_get_editor;
	epv->getEditorInterface = impl_get_editor_interface;
}

static void 
scaffold_document_obj_instance_init (ScaffoldDocumentObj *bs)
{
}


