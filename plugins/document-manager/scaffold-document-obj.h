/* Scaffold
 * 
 * Copyright (C) 2001 Dave Camp <dave@ximian.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.  
 */


#ifndef SCAFFOLD_DOCUMENT_OBJ_H
#define SCAFFOLD_DOCUMENT_OBJ_H

#include <bonobo/bonobo-object.h>
#include <bonobo/bonobo-event-source.h>
#include <libscaffold/libscaffold.h>

G_BEGIN_DECLS

#define SCAFFOLD_DOCUMENT_OBJ_TYPE                (scaffold_document_obj_get_type ())
#define SCAFFOLD_DOCUMENT_OBJ(o)                  (GTK_CHECK_CAST ((o), SCAFFOLD_DOCUMENT_OBJ_TYPE, ScaffoldDocumentObj))
#define SCAFFOLD_DOCUMENT_OBJ_CLASS(k)            (GTK_CHECK_CLASS_CAST((k), SCAFFOLD_DOCUMENT_OBJ_TYPE, ScaffoldDocumentObjClass))
#define SCAFFOLD_IS_DOCUMENT_OBJ(o)               (GTK_CHECK_TYPE ((o), SCAFFOLD_DOCUMENT_OBJ_TYPE))
#define SCAFFOLD_IS_DOCUMENT_OBJ_CLASS(k)         (GTK_CHECK_CLASS_TYPE ((k), SCAFFOLD_DOCUMENT_OBJ_TYPE))

typedef struct _ScaffoldDocumentObj      ScaffoldDocumentObj;

struct _ScaffoldDocumentObj {
	BonoboObject parent;
	struct _ScaffoldDocument *doc;
};

typedef struct {
	BonoboObjectClass parent;
	POA_GNOME_Development_Document__epv epv;
} ScaffoldDocumentObjClass;

GType              scaffold_document_obj_get_type     (void);
ScaffoldDocumentObj *scaffold_document_obj_new          (struct _ScaffoldDocument *scaffold_doc);
ScaffoldDocumentObj *scaffold_document_obj_construct    (ScaffoldDocumentObj *doc,
						     struct _ScaffoldDocument *scaffold_doc);

G_END_DECLS

#endif
