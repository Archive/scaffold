/* Scaffold
 * Copyright (C) 1998-2000 Steffen Kern
 *               2000-2001 Dave Camp <dave@ximian.com>
 *               2001 Ximian, Inc.
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <bonobo/bonobo-stream-memory.h>
#include <fcntl.h>
#include <bonobo-activation/bonobo-activation.h>
#include <libscaffold/scaffold-utils.h>
#include <libscaffold/scaffold-document.h>
#include <libgnome/gnome-macros.h>
#include <libgnome/gnome-exec.h>
#include <libgnomeui/gnome-entry.h>
#include <libgnomevfs/gnome-vfs.h>
#include <libgnomevfs/gnome-vfs-mime-utils.h>
#include <libgnomevfs/gnome-vfs-mime-handlers.h>
#include <bonobo/bonobo-file-selector-util.h>
#include "bonobo-stream-vfs.h"

#include "scaffold-document.h"

typedef struct {
	char *name;
	char *iid;
	guint menu_item;
	gboolean editable;
} AvailableComponent;

struct _ScaffoldBonoboDocumentPrivate {
	GtkWidget *viewer_option;
	GtkWidget *app_option;
	GList *available_components;
	AvailableComponent *current_component;
	GList *available_apps;
	BonoboStream *saved_stream;
	gboolean supports_modified;
};

/* Prototypes */
static void scaffold_bonobo_document_dispose (GObject *object);

static gboolean load_uri_contents (ScaffoldBonoboDocument *doc, const char *uri);
static gboolean load_uri (ScaffoldBonoboDocument *doc, const char *uri);
static void unload_uri (ScaffoldBonoboDocument *doc);

static void load_mime (ScaffoldBonoboDocument *doc, const char *mime);
static void unload_mime (ScaffoldBonoboDocument *doc);

static void update_viewers (ScaffoldBonoboDocument *document);
static void unload_viewers (ScaffoldBonoboDocument *document);

static void activate_component (ScaffoldBonoboDocument *document, AvailableComponent *c);
static void unload_component (ScaffoldBonoboDocument *document);

static void change_component (ScaffoldBonoboDocument *document, AvailableComponent *v);
static void load_uri_into_control (ScaffoldBonoboDocument *document, const char *uri);
static void save_uri_from_control (ScaffoldBonoboDocument *document, const char *uri);

static gboolean create_editor_widget (ScaffoldBonoboDocument *document, 
				      AvailableComponent *v);
static AvailableComponent *choose_default_component (ScaffoldBonoboDocument *document);
static void set_default_clicked_cb (GtkWidget *btn, ScaffoldBonoboDocument *document);

static void scaffold_bonobo_document_dirty (BonoboListener    *listener,
				   const char        *event_name, 
				   const CORBA_any   *any,
				   CORBA_Environment *ev,
				   gpointer           user_data);

gpointer parent_class;

static void
scaffold_bonobo_document_class_init (ScaffoldBonoboDocumentClass *class)
{
	GObjectClass *object_class;
	
	object_class = (GObjectClass*) class;

	object_class->dispose = scaffold_bonobo_document_dispose;
}

static void
scaffold_bonobo_document_instance_init (ScaffoldBonoboDocument *document)
{
	GtkWidget *hbox;
	GtkWidget *btn;

	document->priv = g_new0 (ScaffoldBonoboDocumentPrivate, 1);

	document->file_loaded = FALSE;
	document->uri = NULL;
	document->persist_stream = CORBA_OBJECT_NIL;
	document->untitled = FALSE;
	document->priv->supports_modified = FALSE;

	hbox = gtk_hbox_new (FALSE, 5);
	gtk_box_pack_end (GTK_BOX (document), hbox, FALSE, FALSE, 0);

	btn = gtk_button_new_with_label (_("Make Default"));
	gtk_box_pack_end (GTK_BOX (hbox), btn, FALSE, FALSE, 0);
	g_signal_connect (GTK_OBJECT (btn), "clicked", 
			    G_CALLBACK (set_default_clicked_cb), 
			    document);

	document->priv->viewer_option = gtk_option_menu_new ();
	gtk_box_pack_end (GTK_BOX (hbox), document->priv->viewer_option,
			  FALSE, FALSE, 0);

	gtk_widget_show_all (hbox);
}

GtkWidget *
scaffold_bonobo_document_new (Bonobo_UIContainer ui_container)
{
	CORBA_Environment ev;
	ScaffoldBonoboDocument *document;

	document = g_object_new (SCAFFOLD_TYPE_BONOBO_DOCUMENT, NULL);
	document->ui_container = ui_container;

	CORBA_exception_init (&ev);
	Bonobo_Unknown_ref (ui_container, &ev);
	CORBA_exception_free (&ev);

	return GTK_WIDGET( document );
}

void
scaffold_bonobo_document_make_temp (ScaffoldBonoboDocument *document, const gchar  *mime_type)
{
	if (document->file_loaded) {
		unload_uri (document);
	}

	document->mime_type = g_strdup (mime_type);
	
	update_viewers (document);

	if (document->priv->available_components) {
		activate_component (document, 
				    choose_default_component (document));
	} else {
		/* No Bonobo Control to view document, tell user */
		char *msg = g_strdup_printf ("Document-Manager\nFailed to make "
			"temp file - consider installing glimmer from CVS\nNo available"
			" bonobo objects to open this file.\nMime Type: %s", mime_type);
		scaffold_dialog_error (msg);
		g_free (msg);
	}
}

gboolean
scaffold_bonobo_document_load_template (ScaffoldBonoboDocument *document, const char *uri)
{
	g_return_val_if_fail (document != NULL, FALSE);
	g_return_val_if_fail (uri != NULL, FALSE);
	
	return load_uri_contents (document, uri);
}

gboolean
scaffold_bonobo_document_load_uri (ScaffoldBonoboDocument *document, const char *uri)
{
	g_return_val_if_fail (document != NULL, FALSE);
	g_return_val_if_fail (uri != NULL, FALSE);

	return load_uri (document, uri);
}

void
scaffold_bonobo_document_reload (ScaffoldBonoboDocument *doc)
{
	char *uri;

	if (!doc->uri) {
		return;
	}

	if (scaffold_dialog_question (_("The file has been changed.\nDo you want to reload it?")) == GTK_RESPONSE_NO) {
		return;
	}
	
	uri = g_strdup (doc->uri);
	scaffold_bonobo_document_load_uri (doc, uri);
	g_free (uri);
}

void
scaffold_bonobo_document_save_uri (ScaffoldBonoboDocument *document, 
				 const char *uri)
{
	const char *mime_type;
	char *uri_cpy;
	gboolean uri_changed = TRUE;

	/* There's a pretty good chance the uri passed in was the uri
	 * we are about to free, so we save a copy */
	uri_cpy = g_strdup (uri);

	if (document->uri) {
		uri_changed = strcmp (document->uri, uri_cpy);
		g_free (document->uri);
	}
	document->uri = g_strdup (uri_cpy);

	save_uri_from_control (document, uri_cpy);

	/* If the file changed mime types, we reload to get the mime stuff
	 * right */
	mime_type = gnome_vfs_get_mime_type (uri_cpy);
	if (strcmp (document->mime_type, mime_type)) {
		load_uri (document, uri_cpy);
	} else if (uri_changed) {
		g_signal_emit_by_name (document, "uri_changed", document->uri);
	}

	g_free (uri_cpy);
}

void
scaffold_bonobo_document_save_as_dialog (ScaffoldBonoboDocument *document)
{
	char *new_name;
	
	new_name = bonobo_file_selector_save (NULL, TRUE, NULL, NULL,
					      NULL, document->uri);
	
	if (new_name) {
		scaffold_bonobo_document_save_uri (document, new_name);
		g_free (new_name);
	}
}

/*
 * this function checks if a document has been changed from
 * outside (i.e. the file mod time has changed)
 * signal: 0 - not changed
 * 	   1 - changed
 *	   2 - does not (longer) exist
 */
void
scaffold_bonobo_document_check_changed (ScaffoldBonoboDocument *document)
{
}

gboolean
scaffold_bonobo_document_is_changed (ScaffoldBonoboDocument *document)
{
	g_return_val_if_fail (document != NULL, FALSE);

	return Bonobo_Persist_isDirty (document->persist_stream, NULL);
}

void
scaffold_bonobo_document_set_changed_state( ScaffoldBonoboDocument *document, gboolean state )
{
}

void
scaffold_bonobo_document_set_readonly_state( ScaffoldBonoboDocument *document, gboolean state )
{
}

static void
scaffold_bonobo_document_dirty (BonoboListener    *listener,
		       const char        *event_name, 
		       const CORBA_any   *any,
		       CORBA_Environment *ev,
		       gpointer           user_data)
{
	ScaffoldBonoboDocument *document;

	g_return_if_fail (user_data != NULL);
	g_return_if_fail (SCAFFOLD_IS_DOCUMENT (user_data));

	document = SCAFFOLD_BONOBO_DOCUMENT (user_data);

	if (BONOBO_ARG_GET_BOOLEAN (any)) {
		g_signal_emit_by_name (document, "modified");
	} else {
		g_signal_emit_by_name (document, "unmodified");
	}
}

static void
scaffold_bonobo_document_dispose (GObject *object)
{
	ScaffoldBonoboDocument *doc = SCAFFOLD_BONOBO_DOCUMENT (object);
	
	if (doc->ui_container != CORBA_OBJECT_NIL) {
		if (doc->file_loaded)
			unload_uri (doc);
		else
			unload_component (doc);

		Bonobo_Unknown_unref (doc->ui_container, NULL);
		doc->ui_container = CORBA_OBJECT_NIL;
	}	
}

gboolean
scaffold_bonobo_document_is_readonly(
	ScaffoldBonoboDocument*			document
)
{
	return FALSE;
}


gboolean
scaffold_bonobo_document_is_busy(
	ScaffoldBonoboDocument*			document
)
{
	return FALSE;
}

void
scaffold_bonobo_document_set_busy_state(
	ScaffoldBonoboDocument*			document,
	gboolean			busy
)
{
}

void
scaffold_bonobo_document_set_last_mod(
	ScaffoldBonoboDocument*			document,
	gint				last_mod
)
{
}

gboolean
scaffold_bonobo_document_is_untitled (ScaffoldBonoboDocument *document)
{
	g_return_val_if_fail (document != NULL, FALSE);

	return document->untitled;
}

void 
scaffold_bonobo_document_set_cfg_values (ScaffoldBonoboDocument *document)
{
}

gboolean
scaffold_bonobo_document_supports_modified (ScaffoldBonoboDocument *document)
{
	g_return_val_if_fail (document != NULL, FALSE);

	return document->priv->supports_modified;
}

static gboolean
load_uri_contents (ScaffoldBonoboDocument *document, const char *uri)
{
	const char *mime_type;

	if (document->file_loaded)
		unload_uri (document);

	mime_type = gnome_vfs_get_mime_type (uri);

	load_mime (document, mime_type);

	if (document->priv->available_components) {
		activate_component (document, 
				    choose_default_component (document));
		load_uri_into_control (document, uri);
	} else {
		/* No Bonobo Control to view document, tell user */
		char *msg = g_strdup_printf ("Document-Manager\nNo available bonobo"
			" objects to open this file.\nMime Type: %s", mime_type);
		scaffold_dialog_error (msg);
		g_free (msg);

		return FALSE;
	}
	return TRUE;
}


static gboolean
load_uri (ScaffoldBonoboDocument *document, const char *uri)
{
	if (load_uri_contents (document, uri)) {
		document->uri = g_strdup (uri);
		
		document->file_loaded = TRUE;

		g_signal_emit_by_name (document, "uri_changed", uri);

		return TRUE;
	}
	
	return FALSE;
}

static void
unload_uri (ScaffoldBonoboDocument *document)
{
	unload_component (document);

	unload_mime (document);

	if (document->uri) {
		g_free (document->uri);
		document->uri = NULL;
	}

	if (document->priv->saved_stream) {
		bonobo_object_unref (BONOBO_OBJECT (document->priv->saved_stream));
		document->priv->saved_stream = FALSE;
	}
	
	document->file_loaded = FALSE;

}

static void 
load_mime (ScaffoldBonoboDocument *document, const char *mime_type)
{
	if (document->mime_type) {
		unload_mime (document);
	}
	document->mime_type = g_strdup (mime_type);

	update_viewers (document);	
}

static void
unload_mime (ScaffoldBonoboDocument *document)
{
	if (document->mime_type) {
		unload_viewers (document);
		g_free (document->mime_type);
		document->mime_type = NULL;
	}
}

static BonoboStream *
get_working_copy (ScaffoldBonoboDocument *document)
{
	BonoboStream *ret = NULL;
	CORBA_Environment ev;
	CORBA_exception_init (&ev);

	if (!document->priv->current_component->editable
	    && document->priv->saved_stream) {
		ret = document->priv->saved_stream;
		document->priv->saved_stream = NULL;
	} else if (document->priv->current_component->editable
		   && !CORBA_Object_is_nil (document->persist_stream, &ev)) {
 		ret = bonobo_stream_mem_create (NULL, 0, 
						FALSE, TRUE);
		Bonobo_PersistStream_save (document->persist_stream,
					   bonobo_object_corba_objref (BONOBO_OBJECT (ret)),
					   document->mime_type,
					   &ev);
		if (ev._major != CORBA_NO_EXCEPTION) {
			bonobo_object_unref (BONOBO_OBJECT (ret));
			ret = NULL;
		}
	} 
	
	if (!ret) {
		/* Prompt to save */
 	}

	if (ret) {
		Bonobo_Stream_seek (bonobo_object_corba_objref (BONOBO_OBJECT (ret)),
				    0, Bonobo_Stream_SeekSet, &ev);
	}
	
	CORBA_exception_free (&ev);
	return ret;
}

static void
set_working_copy (ScaffoldBonoboDocument *document, BonoboStream *stream)
{
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	if (stream && !CORBA_Object_is_nil (document->persist_stream, &ev)) {
		/* Both components support Bonobo::PersistStream */
		Bonobo_PersistStream_load (document->persist_stream, 
					   bonobo_object_corba_objref (BONOBO_OBJECT (stream)),
					   document->mime_type,
					   &ev);
	} else if (stream) {
		/* The first component supported Bonobo::PersistStream, 
		 * but the second one does not.
		 * In this case, the user was not prompted to save
		 * because the previous control supported PersistStream and
		 * its contents were stored to memory. */

		/* FIXME: Offer to save the memory stream */
		load_uri_into_control (document, document->uri);
	} else {
		load_uri_into_control (document, document->uri);
	}

	if (!document->priv->current_component->editable && stream) {
		bonobo_object_ref (BONOBO_OBJECT (stream));
		document->priv->saved_stream = stream;
	}

	CORBA_exception_free (&ev);
}

static void
update_option_menu (ScaffoldBonoboDocument *document, AvailableComponent *component)
{
	gtk_option_menu_set_history (GTK_OPTION_MENU (document->priv->viewer_option),
				     component->menu_item);
}

static void
activate_component (ScaffoldBonoboDocument *document, AvailableComponent *component) 
{
	CORBA_Environment ev;
	
	CORBA_exception_init (&ev);
	if (document->priv->current_component) {
		unload_component (document);
	}
	
	create_editor_widget (document, component);
	
	if (document->bonobo_widget) {
		Bonobo_Unknown unk;
		Bonobo_PropertyBag bag;
		CORBA_any *any;

		unk = bonobo_widget_get_objref (BONOBO_WIDGET (document->bonobo_widget));
		document->persist_stream = Bonobo_Unknown_queryInterface 
			(unk, "IDL:Bonobo/PersistStream:1.0", &ev);
		
		document->control_frame = bonobo_widget_get_control_frame
			(BONOBO_WIDGET (document->bonobo_widget));

		if (!document->control_frame) {
			g_error ("could not get control frame for editor");
		}

		bag = bonobo_control_frame_get_control_property_bag
			(document->control_frame, &ev);
		g_assert (!BONOBO_EX (&ev));

		if (bag == CORBA_OBJECT_NIL) {
			document->priv->supports_modified = FALSE;
		} else {
			any = Bonobo_PropertyBag_getValue (bag, "dirty", &ev);
			if (BONOBO_USER_EX (&ev, "IDL:Bonobo/PropertyBag/NotFound:1.0")) {
				document->priv->supports_modified = FALSE;
			} else {
				Bonobo_EventSource source;
				BonoboListener *listener;

				CORBA_free (any);

				source = Bonobo_Unknown_queryInterface (bag,
									"IDL:Bonobo/EventSource:1.0",
									&ev);

				listener = bonobo_listener_new (scaffold_bonobo_document_dirty, document);
				g_object_set_data (G_OBJECT (document), "DirtyListener", listener);

				/* Register listener for "dirty" event. */
				if (!CORBA_Object_is_nil (source, &ev) && ev._major == CORBA_NO_EXCEPTION) {
					Bonobo_EventSource_addListenerWithMask (source,
										BONOBO_OBJREF (listener),
										"Bonobo/Property:change:dirty",
										&ev);
					g_assert (!BONOBO_EX (&ev));
				}

				document->priv->supports_modified = TRUE;
			}
		}

		document->priv->current_component = component;
	}

	update_option_menu (document, component);
	
	CORBA_exception_free (&ev);
}

static void
unload_component (ScaffoldBonoboDocument *document)
{
	CORBA_Environment ev;
	Bonobo_PropertyBag bag;
	BonoboListener *listener;

	if (!document->bonobo_widget)
		return;

	CORBA_exception_init (&ev);

	if (document->priv->supports_modified) {
		bag = bonobo_control_frame_get_control_property_bag (document->control_frame, &ev);
		g_assert (!BONOBO_EX (&ev));

		listener = g_object_get_data (G_OBJECT (document), "DirtyListener");

		/* Remove "dirty" event listener. */
		bonobo_event_source_client_remove_listener (bag,
							    BONOBO_OBJREF (listener),
							    &ev);
		g_assert (!BONOBO_EX (&ev));
	}

	gtk_container_remove (GTK_CONTAINER (document), 
			      document->bonobo_widget);

	document->bonobo_widget = NULL;
	document->priv->current_component = NULL;

	if (!CORBA_Object_is_nil (document->persist_stream, &ev)) {
		Bonobo_Unknown_unref (document->persist_stream, &ev);
		CORBA_Object_release (document->persist_stream, &ev);
		document->persist_stream = CORBA_OBJECT_NIL;
	}

	CORBA_exception_free (&ev);
}

static void
change_component (ScaffoldBonoboDocument *document, AvailableComponent *component)
{	
	BonoboStream *tmp = NULL;

	g_return_if_fail (component != NULL);

	if (document->priv->current_component == component) {
		return;
	}
	
	tmp = get_working_copy (document);

	unload_component (document);
	activate_component (document, component);
	
	set_working_copy (document, tmp);
	
	if (tmp) {
		bonobo_object_unref (BONOBO_OBJECT (tmp));
	}
}

static void 
load_uri_into_control (ScaffoldBonoboDocument *document, const char *uri) 
{
	CORBA_Environment ev;

	CORBA_exception_init (&ev);
	
	if (CORBA_Object_is_nil(document->persist_stream, &ev)) {
		g_warning ("The loaded component does not support Bonobo::PersistStream");
	} else {
		BonoboStreamVfs *stream;

		stream = bonobo_stream_vfs_open (uri,
						 Bonobo_Storage_READ,
		 				 &ev);
		if (!BONOBO_EX (&ev)) {
			Bonobo_PersistStream_load (document->persist_stream,
						   BONOBO_OBJREF (stream), 
						   document->mime_type, &ev);
			bonobo_object_unref (stream);
		}
	} 
	CORBA_exception_free (&ev);
}

static void
save_uri_from_control (ScaffoldBonoboDocument *document, const char *uri)
{
	 CORBA_Environment ev;
	 
	 CORBA_exception_init (&ev);
	 
	 if (CORBA_Object_is_nil (document->persist_stream, &ev)) {
		 g_warning ("The loaded component does not support Bonobo::PersistStream");
	 } else {
		 BonoboStreamVfs *stream;

		gnome_vfs_truncate (uri, 0);

		 stream = bonobo_stream_vfs_open (uri,
						  Bonobo_Storage_WRITE,
						  &ev);
		 if (!BONOBO_EX (&ev)) {
			 Bonobo_PersistStream_save (document->persist_stream,
						    BONOBO_OBJREF (stream),
						    document->mime_type,
						    &ev);
			 bonobo_object_unref (stream);
		 }
	 }
}
						
static GList *
get_available_components (const char *mime_type)
{
	/* FIXME: This should cache, since we're likely to do a lot of queries
	 * only only a few different file types */

	CORBA_Environment ev;
	Bonobo_ServerInfoList *server_list;
	CORBA_char *query;
	GList *ret = NULL;
	char *generic;
	char *p;
	GSList *langs = scaffold_get_lang_list ();

	generic = g_strdup (mime_type);
	p = strchr (generic, '/');
	g_assert (p);
	*(++p) = '*';
	*(++p) = 0;

	CORBA_exception_init (&ev);
	query = g_strdup_printf ("repo_ids.has ('IDL:Bonobo/Control:1.0') AND (bonobo:supported_mime_types.has ('%s') OR bonobo:supported_mime_types.has ('%s')) AND (repo_ids.has ('IDL:Bonobo/PersistStream:1.0'))", mime_type, generic);
	
	server_list = bonobo_activation_query (query, NULL, &ev);

	g_free (generic);	       
	g_free (query);
	
	if (ev._major == CORBA_NO_EXCEPTION && server_list != NULL && server_list->_length >= 1) {
		int i;

		for (i = 0; i < server_list->_length; i++) {
			Bonobo_ActivationProperty *prop;
			AvailableComponent *v = g_new (AvailableComponent, 1);
			Bonobo_ServerInfo *s = &server_list->_buffer[i];
			v->name = g_strdup (bonobo_server_info_prop_lookup (s, "name",
									    langs));
			v->iid = g_strdup (s->iid);

			prop = bonobo_server_info_prop_find (s, 
							     "bonobo:editable");
			v->editable = prop ? prop->v._u.value_boolean : FALSE;

			ret = g_list_prepend (ret, v);
		}
	}
	
	g_slist_free (langs);

	if (server_list != NULL) {
		CORBA_free (server_list);
	}
	CORBA_exception_free (&ev);

	return ret;
}

static void
destroy_available_components (GList *components)
{
	GList *i;
	for (i  = components; i != NULL; i = i->next) {
		AvailableComponent *c = i->data;
		g_free (c->name);
		g_free (c->iid);
		g_free (c);
	}
	g_list_free (components);
}

static GList *
get_available_apps (const char *mime_type)
{
	return gnome_vfs_mime_get_all_applications (mime_type);
}

static void
destroy_available_apps (GList *apps)
{
	gnome_vfs_mime_application_list_free (apps);
}


static void
launch_command (ScaffoldBonoboDocument *doc, char *cmd, gboolean run_in_terminal)
{
	char **argv;
	int i = 0;

	if (run_in_terminal) {
		argv = g_new0 (char *, 5);
		argv[i++] = "gnome-terminal";
		argv[i++] = "--command";
		argv[i++] = g_strdup_printf ("%s %s", 
					     cmd, doc->uri);
		
	} else {
		argv = g_new0 (char*, 3);
		argv[i++] = cmd;
		argv[i++] = doc->uri;
	}
	
	gnome_execute_async (g_get_home_dir (), i, argv);
	
	if (run_in_terminal) {
		g_free (argv[2]);
	}
	
	g_free (argv);
}

static void
launch_application (ScaffoldBonoboDocument *doc, GnomeVFSMimeApplication *app)
{
	launch_command (doc, app->command, app->requires_terminal);
}

static void
view_activated_cb (GtkWidget *widget, ScaffoldBonoboDocument *document)
{
	AvailableComponent *comp = g_object_get_data (G_OBJECT (widget), 
						      "AvailableComponent");
	if (comp) {
		change_component (document, comp);
	} else {
		GnomeVFSMimeApplication *a = g_object_get_data (G_OBJECT (widget), 
								"Application");
		launch_application (document, a);
	}
}

static void
app_activated_cb (GtkWidget *widget, ScaffoldBonoboDocument *document)
{
	GnomeVFSMimeApplication *a = g_object_get_data (G_OBJECT (widget), 
							"Application");
	launch_application (document, a);
}

static void 
browse_cb (GtkWidget *widget, gpointer data)
{
	
}

static void
other_activated_cb (GtkWidget *widget, ScaffoldBonoboDocument *document)
{
	GtkWidget *dlg;
	GtkWidget *hbox;
	GtkWidget *label;
	GtkWidget *entry;
	GtkWidget *needs_terminal;
	GtkWidget *browse;

	dlg = gtk_dialog_new_with_buttons (_("Open With"), NULL, 0, 
					   GTK_STOCK_OK, GTK_STOCK_CANCEL,
					   NULL);

	hbox = gtk_hbox_new (FALSE, 5);
	label = gtk_label_new (_("Application name:"));
	entry = gnome_entry_new ("open-with");
	browse = gtk_button_new_with_label (_("Browse..."));
	g_signal_connect (GTK_OBJECT (browse), "clicked",
			    G_CALLBACK (browse_cb), 
			    gnome_entry_gtk_entry (GNOME_ENTRY (entry)));

	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 5);
	gtk_box_pack_start (GTK_BOX (hbox), entry, TRUE, TRUE, 5);
	gtk_box_pack_start (GTK_BOX (hbox), browse, FALSE, FALSE, 5);
	
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dlg)->vbox), hbox,
			    TRUE, TRUE, 0);

	gtk_widget_show_all (hbox);

	needs_terminal = gtk_check_button_new_with_label (_("Run in terminal"));
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dlg)->vbox), 
			    needs_terminal, TRUE, TRUE, 0);
	gtk_widget_show (needs_terminal);
	
	if (gtk_dialog_run (GTK_DIALOG (dlg)) == 0) {
		char *cmd;
		
		cmd = gtk_editable_get_chars (GTK_EDITABLE (gnome_entry_gtk_entry (GNOME_ENTRY (entry))), 0, -1);

		launch_command (document, cmd, 
				gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (needs_terminal)));
	}
	gtk_widget_destroy (dlg);
}

static GtkWidget *
build_component_menu (ScaffoldBonoboDocument *document)
{
	int index = 0;
	GtkWidget *menu;
	GList *i;
	GtkWidget *item;

	menu = gtk_menu_new ();
	for (i = document->priv->available_components; i != NULL; i = i->next) {
		AvailableComponent *v = i->data;
		char *text;
		text = g_strdup_printf (v->editable ? _("Edit with %s")
					            : _("View with %s"),
					v->name);
		item = gtk_menu_item_new_with_label (text);
		g_free (text);
		g_object_set_data (G_OBJECT (item), "AvailableComponent", 
				   v);

		g_signal_connect (GTK_OBJECT (item), "activate", 
				    G_CALLBACK (view_activated_cb), 
				    document);

		gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
		v->menu_item = index++;
	}

	if (document->priv->available_components) {
		item = gtk_menu_item_new ();
		gtk_widget_set_sensitive (GTK_WIDGET (item), FALSE);
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	}

	for (i = document->priv->available_apps; i != NULL; i = i->next) {
		GnomeVFSMimeApplication *a = i->data;
		char *text;
		text = g_strdup_printf ("Launch %s", a->name);
		item = gtk_menu_item_new_with_label (text);
		g_free (text);

		g_object_set_data (G_OBJECT (item), "AvailableComponent", 
				   NULL);
		g_object_set_data (G_OBJECT (item), "Application", a);

		g_signal_connect (GTK_OBJECT (item), "activate", 
				    G_CALLBACK (app_activated_cb), 
				    document);

		gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	}	

	if (document->priv->available_apps) {
		item = gtk_menu_item_new ();
		gtk_widget_set_sensitive (GTK_WIDGET (item), FALSE);
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	}

	item = gtk_menu_item_new_with_label (_("Other..."));
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	g_signal_connect (GTK_OBJECT (item), "activate",
			  G_CALLBACK (other_activated_cb),
			  document);

	return menu;
}

#if 0
static GtkWidget *
build_app_menu (ScaffoldBonoboDocument *document)
{
	GtkWidget *menu;
	GList *i;
	GtkWidget *item;

	menu = gtk_menu_new ();

	item = gtk_menu_item_new_with_label (_("Select Application"));
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);

	for (i = document->priv->available_apps; i != NULL; i = i->next) {
		GnomeVFSMimeApplication *a = i->data;
		char *text;
		text = g_strdup_printf ("Launch %s", a->name);
		item = gtk_menu_item_new_with_label (text);
		g_free (text);

		g_object_set_data (G_OBJECT (item), "AvailableComponent", 
				   NULL);
		g_object_set_data (G_OBJECT (item), "Application", a);

		g_signal_connect (GTK_OBJECT (item), "activate", 
				    G_CALLBACK (app_activated_cb), 
				    document);

		gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	}

	item = gtk_menu_item_new_with_label (_("Other..."));
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	g_signal_connect (GTK_OBJECT (item), "activate",
			    G_CALLBACK (other_activated_cb),
			    document);

	return menu;
}
#endif

static void
update_viewers (ScaffoldBonoboDocument *document)
{
	GtkWidget *menu;
	
	if (document->priv->available_components) {
		unload_viewers (document);
	}

	document->priv->available_components = 
		get_available_components (document->mime_type);

	if( document->priv->available_components ) {
		/* We have a bonobo control for the request mime_type */
		document->priv->available_apps = get_available_apps (document->mime_type);
		menu = build_component_menu (document);
		gtk_widget_show_all (menu);
		gtk_option_menu_set_menu (GTK_OPTION_MENU (document->priv->viewer_option),
					  menu);
	}
}

static void
unload_viewers (ScaffoldBonoboDocument *document)
{
	if (!document->priv->available_components)
		return;

	destroy_available_components (document->priv->available_components);
	document->priv->available_components = NULL;
	gtk_option_menu_remove_menu (GTK_OPTION_MENU (document->priv->viewer_option));

	destroy_available_apps (document->priv->available_apps);
	document->priv->available_apps = NULL;
}

static gboolean
create_editor_widget (ScaffoldBonoboDocument *document, AvailableComponent *component)
{
	CORBA_Environment ev;
	gboolean ret = FALSE;
	BonoboControlFrame *frame;

	CORBA_exception_init (&ev);

	document->bonobo_widget = bonobo_widget_new_control (component->iid, 
							     document->ui_container);
	frame = bonobo_widget_get_control_frame (BONOBO_WIDGET (document->bonobo_widget));
	bonobo_control_frame_set_autoactivate (frame, FALSE);
	gtk_widget_grab_focus (GTK_WIDGET (document->bonobo_widget));
	bonobo_control_frame_control_activate (frame);

	if (document->bonobo_widget) {				
		gtk_container_add (GTK_CONTAINER (document), 
				   document->bonobo_widget);
		gtk_widget_show (document->bonobo_widget);

		ret = TRUE;
	} else {
		ret = FALSE;
	}
	CORBA_exception_free (&ev);
	return ret;
}

static AvailableComponent *
choose_default_component (ScaffoldBonoboDocument *document)
{
	Bonobo_ServerInfo *server_info;
	AvailableComponent *ret = NULL;
	
	g_return_val_if_fail (document->priv->available_components != NULL, 
			      NULL);

	/* First try to match the gnome-vfs default component */
	server_info = gnome_vfs_mime_get_default_component (document->mime_type);
	if (server_info) {
		GList *i;
		for (i = document->priv->available_components; i != NULL; i = i->next) {
			AvailableComponent *component = i->data;
			if (strcmp (component->iid, server_info->iid) == 0) {
				ret = component;
				break;
			}
		}
		CORBA_free (server_info);
	}
	
	/* Then try to find an editable component */
	if (!ret) {
		GList *i;
		for (i = document->priv->available_components; i != NULL; i = i->next) {
			AvailableComponent *component = i->data;
			if (component->editable) {
				ret = component;
				break;
			}
		}
	}

	/* Then just pick the first one */
	if (!ret) {
		ret = document->priv->available_components->data;
	}
	return ret;
}

static void 
set_default_clicked_cb (GtkWidget *btn, ScaffoldBonoboDocument *document)
{
	char *msg;

	gnome_vfs_mime_set_default_component (document->mime_type,
					      document->priv->current_component->iid);


	msg = g_strdup_printf (_("Set the default component for %s to %s"),
			       document->mime_type,
			       document->priv->current_component->name);
	scaffold_dialog_info (msg);
	g_free (msg);
}

/* ScaffoldDocument interface */

static void 
scaffold_bonobo_document_save (ScaffoldDocument *document, 
			     GError *error)
{
	ScaffoldBonoboDocument *bonobo_document = 
		SCAFFOLD_BONOBO_DOCUMENT (document);
	
	if (!bonobo_document->uri || bonobo_document->untitled) {
		scaffold_bonobo_document_save_as_dialog (bonobo_document);
	} else {
		scaffold_bonobo_document_save_uri 
			(bonobo_document, scaffold_document_get_uri (document));
	}
}

static void
scaffold_bonobo_document_save_as (ScaffoldDocument *document,
				const char *uri,
				GError *error)
{
	scaffold_bonobo_document_save_uri (SCAFFOLD_BONOBO_DOCUMENT (document), 
					 uri);
}

static const char *
scaffold_bonobo_document_get_uri (ScaffoldDocument *document)
{
	g_return_val_if_fail (document != NULL, NULL);

	return SCAFFOLD_BONOBO_DOCUMENT (document)->uri;
}

static const char*
scaffold_bonobo_document_get_mime_type (ScaffoldDocument *document)
{
	return SCAFFOLD_BONOBO_DOCUMENT (document)->mime_type;
}

static Bonobo_Control
scaffold_bonobo_document_get_control (ScaffoldDocument *document)
{
	Bonobo_Control ctrl;
	
	ctrl = bonobo_widget_get_objref 
		(BONOBO_WIDGET (SCAFFOLD_BONOBO_DOCUMENT (document)->bonobo_widget));
	return bonobo_object_dup_ref (ctrl, NULL);
}



static void
scaffold_document_iface_init (ScaffoldDocumentIface *iface)
{
	iface->save = scaffold_bonobo_document_save;
	iface->save_as = scaffold_bonobo_document_save_as;
	iface->get_uri = scaffold_bonobo_document_get_uri;
	iface->get_mime_type = scaffold_bonobo_document_get_mime_type;
	iface->get_control = scaffold_bonobo_document_get_control;
}

SCAFFOLD_TYPE_BEGIN(ScaffoldBonoboDocument, scaffold_bonobo_document,
		  GTK_TYPE_VBOX)
SCAFFOLD_INTERFACE (scaffold_document, SCAFFOLD_TYPE_DOCUMENT)
SCAFFOLD_TYPE_END
