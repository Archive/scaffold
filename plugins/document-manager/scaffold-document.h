/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * scaffold-document.h
 * 
 * Copyright (C) 1998-2000 Steffen Kern
 * Copyright (C) 2003 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __SCAFFOLD_BONOBO_DOCUMENT_H__
#define __SCAFFOLD_BONOBO_DOCUMENT_H__

#include <libbonoboui.h>

G_BEGIN_DECLS

#define SCAFFOLD_TYPE_BONOBO_DOCUMENT        (scaffold_bonobo_document_get_type ())
#define SCAFFOLD_BONOBO_DOCUMENT(o)          (GTK_CHECK_CAST ((o), SCAFFOLD_TYPE_BONOBO_DOCUMENT, ScaffoldBonoboDocument))
#define SCAFFOLD_BONOBO_DOCUMENT_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), SCAFFOLD_TYPE_BONOBO_DOCUMENT, ScaffoldBonoboDocumentClass))
#define SCAFFOLD_IS_BONOBO_DOCUMENT(o)       (GTK_CHECK_TYPE ((o), SCAFFOLD_TYPE_BONOBO_DOCUMENT))
#define SCAFFOLD_IS_BONOBO_DOCUMENT_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), SCAFFOLD_TYPE_BONOBO_DOCUMENT))

typedef struct _ScaffoldBonoboDocument            ScaffoldBonoboDocument;
typedef struct _ScaffoldBonoboDocumentClass       ScaffoldBonoboDocumentClass;
typedef struct _ScaffoldBonoboDocumentPrivate     ScaffoldBonoboDocumentPrivate;

struct _ScaffoldBonoboDocumentClass {
	GtkVBoxClass vbox;

	void (* changed)    (ScaffoldBonoboDocument *document,
			     int                   change_type);
	void (* readonly)   (ScaffoldBonoboDocument *document);
	void (* unreadonly) (ScaffoldBonoboDocument *document);
	void (* source)     (ScaffoldBonoboDocument *document,
			     char                 *filename);
	void (* cursor)     (ScaffoldBonoboDocument* document);
	void (* focus)      (ScaffoldBonoboDocument* document);
};

struct _ScaffoldBonoboDocument {
	GtkVBox parent;
	
	Bonobo_UIContainer ui_container;
	BonoboControlFrame *control_frame;
	GtkWidget *bonobo_widget;
	Bonobo_PersistFile persist_file;
	Bonobo_PersistStream persist_stream;
	
	gboolean file_loaded;
	char *uri;
	char *mime_type;
	gboolean untitled;

	ScaffoldBonoboDocumentPrivate *priv;
};

/* creation */
GType          scaffold_bonobo_document_get_type           (void);
GtkWidget   *  scaffold_bonobo_document_new                (Bonobo_UIContainer    ui_container);

/* file access */
void           scaffold_bonobo_document_make_temp          (ScaffoldBonoboDocument *document,
							  const char           *mime_type);
gboolean       scaffold_bonobo_document_load_template      (ScaffoldBonoboDocument *document,
							  const char           *uri);
gboolean       scaffold_bonobo_document_load_uri           (ScaffoldBonoboDocument *document,
							  const char           *uri);
void           scaffold_bonobo_document_reload             (ScaffoldBonoboDocument *document);
void           scaffold_bonobo_document_save_uri           (ScaffoldBonoboDocument *document,
							  const char           *uri);
void           scaffold_bonobo_document_save_as_dialog     (ScaffoldBonoboDocument *document);

/* Functions to get/set state */
gboolean       scaffold_bonobo_document_is_free            (ScaffoldBonoboDocument *document);
gboolean       scaffold_bonobo_document_is_changed         (ScaffoldBonoboDocument *document);
void           scaffold_bonobo_document_set_changed_state  (ScaffoldBonoboDocument *document,
							  gboolean              state);
gboolean       scaffold_bonobo_document_is_readonly        (ScaffoldBonoboDocument *document);
void           scaffold_bonobo_document_set_readonly_state (ScaffoldBonoboDocument *document,
							  gboolean              state);
gboolean       scaffold_bonobo_document_is_busy            (ScaffoldBonoboDocument *document);
void           scaffold_bonobo_document_set_busy_state     (ScaffoldBonoboDocument *document,
							  gboolean              state);
void           scaffold_bonobo_document_set_last_mod       (ScaffoldBonoboDocument *document,
							  int                   last_mod);
gboolean       scaffold_bonobo_document_is_untitled        (ScaffoldBonoboDocument *document);
void           scaffold_bonobo_document_check_changed      (ScaffoldBonoboDocument *document);
void           scaffold_bonobo_document_set_cfg_values     (ScaffoldBonoboDocument *document);
gboolean       scaffold_bonobo_document_supports_modified  (ScaffoldBonoboDocument *document);

G_END_DECLS

#endif /* __SCAFFOLD_BONOBO_DOCUMENT_H__ */
