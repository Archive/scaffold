/* Scaffold
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgnome/gnome-i18n.h>
#include <libgnome/gnome-macros.h>
#include "scaffold-gnome-application-type.h"

struct _ScaffoldGnomeApplicationTypePrivate {
};

GNOME_CLASS_BOILERPLATE (ScaffoldGnomeApplicationType, scaffold_gnome_application_type, 
			 ScaffoldProjectType, SCAFFOLD_TYPE_PROJECT_TYPE);

static const char *
impl_get_category_name (ScaffoldProjectType *type)
{
	return _("GNOME");
}

static const char *
impl_get_project_name (ScaffoldProjectType *type)
{
	return _("Application (2.0)");
}

static const char *
impl_get_description (ScaffoldProjectType *type)
{
	return _("Generates a basic GNOME 2.0 project with an automake backend.");
}

static const char *
impl_get_backend (ScaffoldProjectType *type)
{
	return "gbf-am:GbfAmProject";
}

static int
impl_get_druid_page_count (ScaffoldProjectType *type,
			   GError **error)
{
	return 0;
}

static GtkWidget *
impl_get_druid_page (ScaffoldProjectType *type,
		     int page,
		     GError **error)
{
	return NULL;
}

static void
impl_create_project (ScaffoldProjectType *type,
		     ScaffoldProjectInfo *info,
		     GError **error)
{
	gchar **args;
	const gchar *scriptdir;
	GError *err = NULL;
	gint status;
	gchar *errmsg, *outmsg;

	g_message ("create_project_impl");

	args = g_new (gchar *, 7);
	args[0] = g_strdup ("python");
	args[1] = g_strdup ("scaffold-gnome-application-create.py");
	args[2] = g_strdup (info->name);
	args[3] = g_strdup (info->package);
	args[4] = g_strdup (info->description);
	args[5] = g_strdup (info->location);
	args[6] = NULL;

	scriptdir = DATADIR "/scaffold/plugins/gnome-project-types";
	g_message (scriptdir);

	/* Spawn the python project creation script. */
	if (!g_spawn_sync (scriptdir, args, NULL, G_SPAWN_SEARCH_PATH, NULL, NULL, &outmsg, &errmsg, &status, &err)) {
		/* Spawn failed. */
		g_message ("error spawning script");
		g_message (err->message);
	}
	else {
		/* Spawn succeeded. */
		g_message (outmsg);
		g_message (errmsg);

		g_free (outmsg);
		g_free (errmsg);
	}

	*error = err;
	g_strfreev (args);
}

static void
scaffold_gnome_application_type_dispose (GObject *object)
{
	ScaffoldGnomeApplicationType *type = SCAFFOLD_GNOME_APPLICATION_TYPE (object);

	g_free (type->priv);
}

ScaffoldProjectType *
scaffold_gnome_application_type_new (void)
{
	return SCAFFOLD_PROJECT_TYPE (g_object_new (SCAFFOLD_TYPE_GNOME_APPLICATION_TYPE, NULL));
}

static void
scaffold_gnome_application_type_class_init (ScaffoldGnomeApplicationTypeClass *klass)
{
	GObjectClass *object_class;
	ScaffoldProjectTypeClass *project_class;

	parent_class = g_type_class_peek_parent (klass);
	object_class = G_OBJECT_CLASS (klass);
	project_class = SCAFFOLD_PROJECT_TYPE_CLASS (klass);

	object_class->dispose = scaffold_gnome_application_type_dispose;

	project_class->get_category_name = impl_get_category_name;
	project_class->get_project_name = impl_get_project_name;
	project_class->get_description = impl_get_description;
	project_class->get_backend = impl_get_backend;
	project_class->get_druid_page_count = impl_get_druid_page_count;
	project_class->get_druid_page = impl_get_druid_page;
	project_class->create_project = impl_create_project;
}

static void
scaffold_gnome_application_type_instance_init (ScaffoldGnomeApplicationType *type)
{
	type->priv = g_new0 (ScaffoldGnomeApplicationTypePrivate, 1);
}
