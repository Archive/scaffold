/* Scaffold
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef SCAFFOLD_GNOME_APPLICATION_TYPE_H
#define SCAFFOLD_GNOME_APPLICATION_TYPE_H

#include "../project-manager/scaffold-project-type.h"

G_BEGIN_DECLS

#define SCAFFOLD_TYPE_GNOME_APPLICATION_TYPE		(scaffold_gnome_application_type_get_type ())
#define SCAFFOLD_GNOME_APPLICATION_TYPE(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), SCAFFOLD_TYPE_GNOME_APPLICATION_TYPE, ScaffoldGnomeApplicationType))
#define SCAFFOLD_GNOME_APPLICATION_TYPE_CLASS(obj)	(G_TYPE_CHECK_CLASS_CAST ((klass), SCAFFOLD_TYPE_GNOME_APPLICATION_TYPE, ScaffoldGnomeApplicationTypeClass))
#define SCAFFOLD_IS_GNOME_APPLICATION_TYPE(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), SCAFFOLD_TYPE_GNOME_APPLICATION_TYPE))
#define SCAFFOLD_IS_GNOME_APPLICATION_TYPE_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((obj), SCAFFOLD_TYPE_GNOME_APPLICATION_TYPE))

typedef struct _ScaffoldGnomeApplicationType		ScaffoldGnomeApplicationType;
typedef struct _ScaffoldGnomeApplicationTypePrivate	ScaffoldGnomeApplicationTypePrivate;
typedef struct _ScaffoldGnomeApplicationTypeClass		ScaffoldGnomeApplicationTypeClass;

struct _ScaffoldGnomeApplicationType {
	ScaffoldProjectType parent;

	ScaffoldGnomeApplicationTypePrivate *priv;
};

struct _ScaffoldGnomeApplicationTypeClass {
	ScaffoldProjectTypeClass parent_class;
};

GType scaffold_gnome_application_type_get_type         (void);
ScaffoldProjectType *scaffold_gnome_application_type_new (void);

G_END_DECLS

#endif /* SCAFFOLD_GNOME_APPLICATION_TYPE_H */
