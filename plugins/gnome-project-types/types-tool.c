/* Scaffold
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libscaffold/libscaffold.h>
#include "scaffold-gnome-application-type.h"
#include "../project-manager/scaffold-project-manager.h"

typedef struct {
	ScaffoldTool parent;

	ScaffoldProjectType *gnome_app_type;
} TypesTool;

typedef struct {
	ScaffoldToolClass parent;
} TypesToolClass;

static void
shell_set (ScaffoldTool *tool)
{
	TypesTool *type_tool = (TypesTool*)tool;
	ScaffoldProjectManager *manager;

	g_return_if_fail (tool != NULL);
	g_return_if_fail (SCAFFOLD_IS_TOOL (tool));

	scaffold_shell_get (tool->shell,
			  "ProjectManager",
			  SCAFFOLD_TYPE_PROJECT_MANAGER,
			  &manager,
			  NULL);
	g_return_if_fail (manager != NULL);
	g_return_if_fail (SCAFFOLD_IS_PROJECT_MANAGER (manager));

	type_tool->gnome_app_type = scaffold_gnome_application_type_new ();
	scaffold_project_manager_add_project_type (manager, type_tool->gnome_app_type);
}

static void
dispose (GObject *obj)
{
	ScaffoldTool *tool = SCAFFOLD_TOOL (obj);
	TypesTool *type_tool = (TypesTool*)obj;
	ScaffoldProjectManager *manager;

	scaffold_shell_get (tool->shell,
			  "ProjectManager",
			  SCAFFOLD_TYPE_PROJECT_MANAGER,
			  &manager,
			  NULL);
	g_return_if_fail (manager != NULL);
	g_return_if_fail (SCAFFOLD_IS_PROJECT_MANAGER (manager));

	if (type_tool->gnome_app_type) {
		scaffold_project_manager_remove_project_type (manager, 
							    type_tool->gnome_app_type);
		g_object_unref (type_tool->gnome_app_type);
		type_tool->gnome_app_type = NULL;
	}
}

static void
types_tool_instance_init (GObject *object)
{
}

static void
types_tool_class_init (GObjectClass *klass)
{
	ScaffoldToolClass *tool_class = SCAFFOLD_TOOL_CLASS (klass);

	tool_class->shell_set = shell_set;
	klass->dispose = dispose;
}

SCAFFOLD_TOOL_BOILERPLATE (TypesTool, types_tool);

SCAFFOLD_SIMPLE_PLUGIN (TypesTool, types_tool);
