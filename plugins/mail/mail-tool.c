/*
 * Scaffold mail plugin
 *
 * Send the current document out via mail
 */

#include <config.h>

#include <libscaffold/libscaffold.h>
#include <unistd.h>
#include <sys/stat.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/utsname.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#define MAIL_COMPONENT_IID "OAFIID:GNOME_Development_Plugin:mail"
#define PLUGIN_NAME			"scaffold-mail-plugin"
#define PLUGIN_XML			"scaffold-mail-plugin.xml"


static void
mail_callback(
	GtkWidget*			widget,
	GtkWidget*			entry
)
{
	gchar*				to = NULL;
	gchar*				smtp = NULL;
	gchar*				email = NULL;
	struct sockaddr_in		socks;
	struct sockaddr_in		sockd;
	gint				skfd;
	struct utsname			uts;
/*
	struct hostent*			hostent = NULL;
	gchar*				hwd;
*/
	gchar*				buf;
/*
	gchar*				user;
*/
	glong				psize = 0;
	ScaffoldTool*			tool;
	gchar*				filename;

	tool = (ScaffoldTool*)gtk_object_get_data(GTK_OBJECT(entry), "tool");
	filename = scaffold_get_current_filename(tool);

	to = gtk_entry_get_text(GTK_ENTRY(entry));

	smtp = g_strdup("127.0.0.1");
	email = getenv("USER");

	skfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(skfd < 0)
	{
		perror("socket");
	}

	if(uname(&uts) < 0)
	{
		perror("uname");
	}

/*
	printf("nodename: %s\n", uts.nodename);
#ifdef __USE_GNU
	printf("domainname: %s\n", uts.domainname);
#else
	printf("domainname: %s\n", uts.__domainname);
#endif

#ifdef __USE_GNU
	hwd = g_strconcat(uts.nodename, ".", uts.domainname, NULL);
#else
	hwd = g_strconcat(uts.nodename, ".", uts.__domainname, NULL);
#endif

	hostent = gethostbyname(hwd);
#ifdef __USE_GNU
	if(!hostent && !strcmp(uts.domainname, "(none)"))
#else
	if(!hostent && !strcmp(uts.__domainname, "(none)"))
#endif
	{
		hostent = gethostbyname(uts.nodename);
	}

	g_free(hwd);

	if(!hostent)
	{
		error_dialog("Unable to determine local address!",
			"Address Lookup Error");
		return;
	}
*/

	socks.sin_family = AF_INET;
	socks.sin_port = htons(0);
	socks.sin_addr.s_addr = inet_addr("0.0.0.0");
	if(bind(skfd, (struct sockaddr*)&socks, sizeof(struct sockaddr_in)) < 0)
	{
		perror("bind");
	}

	sockd.sin_family = AF_INET;
	sockd.sin_port = htons(25);
	sockd.sin_addr.s_addr = inet_addr(smtp);

	g_free(smtp);

	if(connect(skfd, (struct sockaddr*)&sockd,
		sizeof(struct sockaddr_in)) < 0)
	{
		buf = g_strconcat(_("Connect error: "), strerror(errno), NULL);
//		scaffold_error_dialog(buf);
		g_print(buf);
		g_free(buf);
		g_free(filename);
		return;
	}

	/* process gtk stuff */
	while(gtk_events_pending() || gdk_events_pending())
	{
		gtk_main_iteration();
	}

	buf = g_strconcat("HELO", " ", uts.nodename, "\n", NULL);
	write(skfd, buf, strlen(buf));
	g_free(buf);

/*
#ifdef __USE_GNU
	if(!strcmp(uts.domainname, "(none)"))
#else
	if(!strcmp(uts.__domainname, "(none)"))
#endif
	{
		buf = g_strconcat("MAIL FROM: ", user, "@", uts.nodename,
			"\n", NULL);
	}
	else
	{
#ifdef __USE_GNU
		buf = g_strconcat("MAIL FROM: ", user, "@", uts.nodename,
			".", uts.domainname, "\n", NULL);
#else
		buf = g_strconcat("MAIL FROM: ", user, "@", uts.nodename,
			".", uts.__domainname, "\n", NULL);
#endif
	}
*/
	buf = g_strconcat("MAIL FROM: ", email, "\n", NULL);
	write(skfd, buf, strlen(buf));
	g_free(buf);

	buf = g_strconcat("RCPT TO: ", to, "\n", NULL);
	write(skfd, buf, strlen(buf));
	g_free(buf);

	buf = g_strconcat("DATA\n", NULL);
	write(skfd, buf, strlen(buf));
	g_free(buf);

	buf = g_strconcat("To: ", to, "\n", NULL);
	write(skfd, buf, strlen(buf));
	g_free(buf);

	if(filename)
	{
		buf = g_strconcat("Subject: ", filename, "\n", NULL);
	}
	else
	{
		buf = g_strconcat("Subject: File\n", NULL);
	}
	write(skfd, buf, strlen(buf));
	g_free(buf);
	
	for(psize = 0; psize < scaffold_get_document_length(tool); psize++)
	{
		buf = scaffold_get_document_chars(tool, psize, psize + 1);
		write(skfd, buf, 1);
		g_free(buf);

		gtk_main_iteration_do(FALSE);
	}

	buf = g_strconcat(".\n", NULL);
	write(skfd, buf, 2);
	g_free(buf);

	g_free(filename);

	gnome_dialog_run_and_close(GNOME_DIALOG(gnome_ok_dialog(
		_("Mail sent."))));
}

static void
mail(
	GtkWidget*			widget,
	gpointer			data
)
{
	ScaffoldTool*			tool = (ScaffoldTool*)data;
	GtkWidget*			button;
	GtkWidget*			wlabel;
	static GtkWidget*		dialog_window;
	static GtkWidget*		entry;
	GtkWidget*			hbox;

	dialog_window = gtk_dialog_new();
	gtk_signal_connect(GTK_OBJECT(dialog_window), "destroy",
		GTK_SIGNAL_FUNC(gtk_widget_destroyed), &dialog_window);
	gtk_container_set_border_width(GTK_CONTAINER(dialog_window), 10);
	gtk_window_set_title(GTK_WINDOW(dialog_window), _("Mail document"));
	gtk_window_set_position(GTK_WINDOW(dialog_window), GTK_WIN_POS_MOUSE);

	hbox = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog_window)->vbox),
		hbox, FALSE, TRUE, 5);
	gtk_widget_show(hbox);

	wlabel = gtk_label_new(_("Send to:"));
	gtk_box_pack_start(GTK_BOX(hbox), wlabel, FALSE, TRUE, 5);
	gtk_widget_show(wlabel);

	entry = gtk_entry_new();
	gtk_box_pack_start(GTK_BOX(hbox), entry, TRUE, TRUE, 0);
	gtk_signal_connect(GTK_OBJECT(entry), "activate",
		GTK_SIGNAL_FUNC(mail_callback), (gpointer)entry);
	gtk_signal_connect_object(GTK_OBJECT(entry), "activate",
		GTK_SIGNAL_FUNC(gtk_widget_destroy), GTK_OBJECT(dialog_window));
	gtk_widget_grab_focus(entry);
	gtk_widget_show(entry);

	button = gnome_stock_button(GNOME_STOCK_BUTTON_OK);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog_window)->action_area),
		button, TRUE, TRUE, 5);
	gtk_signal_connect(GTK_OBJECT(button), "clicked",
		GTK_SIGNAL_FUNC(mail_callback), (gpointer)entry);
	gtk_signal_connect_object(GTK_OBJECT(button), "clicked",
		GTK_SIGNAL_FUNC(gtk_widget_destroy), GTK_OBJECT(dialog_window));
	GTK_WIDGET_SET_FLAGS(button, GTK_CAN_DEFAULT);
	gtk_widget_grab_default(button);
	gtk_widget_show(button);

	button = gnome_stock_button(GNOME_STOCK_BUTTON_CANCEL);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog_window)->action_area),
		button, TRUE, TRUE, 5);
	gtk_signal_connect_object(GTK_OBJECT(button), "clicked",
		GTK_SIGNAL_FUNC(gtk_widget_destroy), GTK_OBJECT(dialog_window));
	gtk_widget_show(button);

	gtk_object_set_data(GTK_OBJECT(entry), "tool", (gpointer)tool);

	gtk_widget_show(dialog_window);
}

/*
 * Define the verbs in this plugin
 */
static BonoboUIVerb verbs[] = {
	BONOBO_UI_UNSAFE_VERB("Mail", mail),
	BONOBO_UI_VERB_END
};

SCAFFOLD_SHLIB_TOOL_SIMPLE (MAIL_COMPONENT_IID, "Scaffold Mail Plugin",
			  PLUGIN_NAME, SCAFFOLD_DATADIR, PLUGIN_XML,
			  verbs, NULL);
