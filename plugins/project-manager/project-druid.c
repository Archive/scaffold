/* Scaffold
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libscaffold/libscaffold.h>
#include <libgnome/gnome-i18n.h>
#include <libgnome/gnome-macros.h>
#include <libgnomeui/gnome-druid.h>
#include <glade/glade-xml.h>
#include <gtk/gtk.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "project-druid.h"
#include "scaffold-project-type.h"

enum {
	CANCEL,
	FINISH,
	LAST_SIGNAL
};

enum {
	PROP_BOGUS,
	PROP_PROJECT_TYPES
};

struct _ScaffoldProjectDruidPrivate {
	GHashTable *categories;
	GSList *project_types;

	/* GnomeDruid & pages. */
	GtkWidget *druid;
	GtkWidget *new_project_page;
	GtkWidget *project_info_page;
	GtkWidget *create_project_page;

	/* "New Project" page. */
	GtkWidget *category_tree;
	GtkWidget *project_tree;
	GtkWidget *description_area;
	GtkTreeModel *category_model;
	GtkTreeModel *project_model;
	GtkTextBuffer *description_buffer;

	/* "Project Information" page. */
	GtkWidget *project_entry;
	GtkWidget *package_entry;
	GtkWidget *description_entry;
	GtkWidget *location_combo;
	GtkWidget *location_button;

	/* "Create Project" page. */
	GtkWidget *category_label;
	GtkWidget *type_label;
	GtkWidget *name_label;
	GtkWidget *package_label;
	GtkWidget *description_label;
	GtkWidget *location_label;

	/* Project info. */
	ScaffoldProjectType *project_type;
	ScaffoldProjectInfo *project_info;
};

static guint druid_signals [LAST_SIGNAL] = { 0 };

/* Boilerplate code. */
GNOME_CLASS_BOILERPLATE (ScaffoldProjectDruid, scaffold_project_druid,
			 GtkVBox, GTK_TYPE_VBOX);

static void
update_categories (ScaffoldProjectDruid *druid)
{
	ScaffoldProjectDruidPrivate *priv = druid->priv;
	ScaffoldProjectType *type;
	GSList *l, *types;
	const char *category;

	g_hash_table_destroy (priv->categories);
	priv->categories = g_hash_table_new_full (g_str_hash, g_str_equal,
						  g_free, NULL);

	for (l = priv->project_types; l != NULL; l = l->next) {
		type = SCAFFOLD_PROJECT_TYPE (l->data);
		category = scaffold_project_type_get_category_name (type);

		types = g_hash_table_lookup (priv->categories, category);
		types = g_slist_append (types, type);
		if (!types) {
			g_hash_table_insert (priv->categories,
					     g_strdup (category),
					     types);
		} else {
			g_hash_table_replace (priv->categories,
					      g_strdup (category),
					      types);
		}
	}
}

static void
add_category (gpointer key,
	      gpointer value,
	      gpointer data)
{
	ScaffoldProjectDruid *druid = SCAFFOLD_PROJECT_DRUID (data);
	GtkTreeIter iter;

	gtk_tree_store_append (GTK_TREE_STORE (druid->priv->category_model),
			       &iter, NULL);
	gtk_tree_store_set (GTK_TREE_STORE (druid->priv->category_model),
			    &iter, 0, key, -1);
}

static void
build_druid_tree (ScaffoldProjectDruid *druid)
{
	GtkTreePath *path;

	gtk_tree_store_clear (GTK_TREE_STORE (druid->priv->category_model));
	gtk_list_store_clear (GTK_LIST_STORE (druid->priv->project_model));

	g_hash_table_foreach (druid->priv->categories, add_category, druid);

	/* Select the first category & project. */
	path = gtk_tree_path_new_first ();
	gtk_tree_selection_select_path (gtk_tree_view_get_selection 
						(GTK_TREE_VIEW (druid->priv->category_tree)),
						 path);
	gtk_tree_selection_select_path (gtk_tree_view_get_selection 
						(GTK_TREE_VIEW (druid->priv->project_tree)),
						 path);
	gtk_tree_path_free (path);
}

static void
category_set_pixbuf (GtkTreeViewColumn *tree_column,
		     GtkCellRenderer *cell,
		     GtkTreeModel *model,
		     GtkTreeIter *iter,
		     gpointer data)
{
#if 0
	GbfTreeData *tdata;
	GbfProjectTargetSource *source;
	GdkPixbuf *pixbuf;

	gtk_tree_model_get (model, iter, 0, &tdata, -1);

	switch (tdata->type) {
	case GBF_TREE_NODE_FILE:
		source = tdata->data;
		pixbuf = gdl_icon_for_uri (source->source_uri);
		break;
	case GBF_TREE_NODE_FOLDER:
		pixbuf = gdl_icon_for_folder ();
		break;
	default:
		pixbuf = NULL;
	}

	g_object_set (GTK_CELL_RENDERER (cell), "pixbuf", pixbuf, NULL);
	if (pixbuf)
		g_object_unref (pixbuf);

	gbf_tree_data_free (tdata);
#endif
}

static void
category_set_text (GtkTreeViewColumn *tree_column,
		   GtkCellRenderer *cell,
		   GtkTreeModel *model,
		   GtkTreeIter *iter,
		   gpointer data)
{
	char *category;

	gtk_tree_model_get (model, iter, 0, &category, -1);
	g_object_set (GTK_CELL_RENDERER (cell), "text", category, NULL);
	g_free (category);
}

static void
project_set_pixbuf (GtkTreeViewColumn *tree_column,
		    GtkCellRenderer *cell,
		    GtkTreeModel *model,
		    GtkTreeIter *iter,
		    gpointer data)
{
}

static void
project_set_text (GtkTreeViewColumn *tree_column,
		  GtkCellRenderer *cell,
		  GtkTreeModel *model,
		  GtkTreeIter *iter,
		  gpointer data)
{
	ScaffoldProjectType *type;

	gtk_tree_model_get (model, iter, 0, &type, -1);
	g_object_set (GTK_CELL_RENDERER (cell), "text",
		      scaffold_project_type_get_project_name (type), NULL);
}

static void
category_selection_cb (GtkTreeSelection *treesel,
		       gpointer user_data)
{
	ScaffoldProjectDruid *druid = SCAFFOLD_PROJECT_DRUID (user_data);
	GtkTreeIter iter;
	char *category;
	GSList *types, *l;
	ScaffoldProjectType *type;

	gtk_list_store_clear (GTK_LIST_STORE (druid->priv->project_model));

	if (gtk_tree_selection_get_selected (treesel, NULL, &iter)) {
		gtk_tree_model_get (druid->priv->category_model,
				    &iter, 0, &category, -1);

		types = g_hash_table_lookup (druid->priv->categories, category);

		for (l = types; l != NULL; l = l->next) {
			type = SCAFFOLD_PROJECT_TYPE (l->data);

			gtk_list_store_append (GTK_LIST_STORE (druid->priv->project_model),
					       &iter);
			gtk_list_store_set (GTK_LIST_STORE (druid->priv->project_model),
					    &iter, 0, type, -1);
		}
	}
}

static void
project_selection_cb (GtkTreeSelection *treesel,
		      gpointer user_data)
{
	ScaffoldProjectDruid *druid = SCAFFOLD_PROJECT_DRUID (user_data);
	GtkTextBuffer *buffer = druid->priv->description_buffer;
	GtkTreeIter iter;
	GtkTextIter start, end;
	ScaffoldProjectType *type;
	const char *description;

	if (gtk_tree_selection_get_selected (treesel, NULL, &iter)) {
		gtk_tree_model_get (druid->priv->project_model,
				    &iter, 0, &type, -1);
		g_return_if_fail (SCAFFOLD_IS_PROJECT_TYPE (type));
		druid->priv->project_type = type;

		description = scaffold_project_type_get_description (type);

		gtk_text_buffer_get_start_iter (buffer, &start);
		gtk_text_buffer_get_end_iter (buffer, &end);
		gtk_text_buffer_delete (buffer, &start, &end);
		gtk_text_buffer_insert (buffer, &start, description,
					strlen (description));
	}
}

static void
file_selection_cb (GtkWidget *button,
		   gpointer user_data)
{
	ScaffoldProjectDruid *druid = SCAFFOLD_PROJECT_DRUID (user_data);
	const char *filename;
	char *dirname;

	filename = gtk_file_selection_get_filename (GTK_FILE_SELECTION (gtk_widget_get_toplevel (button)));
	dirname = g_path_get_dirname (filename);
	gtk_entry_set_text (GTK_ENTRY (GTK_COMBO (druid->priv->location_combo)->entry),
			    dirname);
	g_free (dirname);
}

static void
location_button_cb (GtkWidget *widget,
		    gpointer user_data)
{
	ScaffoldProjectDruid *druid = SCAFFOLD_PROJECT_DRUID (user_data);
	GtkWidget *filesel;

	filesel = gtk_file_selection_new (_("Project directory"));
	gtk_window_set_transient_for (GTK_WINDOW (filesel),
				      GTK_WINDOW (gtk_widget_get_toplevel (druid->priv->druid)));
	gtk_window_set_modal (GTK_WINDOW (filesel), TRUE);
	gtk_widget_show_all (filesel);

	g_signal_connect (G_OBJECT (GTK_FILE_SELECTION (filesel)->ok_button),
			  "clicked", G_CALLBACK (file_selection_cb), druid);

	g_signal_connect_swapped (G_OBJECT (GTK_FILE_SELECTION (filesel)->ok_button),
				  "clicked", G_CALLBACK (gtk_widget_destroy),
				  (gpointer) filesel); 

	g_signal_connect_swapped (G_OBJECT (GTK_FILE_SELECTION (filesel)->cancel_button),
				  "clicked", G_CALLBACK (gtk_widget_destroy),
				  (gpointer) filesel); 
}

static gboolean
new_project_prepare_cb (GnomeDruidPage *page,
			GtkWidget *widget,
			gpointer data)
{
	gnome_druid_set_buttons_sensitive (GNOME_DRUID (widget), FALSE, TRUE,
					   TRUE, TRUE);

	return TRUE;
}

static gboolean
project_info_next_cb (GnomeDruidPage *page,
		      GtkWidget *widget,
		      gpointer data)
{
	ScaffoldProjectDruid *druid = SCAFFOLD_PROJECT_DRUID (data);
	ScaffoldProjectDruidPrivate *priv = druid->priv;
	const char *project, *package, *description, *location;
	GtkWidget *dialog, *newpage;
	int pages, i;
	GError *err = NULL;

	if (priv->project_info)
		g_free (priv->project_info);

	priv->project_info = g_new0 (ScaffoldProjectInfo, 1);

	project = gtk_entry_get_text (GTK_ENTRY (priv->project_entry));
	package = gtk_entry_get_text (GTK_ENTRY (priv->package_entry));
	description = gtk_entry_get_text (GTK_ENTRY (priv->description_entry));
	location = gtk_entry_get_text (GTK_ENTRY (GTK_COMBO (priv->location_combo)->entry));

	if (strlen (project) == 0 ||
	    strlen (package) == 0 ||
	    strlen (location) == 0) {
		dialog = gtk_message_dialog_new (GTK_WINDOW (gtk_widget_get_toplevel (priv->druid)),
						 GTK_DIALOG_DESTROY_WITH_PARENT,
						 GTK_MESSAGE_INFO,
						 GTK_BUTTONS_CLOSE,
						 _("You need to enter a project name, package name and location."));
		gtk_dialog_run (GTK_DIALOG (dialog));
		gtk_widget_destroy (dialog);
		return TRUE;
	}

	/* Check if the project directory already exists and ask if the user
	 * wants to overwrite any files there. */
	if (g_file_test (location, G_FILE_TEST_IS_DIR | G_FILE_TEST_EXISTS)) {
		dialog = gtk_message_dialog_new (GTK_WINDOW (gtk_widget_get_toplevel (priv->druid)),
						 GTK_DIALOG_DESTROY_WITH_PARENT,
						 GTK_MESSAGE_QUESTION,
						 GTK_BUTTONS_OK_CANCEL,
						 _("The project directory already exists. Do you want to overwrite any files there?"));
		if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_CANCEL) {
			gtk_widget_destroy (dialog);
			return TRUE;
		}
		gtk_widget_destroy (dialog);
	}

	priv->project_info->name = g_strdup (project);
	priv->project_info->package = g_strdup (package);
	priv->project_info->description = g_strdup (description);
	priv->project_info->location = g_strdup (location);
	priv->project_info->type = priv->project_type;

	/* Add the additional druid pages from the project type. */
	pages = scaffold_project_type_get_druid_page_count (priv->project_type, &err);
	for (i = 0; i < pages; i++) {
		newpage = scaffold_project_type_get_druid_page (priv->project_type, i, &err);
		gnome_druid_append_page (GNOME_DRUID (priv->druid),
					 GNOME_DRUID_PAGE (newpage));
	}

	return FALSE;
}

static gboolean
create_project_prepare_cb (GnomeDruidPage *page,
			   GtkWidget *widget,
			   gpointer data)
{
	ScaffoldProjectDruid *druid = SCAFFOLD_PROJECT_DRUID (data);
	ScaffoldProjectDruidPrivate *priv = druid->priv;

	gnome_druid_set_buttons_sensitive (GNOME_DRUID (widget), TRUE, TRUE,
					   TRUE, TRUE);
	gnome_druid_set_show_finish (GNOME_DRUID (widget), TRUE);

	gtk_label_set_text (GTK_LABEL (priv->category_label),
			    scaffold_project_type_get_category_name (priv->project_type));
	gtk_label_set_text (GTK_LABEL (priv->type_label),
			    scaffold_project_type_get_project_name (priv->project_type));
	gtk_label_set_text (GTK_LABEL (priv->name_label), priv->project_info->name);
	gtk_label_set_text (GTK_LABEL (priv->package_label), priv->project_info->package);
	gtk_label_set_text (GTK_LABEL (priv->description_label), priv->project_info->description);
	gtk_label_set_text (GTK_LABEL (priv->location_label), priv->project_info->location);

	return TRUE;
}

static void
druid_cancel_cb (GnomeDruid *widget,
		 gpointer user_data)
{
	ScaffoldProjectDruid *druid = SCAFFOLD_PROJECT_DRUID (user_data);

	g_signal_emit (G_OBJECT (druid), druid_signals[CANCEL], 0, NULL);
}

static void
druid_finish_cb (GnomeDruidPage *page,
		 GtkWidget *widget,
		 gpointer user_data)
{
	ScaffoldProjectDruid *druid = SCAFFOLD_PROJECT_DRUID (user_data);
	char *location = druid->priv->project_info->location;

	if (!g_file_test (location, G_FILE_TEST_IS_DIR)) {
		mkdir (location, 0755);
		if (!g_file_test (location, G_FILE_TEST_IS_DIR)) {
			scaffold_dialog_error (_("Could not create project directory"));
			return;
		}
	}

	g_signal_emit (G_OBJECT (druid), druid_signals[FINISH], 0, NULL);
}

static void
scaffold_project_druid_get_property (GObject *object,
				   guint prop_id,
				   GValue *value,
				   GParamSpec *pspec)
{
	ScaffoldProjectDruid *druid = SCAFFOLD_PROJECT_DRUID (object);

	switch (prop_id) {
	case PROP_PROJECT_TYPES:
		g_value_set_pointer (value, druid->priv->project_types);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void
scaffold_project_druid_set_property (GObject *object,
				   guint prop_id,
				   const GValue *value,
				   GParamSpec *pspec)
{
	ScaffoldProjectDruid *druid = SCAFFOLD_PROJECT_DRUID (object);

	switch (prop_id) {
	case PROP_PROJECT_TYPES:
		druid->priv->project_types = g_value_get_pointer (value);
		update_categories (druid);
		build_druid_tree (druid);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void
scaffold_project_druid_finalize (GObject *object)
{
	if (G_OBJECT_CLASS (parent_class)->finalize)
		G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
scaffold_project_druid_class_init (ScaffoldProjectDruidClass *klass)
{
	GObjectClass *object_class = (GObjectClass *) klass;

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = scaffold_project_druid_finalize;
	object_class->get_property = scaffold_project_druid_get_property;
	object_class->set_property = scaffold_project_druid_set_property;

	g_object_class_install_property (object_class, PROP_PROJECT_TYPES,
		 			 g_param_spec_pointer ("project_types", 
							       _("Project types"),
							       _("Project types"),
							       G_PARAM_READWRITE));

	druid_signals [CANCEL] = 
		g_signal_new ("cancel", 
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (ScaffoldProjectDruidClass, cancel),
			      NULL, /* accumulator */
			      NULL, /* accu_data */
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE, /* return type */
			      0,
			      NULL);
	druid_signals [FINISH] = 
		g_signal_new ("finish", 
			      G_TYPE_FROM_CLASS (klass),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (ScaffoldProjectDruidClass, finish),
			      NULL, /* accumulator */
			      NULL, /* accu_data */
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE, /* return type */
			      0,
			      NULL);
}

static void
scaffold_project_druid_instance_init (ScaffoldProjectDruid *druid)
{
	ScaffoldProjectDruidPrivate *priv;
	char *file;
	GladeXML *gui;
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;

	priv = g_new0 (ScaffoldProjectDruidPrivate, 1);
	druid->priv = priv;

	priv->project_types = NULL;
	priv->categories = g_hash_table_new_full (g_str_hash, g_str_equal,
						  g_free, NULL);
	priv->project_info = NULL;

	file = g_strconcat (DATADIR, "/scaffold/glade/", 
			    "scaffold-project-manager.glade", NULL);
	gui = glade_xml_new (file, "project_druid", NULL);
	g_free (file);
	if (!gui) {
		g_error ("Could not find project-druid.glade, reinstall scaffold");
		return;
	}

	/* Get widgets from glade. */
	priv->druid = glade_xml_get_widget (gui, "project_druid");
	priv->new_project_page = glade_xml_get_widget (gui, "new_project_page");
	priv->project_info_page = glade_xml_get_widget (gui, "project_info_page");
	priv->create_project_page = glade_xml_get_widget (gui, "create_project_page");
	priv->category_tree = glade_xml_get_widget (gui, "category_tree");
	priv->project_tree = glade_xml_get_widget (gui, "project_tree");
	priv->description_area = glade_xml_get_widget (gui, "description_area");
	priv->location_combo = glade_xml_get_widget (gui, "location_combo");
	priv->location_button = glade_xml_get_widget (gui, "location_button");
	priv->description_buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (priv->description_area));
	priv->project_entry = glade_xml_get_widget (gui, "project_entry");
	priv->package_entry = glade_xml_get_widget (gui, "package_entry");
	priv->description_entry = glade_xml_get_widget (gui, "description_entry");
	priv->location_combo = glade_xml_get_widget (gui, "location_combo");
	priv->category_label = glade_xml_get_widget (gui, "category_label");
	priv->type_label = glade_xml_get_widget (gui, "type_label");
	priv->name_label = glade_xml_get_widget (gui, "name_label");
	priv->package_label = glade_xml_get_widget (gui, "package_label");
	priv->description_label = glade_xml_get_widget (gui, "description_label");
	priv->location_label = glade_xml_get_widget (gui, "location_label");

	g_signal_connect (G_OBJECT (priv->location_button), "clicked",
			  G_CALLBACK (location_button_cb), druid);

	/* Create model for category tree. */
	priv->category_model = GTK_TREE_MODEL (gtk_tree_store_new (1, G_TYPE_STRING));
	gtk_tree_view_set_model (GTK_TREE_VIEW (priv->category_tree), priv->category_model);
	g_signal_connect (G_OBJECT (gtk_tree_view_get_selection (GTK_TREE_VIEW (priv->category_tree))),
			  "changed", G_CALLBACK (category_selection_cb), druid);

	/* Set renderer for category column. */
	column = gtk_tree_view_column_new ();
	gtk_tree_view_column_set_title (column, "Categories");
	renderer = gtk_cell_renderer_pixbuf_new ();
	gtk_tree_view_column_pack_start (column, renderer, FALSE);
	gtk_tree_view_column_set_cell_data_func (column, renderer,
						 category_set_pixbuf,
						 NULL, NULL);
	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (column, renderer, TRUE);
	gtk_tree_view_column_set_cell_data_func (column, renderer,
						 category_set_text,
						 NULL, NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (priv->category_tree), column);

	/* Create model for project tree. */
	priv->project_model = GTK_TREE_MODEL (gtk_list_store_new (1, G_TYPE_POINTER));
	gtk_tree_view_set_model (GTK_TREE_VIEW (priv->project_tree), priv->project_model);
	g_signal_connect (G_OBJECT (gtk_tree_view_get_selection (GTK_TREE_VIEW (priv->project_tree))),
			  "changed", G_CALLBACK (project_selection_cb), druid);

	/* Set renderer for project column. */
	column = gtk_tree_view_column_new ();
	gtk_tree_view_column_set_title (column, "Projects");
	renderer = gtk_cell_renderer_pixbuf_new ();
	gtk_tree_view_column_pack_start (column, renderer, FALSE);
	gtk_tree_view_column_set_cell_data_func (column, renderer,
						 project_set_pixbuf,
						 NULL, NULL);
	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (column, renderer, TRUE);
	gtk_tree_view_column_set_cell_data_func (column, renderer,
						 project_set_text,
						 NULL, NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (priv->project_tree), column);

	g_signal_connect_after (G_OBJECT (priv->new_project_page), "prepare",
				G_CALLBACK (new_project_prepare_cb), druid);
	g_signal_connect (G_OBJECT (priv->project_info_page), "next",
			  G_CALLBACK (project_info_next_cb), druid);
	g_signal_connect_after (G_OBJECT (priv->create_project_page), "prepare",
				G_CALLBACK (create_project_prepare_cb), druid);
	g_signal_connect (G_OBJECT (priv->druid), "cancel",
			  G_CALLBACK (druid_cancel_cb), druid);
	g_signal_connect (G_OBJECT (priv->create_project_page), "finish",
			  G_CALLBACK (druid_finish_cb), druid);
	gnome_druid_set_buttons_sensitive (GNOME_DRUID (priv->druid),
					   FALSE, TRUE, TRUE, TRUE);

	gtk_box_pack_start (GTK_BOX (druid), priv->druid, TRUE, TRUE, 0);
	gtk_widget_show_all (GTK_WIDGET (druid));
}

GtkWidget *
scaffold_project_druid_new (GSList *project_types)
{
	return GTK_WIDGET (g_object_new (SCAFFOLD_TYPE_PROJECT_DRUID, 
					 "project_types", project_types, NULL));
}

ScaffoldProjectInfo *
scaffold_project_druid_get_info (ScaffoldProjectDruid *druid)
{
	g_return_val_if_fail (druid != NULL, NULL);
	g_return_val_if_fail (SCAFFOLD_IS_PROJECT_DRUID (druid), NULL);

	return druid->priv->project_info;
}
