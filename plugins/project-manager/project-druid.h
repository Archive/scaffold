/* Scaffold
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef SCAFFOLD_PROJECT_DRUID_H
#define SCAFFOLD_PROJECT_DRUID_H

#include <gtk/gtkvbox.h>
#include "scaffold-project-type.h"

G_BEGIN_DECLS

#define SCAFFOLD_TYPE_PROJECT_DRUID		(scaffold_project_druid_get_type ())
#define SCAFFOLD_PROJECT_DRUID(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), SCAFFOLD_TYPE_PROJECT_DRUID, ScaffoldProjectDruid))
#define SCAFFOLD_PROJECT_DRUID_CLASS(obj)		(G_TYPE_CHECK_CLASS_CAST ((klass), SCAFFOLD_TYPE_PROJECT_DRUID, ScaffoldProjectDruidClass))
#define SCAFFOLD_IS_PROJECT_DRUID(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), SCAFFOLD_TYPE_PROJECT_DRUID))
#define SCAFFOLD_IS_PROJECT_DRUID_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((obj), SCAFFOLD_TYPE_PROJECT_DRUID))

typedef struct _ScaffoldProjectDruid        ScaffoldProjectDruid;
typedef struct _ScaffoldProjectDruidPrivate ScaffoldProjectDruidPrivate;
typedef struct _ScaffoldProjectDruidClass   ScaffoldProjectDruidClass;

struct _ScaffoldProjectDruid {
	GtkVBox parent;

	ScaffoldProjectDruidPrivate *priv;
};

struct _ScaffoldProjectDruidClass {
	GtkVBoxClass parent_class;

	void (*cancel) (ScaffoldProjectDruid *druid);
	void (*finish) (ScaffoldProjectDruid *druid);
};

GType      scaffold_project_druid_get_type (void);
GtkWidget *scaffold_project_druid_new (GSList *project_types);

ScaffoldProjectInfo *scaffold_project_druid_get_info (ScaffoldProjectDruid *druid);

G_END_DECLS

#endif /* SCAFFOLD_PROJECT_DRUID_H */
