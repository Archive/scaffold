/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * project-tool.c
 * 
 * Copyright (C) 2001 JP Rosevear
 * Copyright (C) 2002, 2003 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <libxml/tree.h>
#include <bonobo/bonobo-ui-util.h>
#include <bonobo/bonobo-file-selector-util.h>
#include <bonobo/bonobo-window.h>
#include <gconf/gconf-client.h>
#include <gdl/gdl.h>
#include <gbf/gbf-backend.h>
#include <gbf/gbf-project-model.h>
#include <gbf/gbf-project-view.h>
#include <gbf/gbf-build-info.h>
#include <gbf/gbf-project-util.h>
#include <glade/glade-xml.h>
#include <libscaffold/libscaffold.h>
#include <libgnome/gnome-i18n.h>
#include <libgnomevfs/gnome-vfs.h>
#include "project-druid.h"
#include "scaffold-project-manager.h"
#include "scaffold-project-manager-private.h"

#define RECENT_PROJECTS_LIMIT "/apps/scaffold/plugins/project_manager/recent_projects_limit"

typedef struct {
	ScaffoldTool parent;

	ScaffoldProjectManager *manager;

	GtkWidget *project_view;
	GtkWidget *project_view_popup;
	GbfProjectModel *project_model;

	GtkWidget *build_info;
	
	GtkWidget *project_combo;
	GtkWidget *project_menu;
	GtkWidget *build_combo;
	GtkWidget *build_menu;
	GtkWidget *run_combo;
	GtkWidget *run_menu;

	GbfProject *project;
	GbfProjectTarget *current_target;
	gchar *project_root;
	xmlDocPtr project_doc;

	GdlRecent *recent;
} ProjectTool;

typedef struct {
	ScaffoldToolClass parent;
} ProjectToolClass;

typedef struct {
	ScaffoldTool   *tool;
	GbfBuildTarget *target;
} BuildTargetMenuData;


static GdlPixmap pixmaps[] = {
	GDL_PIXMAP ("/commands/BuildDefault", "stock_compile-16.png"),
	GDL_PIXMAP ("/commands/BuildConfigurations", "stock_autopilot-16.png"),
	GDL_PIXMAP ("/commands/ProjectNewFolder", "stock_folder-16.png"),
	GDL_PIXMAP ("/commands/RunConfigurations", "stock_autopilot-16.png"),
	GDL_PIXMAP ("/Project/ProjectNew", "scaffold-new-project.png"),
	GDL_PIXMAP ("/Project/ProjectClose", "scaffold-close-project.png"),
	GDL_PIXMAP_END
};  


static void
open_file (ProjectTool *tool,
	   const char  *filename,
	   int          line_num)
{
	gboolean res;

	res = scaffold_show_file (SCAFFOLD_TOOL (tool), filename);
	if (!res) {
		scaffold_dialog_error (_("Unable to open file."));
		return;
	}

	if (line_num != 0)
		scaffold_set_line_num (SCAFFOLD_TOOL (tool), line_num);
}

static void
set_current_target (ProjectTool *tool, GbfProjectTarget *target)
{
	if (tool->current_target) {
		scaffold_shell_remove_value (SCAFFOLD_TOOL (tool)->shell, 
					     "ProjectManager::CurrentTarget",
					     NULL);

		gbf_project_target_free (tool->current_target);
		tool->current_target = NULL;
	}

	if (target) {
		tool->current_target = target;
		scaffold_shell_add (SCAFFOLD_TOOL (tool)->shell,
				    "ProjectManager::CurrentTarget",
				    G_TYPE_POINTER,
				    tool->current_target,
				    NULL);
	}
}

static void
build_target_cb (BonoboUIComponent *component,
		 gpointer           user_data)
{
	g_message ("build_target_cb"); 
}

static void
build_menu_data_destroy_cb (gpointer  user_data,
			    GClosure *closure)
{
	BuildTargetMenuData *md = user_data;

	g_free (md->target->id);
	g_free (md->target->label);
	g_free (md->target->description);
	g_free (md->target);
	g_free (md);
}

static void
update_build_menu (ScaffoldTool *tool,
		   GbfProject *project)
{
	GList *targets, *l;
	BonoboUIComponent *uic;
	int i;
	char *verb_name, *cmd, *item_path, *xml;
	const char *menu_path = "/menu/ProjectMenu/Project/Build/ProjectBuildPlaceholder";
	BuildTargetMenuData *md;
	GClosure *closure;

	targets = gbf_project_get_build_targets (project, NULL);

	uic = tool->uic;

	bonobo_ui_component_freeze (uic, NULL);

	for (l = targets, i = 1; l != NULL; l = l->next, i++) {
		GbfBuildTarget *target = l->data;

		/* Create verb & command for menuitem. */
		verb_name = g_strdup_printf ("%s%d", "target-", i);
		cmd = g_strdup_printf ("<cmd name = \"%s\"/> ", verb_name);
		bonobo_ui_component_set_translate (uic, "/commands/", cmd, NULL);

		md = g_new0 (BuildTargetMenuData, 1);
		md->tool = tool;
		md->target = target;

		closure = g_cclosure_new (G_CALLBACK (build_target_cb), md,
					  build_menu_data_destroy_cb);

		bonobo_ui_component_add_listener_full (uic, verb_name, closure);

		item_path = g_strconcat (menu_path, "/", verb_name, NULL);

		if (bonobo_ui_component_path_exists (uic, item_path, NULL)) {
			bonobo_ui_component_set_prop (uic, item_path, "label",
						      target->label, NULL);
			bonobo_ui_component_set_prop (uic, item_path, "tip",
						      target->description, NULL);
		} else {
			xml = g_strdup_printf ("<menuitem name=\"%s\" verb=\"%s\""
					       " _label=\"%s\" _tip=\"%s\" hidden=\"0\"/>", 
					       verb_name, verb_name,
					       target->label, target->description);

			bonobo_ui_component_set_translate (uic, menu_path,
							   xml, NULL);
			g_free (xml);
		}

		g_free (item_path);
		g_free (cmd);
		g_free (verb_name);
	}

	g_list_free (targets);

	bonobo_ui_component_thaw (uic, NULL);
}

static void
build_start_cb (GbfProject *project,
		gpointer    user_data)
{
}

static void
build_stop_cb (GbfProject *project,
	       gboolean    success,
	       gpointer    user_data)
{
}

static void
set_build (ProjectTool *proj_tool, const gchar *path)
{
	ScaffoldTool *tool = SCAFFOLD_TOOL (proj_tool);
	GbfProject *project = NULL;
	GSList *l;
	GError *err = NULL;
	const char *sensitivity;

	g_return_if_fail (tool != NULL);
	g_return_if_fail (SCAFFOLD_IS_TOOL (tool));

	set_current_target (proj_tool, NULL);

	/* Close any open project first. */
	if (proj_tool->project) {
		g_object_set (G_OBJECT (proj_tool->project_model),
			      "project", NULL, NULL);
		g_object_set (G_OBJECT (proj_tool->build_info),
			      "project", NULL, NULL);

		scaffold_shell_remove_value (tool->shell, 
					     "ProjectManager::CurrentProject",
					     NULL);
		g_object_unref (proj_tool->project);
		proj_tool->project = NULL;

		g_free (proj_tool->project_root);
		proj_tool->project_root = NULL;

		xmlFreeDoc (proj_tool->project_doc);
		proj_tool->project_doc = NULL;
	}

	if (path != NULL) {
		char *basename;
		xmlChar *backend_id = NULL;
		xmlNodePtr node, prj_node = NULL;
		GbfBackend *backend = NULL;
		char *dirname;

		/* Check if the path contains a file, it exists and is has a 
		 * .scaffold extension. */		
		basename = g_path_get_basename (path);
		if (!g_file_test (path, G_FILE_TEST_EXISTS) ||
		    !g_str_has_suffix (basename, ".scaffold")) {
			char *msg = g_strdup_printf (_("File '%s' is not an scaffold project file"),
						     path);
			g_free (basename);
			scaffold_dialog_error (msg);
			g_free (msg);
			return;
		}
		g_free (basename);

		/* Load the .scaffold file. */
		proj_tool->project_doc = xmlParseFile (path);
		if (!proj_tool->project_doc) {
			char *msg = g_strdup_printf (_("Unable to parse project file '%s'"),
						     path);
			scaffold_dialog_error (msg);
			g_free (msg);
			return;
		}

		/* Find the <project> node. */
		node = proj_tool->project_doc->xmlChildrenNode;
		while (node) {
			if (!xmlStrcmp (node->name, (const xmlChar *) "project")) {
				prj_node = node;
				break;
			}
			node = node->next;
		}
		if (!prj_node) {
			char *msg = g_strdup_printf (_("File '%s' is not a valid scaffold project file"),
						     path);
			scaffold_dialog_error (msg);
			g_free (msg);
			xmlFreeDoc (proj_tool->project_doc);
			return;
		}
		
		/* Get the backend id from the document. */
		node = prj_node->xmlChildrenNode;
		while (node) {
			if (!xmlStrcmp (node->name, (const xmlChar *) "backend")) {
				backend_id = xmlNodeGetContent (node);
				break;
			}
			node = node->next;
		}
		if (!backend_id) {
			char *msg = g_strdup_printf (_("File '%s' is not a valid scaffold project file"),
						     path);
			scaffold_dialog_error (msg);
			g_free (msg);
			xmlFreeDoc (proj_tool->project_doc);
			return;
		}

		gbf_backend_init ();

		for (l = gbf_backend_get_backends (); l; l = l->next) {
			backend = l->data;
			if (!strcmp (backend->id, backend_id))
				break;
			backend = NULL;
		}

		if (!backend) {
			char *msg = g_strdup_printf (_("Project backend '%s' not found"),
						     backend_id);
			scaffold_dialog_error (msg);
			g_free (msg);
			xmlFreeDoc (proj_tool->project_doc);
			xmlFree (backend_id);
			return;
		}
		xmlFree (backend_id);

		project = gbf_backend_new_project (backend->id);
		if (!project) {
			scaffold_dialog_error (_("Could not create project object"));
			return;
		}

		/* Try to load the project */
		dirname = g_path_get_dirname (path);
		gbf_project_load (project, dirname, &err);
		g_free (dirname);
		if (err != NULL) {
			/* FIXME: Add better error reporting. */
			gchar *str;
			if (g_error_matches (err, gbf_project_error_quark (),
					     GBF_PROJECT_ERROR_DOESNT_EXIST)) {
				str = g_strdup_printf (_("No project found at location '%s'"),
						       path);
			} else if (g_error_matches (err, gbf_project_error_quark (),
						    GBF_PROJECT_ERROR_PROJECT_MALFORMED)) {
				str = g_strdup_printf (_("Not a valid project: '%s'"),
						       err->message);
			} else {
				str = g_strdup_printf (_("Unable to load project: '%s'"),
						       err->message);
			}
			scaffold_dialog_error (str);
			g_free (str);
			g_error_free (err);
			return;
		}

		proj_tool->project = project;
		scaffold_shell_add (tool->shell, 
				    "ProjectManager::CurrentProject",
				    G_TYPE_POINTER,
				    project,
				    NULL);

		/* Save root for future reference. */
		proj_tool->project_root = g_strdup (path);

		/* Update the Project->Build menu. */
		update_build_menu (tool, project);

		/* Connect to build signals. */
		g_signal_connect (G_OBJECT (project), "build_start",
				  G_CALLBACK (build_start_cb), proj_tool);
		g_signal_connect (G_OBJECT (project), "build_stop",
				  G_CALLBACK (build_stop_cb), proj_tool);
	}

	g_object_set (G_OBJECT (proj_tool->project_model), "project", project, NULL);
	g_object_set (G_OBJECT (proj_tool->build_info), "project", project, NULL);

	/* Update GUI. */
	sensitivity = project != NULL ? "1" : "0";
	bonobo_ui_component_set_prop (tool->uic, "/commands/ProjectClose",
				      "sensitive", sensitivity, NULL);
	bonobo_ui_component_set_prop (tool->uic, "/commands/ProjectAddNewGroup",
				      "sensitive", sensitivity, NULL);
	bonobo_ui_component_set_prop (tool->uic, "/commands/ProjectAddNewTarget",
				      "sensitive", sensitivity, NULL);
	bonobo_ui_component_set_prop (tool->uic, "/commands/ProjectAddSource",
				      "sensitive", sensitivity, NULL);
	bonobo_ui_component_set_prop (tool->uic, "/commands/ProjectAddResource",
				      "sensitive", sensitivity, NULL);
	bonobo_ui_component_set_prop (tool->uic, "/commands/ProjectRemove",
				      "sensitive", sensitivity, NULL);
	bonobo_ui_component_set_prop (tool->uic, "/commands/BuildDefault",
				      "sensitive", sensitivity, NULL);
	bonobo_ui_component_set_prop (tool->uic, "/commands/BuildClean",
				      "sensitive", sensitivity, NULL);
	bonobo_ui_component_set_prop (tool->uic, "/commands/BuildConfigurations",
				      "sensitive", sensitivity, NULL);
	bonobo_ui_component_set_prop (tool->uic, "/commands/ProjectNewFolder",
				      "sensitive", sensitivity, NULL);
	bonobo_ui_component_set_prop (tool->uic, "/commands/ProjectRefresh",
				      "sensitive", sensitivity, NULL);
	bonobo_ui_component_set_prop (tool->uic, "/commands/ProjectProperties",
				      "sensitive", sensitivity, NULL);
	bonobo_ui_component_set_prop (tool->uic, "/commands/RunDefault",
				      "sensitive", sensitivity, NULL);
	bonobo_ui_component_set_prop (tool->uic, "/commands/RunConfigurations",
				      "sensitive", sensitivity, NULL);
}

static void
recent_project_cb (GdlRecent  *recent,
		   const char *uri,
		   gpointer    data)
{
	ProjectTool *proj_tool = (ProjectTool *)data;

	set_build (proj_tool, uri);
}

static void
cancel_cb (GtkWidget *widget, gpointer data)
{
	gtk_widget_destroy (GTK_WIDGET (data));
}

static void
finish_cb (GtkWidget *widget, ProjectTool *proj_tool)
{
	ScaffoldProjectDruid *druid = SCAFFOLD_PROJECT_DRUID (widget);
	ScaffoldProjectInfo *info;
	GError *err = NULL;
	xmlDocPtr doc;
	char *filename;

	info = scaffold_project_druid_get_info (druid);
	gtk_widget_destroy (widget->parent);
	
	scaffold_project_type_create_project (info->type, info, &err);
	if (err != NULL) {
		scaffold_dialog_error (_("Error creating project"));
		g_error_free (err);
		return;
	}

	doc = xmlNewDoc ("1.0");
	doc->children = xmlNewDocNode (doc, NULL, "project", NULL);
	xmlNewChild (doc->children, NULL, "backend",
		     scaffold_project_type_get_backend (info->type));

	filename = g_strconcat (info->location, "/", info->package, ".scaffold", NULL);

	xmlSaveFormatFile (filename, doc, TRUE);
	xmlFreeDoc (doc);

	set_build (proj_tool, filename);
	gdl_recent_add (proj_tool->recent, filename);
	g_free (filename);

	g_free (info->name);
	g_free (info->package);
	g_free (info->description);
	g_free (info->location);
	g_free (info);
}

static void 
project_new_cb (BonoboUIComponent *uic, ScaffoldTool *tool)
{
	ProjectTool *proj_tool = (ProjectTool*)tool;
	GtkWidget *druid;
	GtkWidget *window;

	window = g_object_new (GTK_TYPE_WINDOW, 
			       "type", GTK_WINDOW_TOPLEVEL,
			       "title", _("Scaffold Project Druid"),
			       "modal", TRUE,
			       NULL);

	druid = scaffold_project_druid_new 
		(scaffold_project_manager_get_project_types (proj_tool->manager));

	g_signal_connect (druid, "cancel", G_CALLBACK (cancel_cb), window);
	g_signal_connect (druid, "finish", G_CALLBACK (finish_cb), tool);

	gtk_container_add (GTK_CONTAINER (window), druid);
	gtk_window_set_transient_for (GTK_WINDOW (window), GTK_WINDOW (tool->shell));
	gtk_window_set_modal (GTK_WINDOW (window), TRUE);
	gtk_window_set_position (GTK_WINDOW (window), GTK_WIN_POS_CENTER_ON_PARENT);
	gtk_widget_show (window);
}

static void 
project_open_cb (BonoboUIComponent *uic, ScaffoldTool *tool)
{
	ProjectTool *proj_tool = (ProjectTool*)tool;
	char *uri;

	uri = bonobo_file_selector_open (GTK_WINDOW (tool->shell), TRUE,
					 _("Open Project"), NULL, NULL);

	if (uri) {
		/* FIXME: Use gnome-vfs throughout this file instead of
		 * converting every file:// uri to a local path (because
		 * g_file_test doesn't accept uris; libxml2 probably doesn't
		 * either). */
		char *path = gnome_vfs_get_local_path_from_uri (uri);
		if (!path) {
			scaffold_dialog_error (_("Only local uris are supported for now"));
			return;
		}

		set_build (proj_tool, path);
		gdl_recent_add (proj_tool->recent, path);

		g_free (path);
		g_free (uri);
	}
}

static void
backend_set_text (GtkTreeViewColumn *tree_column,
		  GtkCellRenderer *cell,
		  GtkTreeModel *model,
		  GtkTreeIter *iter,
		  gpointer data)
{
	GbfBackend *backend;

	gtk_tree_model_get (model, iter, 0, &backend, -1);
	g_object_set (GTK_CELL_RENDERER (cell), "text", backend->description, NULL);
}

static void
project_import_cb (BonoboUIComponent *uic, ScaffoldTool *tool)
{
	ProjectTool *proj_tool = (ProjectTool *)tool;
	char *uri;
	GSList *l;
	GbfBackend *backend = NULL;
	GError *err = NULL;
	GSList *backends = NULL;
	GbfProject *project;
	char *path, *dirname;

	uri = bonobo_file_selector_open (GTK_WINDOW (tool->shell), TRUE, 
					 _("Import Project"), NULL, NULL);

	if (uri) {
		path = gnome_vfs_get_local_path_from_uri (uri);
		if (!path) {
			scaffold_dialog_error (_("Only local uris are supported for now"));
			return;
		}
		dirname = g_path_get_dirname (path);

		gbf_backend_init ();

		for (l = gbf_backend_get_backends (); l; l = l->next) {
			backend = l->data;

			project = gbf_backend_new_project (backend->id);
			if (!project) {
				g_message ("Could not create project: %s", backend->id);
				continue;
			}

			/* Probe to see if the backend can load the project. */
			if (gbf_project_probe (project, dirname, &err)) {
				backends = g_slist_append (backends, backend);
			}

			g_object_unref (project);
		}

		if (g_slist_length (backends) == 0) {
			scaffold_dialog_error (_("No backends available which can import this project"));
		} else {
			GtkWidget *dialog;
			GtkWidget *tree;
			GtkTreeModel *model;
			GtkCellRenderer *renderer;
			GtkTreeViewColumn *column;
			GtkTreeIter iter;

			char *file = g_strconcat (DATADIR, "/scaffold/glade/", 
						  "scaffold-project-manager.glade", NULL);
			GladeXML *gui = glade_xml_new (file, "project-import-dialog", NULL);
			g_free (file);
			if (!gui) {
				g_error ("Could not find project-druid.glade, reinstall scaffold");
				return;
			}

			dialog = glade_xml_get_widget (gui, "project-import-dialog");
			tree = glade_xml_get_widget (gui, "backend-tree");

			model = GTK_TREE_MODEL (gtk_list_store_new (1, G_TYPE_POINTER));
			gtk_tree_view_set_model (GTK_TREE_VIEW (tree), model);
			column = gtk_tree_view_column_new ();
			gtk_tree_view_column_set_title (column, "Backends");
			renderer = gtk_cell_renderer_text_new ();
			gtk_tree_view_column_pack_start (column, renderer, TRUE);
			gtk_tree_view_column_set_cell_data_func (column, renderer,
								 backend_set_text,
								 NULL, NULL);
			gtk_tree_view_append_column (GTK_TREE_VIEW (tree), column);

			for (l = backends; l != NULL; l = l->next) {
				backend = l->data;
				gtk_list_store_append (GTK_LIST_STORE (model),
						       &iter);
				gtk_list_store_set (GTK_LIST_STORE (model),
						    &iter, 0, backend, -1);
			}

			gtk_window_set_transient_for (GTK_WINDOW (dialog),
						      GTK_WINDOW (tool->shell));
			gtk_window_set_position (GTK_WINDOW (dialog),
						 GTK_WIN_POS_CENTER_ON_PARENT);
			if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_OK) {
				xmlDocPtr doc;
				char *filename, *save_uri;
				GtkTreeSelection *treesel = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree));
				gtk_tree_selection_get_selected (treesel, NULL, &iter);
				gtk_tree_model_get (model, &iter, 0, &backend, -1);

				doc = xmlNewDoc ("1.0");
				doc->children = xmlNewDocNode (doc, NULL, "project", NULL);
				xmlNewChild (doc->children, NULL, "backend", backend->id);

				filename = g_strconcat ("project", ".scaffold", NULL);
				save_uri = bonobo_file_selector_save (GTK_WINDOW (tool->shell),
								      TRUE, _("Save Project"),
								      NULL, dirname, filename);
				g_free (filename);

				if (save_uri) {
					xmlSaveFormatFile (save_uri, doc, TRUE);
					xmlFreeDoc (doc);
					filename = gnome_vfs_get_local_path_from_uri (save_uri);
					set_build (proj_tool, filename);
					g_free (filename);
					g_free (save_uri);
				}
			}
			gtk_widget_destroy (dialog);
		}

		g_free (path);
		g_free (dirname);
		g_free (uri);
	}
}

static void 
project_close_cb (BonoboUIComponent *uic, ScaffoldTool *tool)
{
	ProjectTool *proj_tool = (ProjectTool *)tool;

	set_build (proj_tool, NULL);
}

static void
project_add_new_group_cb (BonoboUIComponent *uic, ScaffoldTool *tool)
{
	ProjectTool *proj_tool = (ProjectTool *)tool;
	gchar *parent_group;
	GbfTreeData *data;

	/* Get default parent group. */
	data = gbf_project_view_find_selected (
		GBF_PROJECT_VIEW (proj_tool->project_view), GBF_TREE_NODE_GROUP);
	if (data)
		parent_group = data->id;
	else
		parent_group = NULL;

	gbf_project_util_new_group (proj_tool->project_model,
				    GTK_WINDOW (tool->shell),
				    parent_group);
	gbf_tree_data_free (data);
}

static void
project_add_new_target_cb (BonoboUIComponent *uic, ScaffoldTool *tool)
{
	ProjectTool *proj_tool = (ProjectTool *)tool;
	gchar *default_group;
	GbfTreeData *data;

	/* Get default group. */
	data = gbf_project_view_find_selected (
		GBF_PROJECT_VIEW (proj_tool->project_view), GBF_TREE_NODE_GROUP);
	if (data)
		default_group = data->id;
	else
		default_group = NULL;

	gbf_project_util_new_target (proj_tool->project_model,
				     GTK_WINDOW (tool->shell),
				     default_group);
	gbf_tree_data_free (data);
}

static void
project_add_source_cb (BonoboUIComponent *uic, ScaffoldTool *tool)
{
	ProjectTool *proj_tool = (ProjectTool *)tool;
	gchar *uri;
	gchar *default_target;
	GbfTreeData *data;

	/* Get default target. */
	data = gbf_project_view_find_selected (
		GBF_PROJECT_VIEW (proj_tool->project_view), GBF_TREE_NODE_TARGET);
	if (data)
		default_target = data->id;
	else
		default_target = NULL;

	/* get default file to add */
	uri = scaffold_get_current_uri (tool);

	gbf_project_util_add_source (proj_tool->project_model,
				     GTK_WINDOW (tool->shell),
				     default_target,
				     uri);

	g_free (uri);
	gbf_tree_data_free (data);
}

static void
project_add_resource_cb (BonoboUIComponent *uic, ScaffoldTool *tool)
{
}

static void
project_remove_cb (BonoboUIComponent *uic, ScaffoldTool *tool) 
{
#if 0
	ProjectTool *proj_tool = (ProjectTool *)tool;
	GbfTreeData *data;
	GtkTreeSelection *selection;
	GtkTreeModel *model;
	GtkTreeIter iter;
	
	/* Examine the project view to get the currently selected source. */
	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (proj_tool->project_view));
	if (gtk_tree_selection_get_selected (selection, &model, &iter)) {
		gtk_tree_model_get (model, &iter,
				    GBF_PROJECT_MODEL_COLUMN_DATA, &data,
				    -1);
		if (data->type == GBF_TREE_NODE_TARGET_SOURCE) {
			GError *err = NULL;
			
			gbf_project_remove_source (proj_tool->project, data->id, &err);
			if (err) {
				gchar *msg;
				msg = g_strdup_printf ("Error removing source: %s", err->message);
				scaffold_dialog_error (msg);
				g_free (msg);
				g_error_free (err);
			}
			
		} else {
			scaffold_dialog_error ("No source file is selected");
			
		}
		gbf_tree_data_free (data);

	} else {
		scaffold_dialog_error ("Nothing selected");
	}
#endif
}

static void
build_default_cb (BonoboUIComponent *uic, ScaffoldTool *tool)
{
}

static void
build_clean_cb (BonoboUIComponent *uic, ScaffoldTool *tool)
{
	ProjectTool *proj_tool = (ProjectTool *)tool;
	GError *err = NULL;
	gchar *str;

	gbf_project_clean (proj_tool->project, &err);
	if (err != NULL) {
		str = g_strdup_printf (_("An error occurred during cleaning: '%s'"),
				       err->message);
		g_free (str);
		g_error_free (err);
	}
}

static void
build_config_cb (BonoboUIComponent *uic, ScaffoldTool *tool)
{
}

static void
project_new_folder_cb (BonoboUIComponent *uic, ScaffoldTool *tool)
{
}

static void
project_refresh_cb (BonoboUIComponent *uic, ScaffoldTool *tool)
{
	ProjectTool *proj_tool = (ProjectTool *)tool;
	GError *err = NULL;

	gbf_project_refresh (proj_tool->project, &err);
	if (err != NULL) {
		/* FIXME: Add better error reporting. */
		gchar *str;
		if (g_error_matches (err, gbf_project_error_quark (),
				     GBF_PROJECT_ERROR_DOESNT_EXIST)) {
			str = g_strdup_printf (_("No project found at location '%s'"),
					       proj_tool->project_root);
		} else if (g_error_matches (err, gbf_project_error_quark (),
					    GBF_PROJECT_ERROR_PROJECT_MALFORMED)) {
			str = g_strdup_printf (_("Not a valid project: '%s'"),
					       err->message);
		} else {
			str = g_strdup_printf (_("Unable to load project: '%s'"),
					       err->message);
		}
		scaffold_dialog_error (str);
		g_free (str);
		g_error_free (err);
		return;
	}
}

static void
project_properties_cb (BonoboUIComponent *uic, ScaffoldTool *tool)
{
}

static void
run_default_cb (BonoboUIComponent *uic, ScaffoldTool *tool)
{
}

static void
run_config_cb (BonoboUIComponent *uic, ScaffoldTool *tool)
{
}

static BonoboUIVerb verbs [] = {
	BONOBO_UI_UNSAFE_VERB ("ProjectNew", project_new_cb),
	BONOBO_UI_UNSAFE_VERB ("ProjectOpen", project_open_cb),
	BONOBO_UI_UNSAFE_VERB ("ProjectImport", project_import_cb),
	BONOBO_UI_UNSAFE_VERB ("ProjectClose", project_close_cb),

	BONOBO_UI_UNSAFE_VERB ("ProjectAddNewGroup", project_add_new_group_cb),
	BONOBO_UI_UNSAFE_VERB ("ProjectAddNewTarget", project_add_new_target_cb),
	BONOBO_UI_UNSAFE_VERB ("ProjectAddSource", project_add_source_cb),
	BONOBO_UI_UNSAFE_VERB ("ProjectAddResource", project_add_resource_cb),
	BONOBO_UI_UNSAFE_VERB ("ProjectRemove", project_remove_cb),

	BONOBO_UI_UNSAFE_VERB ("BuildDefault", build_default_cb),
	BONOBO_UI_UNSAFE_VERB ("BuildClean", build_clean_cb),
	BONOBO_UI_UNSAFE_VERB ("BuildConfigurations", build_config_cb),

	BONOBO_UI_UNSAFE_VERB ("ProjectNewFolder", project_new_folder_cb),
	BONOBO_UI_UNSAFE_VERB ("ProjectRefresh", project_refresh_cb),
	BONOBO_UI_UNSAFE_VERB ("ProjectProperties", project_properties_cb),

	BONOBO_UI_UNSAFE_VERB ("RunDefault", run_default_cb),
	BONOBO_UI_UNSAFE_VERB ("RunConfigurations", run_config_cb),
	BONOBO_UI_VERB_END
};

static gboolean
project_view_popup_cb (GtkWidget      *widget,
		       GdkEventButton *event,
		       gpointer        user_data)
{
	ScaffoldTool *tool = (ScaffoldTool *)user_data;
	ProjectTool *proj_tool = (ProjectTool *)tool;
	GtkTreePath *path = NULL;
	GtkTreeIter iter;
	GbfTreeData *data = NULL;
	GSList *actions, *l;

	if (event->type != GDK_BUTTON_PRESS || event->button != 3)
		return FALSE;

	if (!gtk_tree_view_get_path_at_pos (GTK_TREE_VIEW (widget),
					    event->x, event->y, &path, NULL,
					    NULL, NULL))
		return FALSE;

	/* Get the tree data from the selected row. */
	gtk_tree_model_get_iter (GTK_TREE_MODEL (proj_tool->project_model),
				 &iter, path);
	gtk_tree_path_free (path);
	gtk_tree_model_get (GTK_TREE_MODEL (proj_tool->project_model), &iter,
			    GBF_PROJECT_MODEL_COLUMN_DATA, &data, -1);
	g_return_val_if_fail (data != NULL, FALSE);

	/* Give plugins a chance to merge menuitems with the popup menu. */
	actions = scaffold_project_manager_get_context_actions (proj_tool->manager);
	for (l = actions; l != NULL; l = l->next) {
		ScaffoldProjectManagerContextAction *ctx = l->data;
		(* ctx->callback) (proj_tool->manager,
				   tool->uic,
				   data,
				   ctx->callback_data);
	}
	gbf_tree_data_free (data);

	/* Show the popup menu. */
	gtk_menu_popup (GTK_MENU (proj_tool->project_view_popup),
			NULL, NULL, NULL, proj_tool, event->button,
			event->time);

	return FALSE;
}

static void
uri_activated_cb (GtkWidget  *widget,
		  const char *uri,
		  gpointer    user_data)
{
	open_file (user_data, uri, 0);
}

static void
target_selected_cb (GtkWidget  *widget,
		    const char *target_id,
		    gpointer    user_data)
{
	ProjectTool *proj_tool = user_data;
	GbfProject *project = proj_tool->project;
	GbfProjectTarget *target;
	GError *err = NULL;

	target = gbf_project_get_target (project, target_id, &err);
	if (err) {
		g_print ("error retrieving target: %s\n", target_id);
		g_error_free (err);
		return;
	}

	set_current_target (proj_tool, target);
}

static void
init_project_view (ScaffoldTool *tool)
{
	GtkWidget *scrolled_window;
	ProjectTool *proj_tool = (ProjectTool *)tool;

	proj_tool->project_model = gbf_project_model_new (NULL);
	proj_tool->project_view = gbf_project_view_new ();
	proj_tool->project_view_popup = gtk_menu_new ();
	gtk_tree_view_set_model (GTK_TREE_VIEW (proj_tool->project_view),
				 GTK_TREE_MODEL (proj_tool->project_model));
	gtk_widget_show (proj_tool->project_view);

        scrolled_window = gtk_scrolled_window_new (NULL, NULL);
        gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window),
                                        GTK_POLICY_AUTOMATIC,
                                        GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrolled_window),
					     GTK_SHADOW_IN);
        gtk_container_add (GTK_CONTAINER (scrolled_window), proj_tool->project_view);
        gtk_widget_show (scrolled_window);

	bonobo_window_add_popup (BONOBO_WINDOW (tool->shell),
				 GTK_MENU (proj_tool->project_view_popup),
				 "/popups/ProjectViewPopup");

	g_signal_connect (proj_tool->project_view, "button_press_event",
			  G_CALLBACK (project_view_popup_cb), tool);
	g_signal_connect (proj_tool->project_view, "uri_activated",
			  G_CALLBACK (uri_activated_cb), tool);
	g_signal_connect (proj_tool->project_view, "target_selected",
			  G_CALLBACK (target_selected_cb), tool);

	scaffold_shell_add_widget (tool->shell,
				   scrolled_window,
				   "ProjectManager::ProjectView",
				   _("Project"),
				   NULL);
}

static void
warning_selected_cb (GtkWidget  *widget,
		     const char *filename,
		     int         line,
		     gpointer    user_data)
{
	open_file (user_data, filename, line);
}

static void
init_build_info (ScaffoldTool *tool)
{
	ProjectTool *proj_tool = (ProjectTool *)tool;

	proj_tool->build_info = gbf_build_info_new ();
	gtk_widget_show (proj_tool->build_info);

	scaffold_shell_add_widget (tool->shell,
				   proj_tool->build_info,
				   "ProjectManager::BuildInfo",
				   _("Build"),
				   NULL);

	g_signal_connect (G_OBJECT (proj_tool->build_info), "warning_selected",
			  G_CALLBACK (warning_selected_cb), tool);
	g_signal_connect (G_OBJECT (proj_tool->build_info), "error_selected",
			  G_CALLBACK (warning_selected_cb), tool);
}

static void
session_load_cb (ScaffoldShell *shell,
		 ScaffoldTool *tool)
{
	ProjectTool *proj_tool = (ProjectTool *)tool;
	const char **args;
	int i;
	ScaffoldSession *session;
	char *project_root;

	/* If the user startup scaffold with the intention to display a source
	 * file then don't load the previous session but only the files on the
	 * commandline. */
	scaffold_shell_get (tool->shell,
			    "Shell::ProgramArguments",
			    G_TYPE_POINTER,
			    &args,
			    NULL);
	if (args) {
		for (i = 0; args[i] != NULL; i++) {
			if (g_str_has_suffix (args[i], ".scaffold")) {
				set_build (proj_tool, args[i]);
				return;
			}
		}
	} 
	
	/* Load previous project. */
	scaffold_shell_get (tool->shell,
			    "Shell::CurrentSession",
			    SCAFFOLD_TYPE_SESSION,
			    &session,
			    NULL);
	
	scaffold_session_get (session, "project-root", &project_root, NULL);
	set_build (proj_tool, project_root);
}

static void
session_save_cb (ScaffoldShell *shell,
		 ScaffoldTool *tool)
{
	ProjectTool *project_tool = (ProjectTool *)tool;
	ScaffoldSession *session;
	
	scaffold_shell_get (tool->shell,
			    "Shell::CurrentSession",
			    SCAFFOLD_TYPE_SESSION,
			    &session,
			    NULL);
		
	scaffold_session_set (session, "project-root", 
			      project_tool->project_root, NULL);
}

static void
shell_set (ScaffoldTool *tool)
{
	ProjectTool *proj_tool;
	GConfClient *client;
	GdlRecent *recent;
	GdkPixbuf *icon;
	BonoboControl *control;
	int limit;

	g_return_if_fail (tool != NULL);
	g_return_if_fail (SCAFFOLD_IS_TOOL (tool));

	proj_tool = (ProjectTool *)tool;

	proj_tool->manager = SCAFFOLD_PROJECT_MANAGER (scaffold_project_manager_new ());
	scaffold_shell_add (tool->shell, "ProjectManager",
			    SCAFFOLD_TYPE_PROJECT_MANAGER,
			    proj_tool->manager,
			    NULL);

	g_signal_connect (G_OBJECT (tool->shell), "session_load",
			  G_CALLBACK (session_load_cb), tool);
	g_signal_connect (G_OBJECT (tool->shell), "session_save",
			  G_CALLBACK (session_save_cb), tool);

	scaffold_tool_merge_ui (tool, "scaffold-project-manager", DATADIR,
				"scaffold-project-manager.xml", verbs, tool);

	gdl_pixmaps_update (tool->uic, SCAFFOLD_IMAGES, pixmaps);

	/* Create a new Build GdlComboButton. */
	proj_tool->project_combo = gdl_combo_button_new ();
	proj_tool->project_menu = gtk_menu_new ();
	gdl_combo_button_set_label (GDL_COMBO_BUTTON (proj_tool->project_combo),
				    _("Project"));
	gdl_combo_button_set_menu (GDL_COMBO_BUTTON (proj_tool->project_combo),
				   GTK_MENU (proj_tool->project_menu));
	icon = gdk_pixbuf_new_from_file (SCAFFOLD_IMAGES "/scaffold-open-project.png", NULL);
	gdl_combo_button_set_icon (GDL_COMBO_BUTTON (proj_tool->project_combo), icon);
	gdk_pixbuf_unref (icon);
	gtk_widget_show (proj_tool->project_combo);

	/* Create new BonoboControl and add it to the toolbar. */
	control = bonobo_control_new (proj_tool->project_combo);
	bonobo_ui_component_object_set (tool->uic,
					"/Project/ProjectOpenComboButton",
					BONOBO_OBJREF (control), NULL);
	bonobo_object_unref (control);

	/* Create a new Build GdlComboButton. */
	proj_tool->build_combo = gdl_combo_button_new ();
	proj_tool->build_menu = gtk_menu_new ();
	gdl_combo_button_set_label (GDL_COMBO_BUTTON (proj_tool->build_combo),
				    _("Build"));
	gdl_combo_button_set_menu (GDL_COMBO_BUTTON (proj_tool->build_combo),
				   GTK_MENU (proj_tool->build_menu));
	icon = gdk_pixbuf_new_from_file (SCAFFOLD_IMAGES "/stock_compile.png", NULL);
	gdl_combo_button_set_icon (GDL_COMBO_BUTTON (proj_tool->build_combo), icon);
	gdk_pixbuf_unref (icon);
	gtk_widget_show (proj_tool->build_combo);

	/* Create new BonoboControl and add it to the toolbar. */
	control = bonobo_control_new (proj_tool->build_combo);
	bonobo_ui_component_object_set (tool->uic,
					"/Build/BuildComboButton",
					BONOBO_OBJREF (control), NULL);
	bonobo_object_unref (control);

	/* Create a new Run GdlComboButton. */
	proj_tool->run_combo = gdl_combo_button_new ();
	proj_tool->run_menu = gtk_menu_new ();
	gdl_combo_button_set_label (GDL_COMBO_BUTTON (proj_tool->run_combo),
				    _("Run"));
	gdl_combo_button_set_menu (GDL_COMBO_BUTTON (proj_tool->run_combo),
				   GTK_MENU (proj_tool->run_menu));
	icon = gtk_widget_render_icon (proj_tool->run_combo, GTK_STOCK_EXECUTE,
				       GTK_ICON_SIZE_LARGE_TOOLBAR, NULL);
	gdl_combo_button_set_icon (GDL_COMBO_BUTTON (proj_tool->run_combo), icon);
	gdk_pixbuf_unref (icon);
	gtk_widget_show (proj_tool->run_combo);

	/* Create new BonoboControl and add it to the toolbar. */
	control = bonobo_control_new (proj_tool->run_combo);
	bonobo_ui_component_object_set (tool->uic,
					"/Build/RunComboButton",
					BONOBO_OBJREF (control), NULL);
	bonobo_object_unref (control);

	/* Create project view and build info. */
	init_project_view (tool);
	init_build_info (tool);
	
	/* Create GdlRecent object for projects history. */
	client = gconf_client_get_default ();
	limit = gconf_client_get_int (client, RECENT_PROJECTS_LIMIT, NULL);
	limit = (limit > 0) ? limit : 10;
	recent = gdl_recent_new ("/apps/scaffold/plugins/project_manager/recent_projects",
				 "/menu/File/FileRecentFilesPlaceholder/FileRecent/RecentProjects",
				 limit,
				 GDL_RECENT_LIST_ALPHABETIC);
	g_object_unref (G_OBJECT (client));
	g_signal_connect (G_OBJECT (recent), "activate",
			  G_CALLBACK (recent_project_cb), tool);
	gdl_recent_set_ui_component (recent, tool->uic);

	proj_tool->recent = recent;
}

static void
dispose (GObject *obj)
{
	ScaffoldTool *tool = SCAFFOLD_TOOL (obj);
	ProjectTool *proj_tool = (ProjectTool*)obj;

	if (proj_tool->project_view) {
		g_object_unref (proj_tool->project_model);
		proj_tool->project_model = NULL;

		scaffold_shell_remove_value (tool->shell,
					     "ProjectManager::ProjectView",
					     NULL);
		proj_tool->project_view = NULL;
	}

	if (proj_tool->build_info) {
		scaffold_shell_remove_value (tool->shell,
					     "ProjectManager::BuildInfo",
					     NULL);
		proj_tool->build_info = NULL;
	}

	if (proj_tool->project) {
		scaffold_shell_remove_value (tool->shell,
					     "ProjectManager::CurrentProject",
					     NULL);
		g_object_unref (proj_tool->project);
		proj_tool->project = NULL;
	}

	if (proj_tool->recent) {
		g_object_unref (proj_tool->recent);
		proj_tool->recent = NULL;
	}

	scaffold_tool_unmerge_ui (tool);

	if (proj_tool->manager) {
		scaffold_shell_remove_value (tool->shell,
					     "ProjectManager",
					     NULL);
		g_object_unref (proj_tool->manager);
		proj_tool->manager = NULL;
	}
}

static void
project_tool_instance_init (GObject *object)
{
}

static void
project_tool_class_init (GObjectClass *klass)
{
	ScaffoldToolClass *tool_class = SCAFFOLD_TOOL_CLASS (klass);

	tool_class->shell_set = shell_set;
	klass->dispose = dispose;
}

SCAFFOLD_TOOL_BOILERPLATE (ProjectTool, project_tool);

SCAFFOLD_SIMPLE_PLUGIN (ProjectTool, project_tool);
