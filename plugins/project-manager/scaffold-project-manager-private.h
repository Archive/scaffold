/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * scaffold-project-manager-private.h
 * 
 * Copyright (C) 2003 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __SCAFFOLD_PROJECT_MANAGER_PRIVATE_H__
#define __SCAFFOLD_PROJECT_MANAGER_PRIVATE_H__

#include "scaffold-project-manager.h"

G_BEGIN_DECLS

typedef struct _ScaffoldProjectManagerContextAction ScaffoldProjectManagerContextAction;

struct _ScaffoldProjectManagerContextAction {
	ScaffoldProjectManagerContextActionCallback callback;
	gpointer callback_data;	
};

GSList *scaffold_project_manager_get_context_actions (ScaffoldProjectManager *manager);

G_END_DECLS

#endif /* __SCAFFOLD_PROJECT_MANAGER_PRIVATE_H__ */
