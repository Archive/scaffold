/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * scaffold-project-manager.h
 * 
 * Copyright (C) 2002, 2003 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <libgnome/gnome-macros.h>
#include "scaffold-project-manager.h"
#include "scaffold-project-manager-private.h"

struct _ScaffoldProjectManagerPrivate {
	GSList *project_types;
	GSList *context_actions; 
};

GNOME_CLASS_BOILERPLATE (ScaffoldProjectManager, scaffold_project_manager,
			 GObject, G_TYPE_OBJECT);

GObject *
scaffold_project_manager_new (void)
{
	return g_object_new (SCAFFOLD_TYPE_PROJECT_MANAGER, NULL);
}

static void
scaffold_project_manager_class_init (ScaffoldProjectManagerClass *klass)
{
	parent_class = g_type_class_peek_parent (klass);
}

static void
scaffold_project_manager_instance_init (ScaffoldProjectManager *manager)
{
	manager->priv = g_new0 (ScaffoldProjectManagerPrivate, 1);
	manager->priv->project_types = NULL;
	manager->priv->context_actions = NULL;
}

void
scaffold_project_manager_add_project_type (ScaffoldProjectManager *manager,
					   ScaffoldProjectType *type)
{
	g_return_if_fail (manager != NULL);
	g_return_if_fail (SCAFFOLD_IS_PROJECT_MANAGER (manager));
	g_return_if_fail (type != NULL);
	g_return_if_fail (SCAFFOLD_IS_PROJECT_TYPE (type));

	manager->priv->project_types = g_slist_append (manager->priv->project_types,
						       type);
}

void
scaffold_project_manager_remove_project_type (ScaffoldProjectManager *manager,
					      ScaffoldProjectType *type)
{
	g_return_if_fail (manager != NULL);
	g_return_if_fail (SCAFFOLD_IS_PROJECT_MANAGER (manager));
	g_return_if_fail (type != NULL);
	g_return_if_fail (SCAFFOLD_IS_PROJECT_TYPE (type));

	manager->priv->project_types = g_slist_remove (manager->priv->project_types,
						       type);
}

GSList *
scaffold_project_manager_get_project_types (ScaffoldProjectManager *manager)
{
	g_return_val_if_fail (manager != NULL, NULL);
	g_return_val_if_fail (SCAFFOLD_IS_PROJECT_MANAGER (manager), NULL);

	return manager->priv->project_types;
}

void
scaffold_project_manager_add_context_action (ScaffoldProjectManager *manager,
					     ScaffoldProjectManagerContextActionCallback cb,
					     gpointer callback_data)
{
	ScaffoldProjectManagerContextAction *ctx;

	g_return_if_fail (manager != NULL);
	g_return_if_fail (SCAFFOLD_IS_PROJECT_MANAGER (manager));
	g_return_if_fail (cb != NULL);

	ctx = g_new0 (ScaffoldProjectManagerContextAction, 1);
	ctx->callback = cb;
	ctx->callback_data = callback_data;

	manager->priv->context_actions = g_slist_append (manager->priv->context_actions,
							 ctx);	
}

static gint
find_callback (gconstpointer a, gconstpointer b)
{
	ScaffoldProjectManagerContextAction *ctx = (ScaffoldProjectManagerContextAction *)a;
	if (ctx->callback == b)
		return 0;
	else
		return -1;
}

void
scaffold_project_manager_remove_context_action (ScaffoldProjectManager *manager,
						ScaffoldProjectManagerContextActionCallback cb)
{
	GSList *l;
	ScaffoldProjectManagerContextAction *ctx;

	g_return_if_fail (manager != NULL);
	g_return_if_fail (SCAFFOLD_IS_PROJECT_MANAGER (manager));
	g_return_if_fail (cb != NULL);

	l = g_slist_find_custom (manager->priv->context_actions,
				 cb, find_callback);
	ctx = l->data;
	manager->priv->context_actions = g_slist_remove (manager->priv->context_actions,
							 ctx);
	g_free (ctx);
}

GSList *
scaffold_project_manager_get_context_actions (ScaffoldProjectManager *manager)
{
	g_return_val_if_fail (manager != NULL, NULL);
	g_return_val_if_fail (SCAFFOLD_IS_PROJECT_MANAGER (manager), NULL);

	return g_slist_copy (manager->priv->context_actions);
}
