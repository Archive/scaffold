/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * scaffold-project-manager.h
 * 
 * Copyright (C) 2002, 2003 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __SCAFFOLD_PROJECT_MANAGER_H__
#define __SCAFFOLD_PROJECT_MANAGER_H__

#include <bonobo/bonobo-ui-component.h>
#include <gbf/gbf-tree-data.h>
#include "scaffold-project-type.h"

G_BEGIN_DECLS

#define SCAFFOLD_TYPE_PROJECT_MANAGER		 (scaffold_project_manager_get_type ())
#define SCAFFOLD_PROJECT_MANAGER(obj)		 (G_TYPE_CHECK_INSTANCE_CAST ((obj), SCAFFOLD_TYPE_PROJECT_MANAGER, ScaffoldProjectManager))
#define SCAFFOLD_PROJECT_MANAGER_CLASS(obj)	 (G_TYPE_CHECK_CLASS_CAST ((klass), SCAFFOLD_TYPE_PROJECT_MANAGER, ScaffoldProjectManagerClass))
#define SCAFFOLD_IS_PROJECT_MANAGER(obj)	 (G_TYPE_CHECK_INSTANCE_TYPE ((obj), SCAFFOLD_TYPE_PROJECT_MANAGER))
#define SCAFFOLD_IS_PROJECT_MANAGER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((obj), SCAFFOLD_TYPE_PROJECT_MANAGER))

typedef struct _ScaffoldProjectManager		ScaffoldProjectManager;
typedef struct _ScaffoldProjectManagerPrivate	ScaffoldProjectManagerPrivate;
typedef struct _ScaffoldProjectManagerClass	ScaffoldProjectManagerClass;

struct _ScaffoldProjectManager {
	GObject parent;

	ScaffoldProjectManagerPrivate *priv;
};

struct _ScaffoldProjectManagerClass {
	GObjectClass parent_class;
};

typedef void (* ScaffoldProjectManagerContextActionCallback) (ScaffoldProjectManager *manager,
							      BonoboUIComponent      *uic,
							      const GbfTreeData      *tree_data,
							      gpointer                callback_data);

GType    scaffold_project_manager_get_type             (void);
GObject *scaffold_project_manager_new                  (void);

/* Project types registration. */
void     scaffold_project_manager_add_project_type     (ScaffoldProjectManager *manager,
							ScaffoldProjectType    *type);
void     scaffold_project_manager_remove_project_type  (ScaffoldProjectManager *manager,
							ScaffoldProjectType    *type);
GSList  *scaffold_project_manager_get_project_types    (ScaffoldProjectManager *manager);

/* Project tree context actions. */
void     scaffold_project_manager_add_context_action   (ScaffoldProjectManager *manager,
							ScaffoldProjectManagerContextActionCallback cb,
							gpointer                callback_data);
void     scaffold_project_manager_remove_context_action(ScaffoldProjectManager *manager,
							ScaffoldProjectManagerContextActionCallback cb);							

G_END_DECLS

#endif /* __SCAFFOLD_PROJECT_MANAGER_H__ */
