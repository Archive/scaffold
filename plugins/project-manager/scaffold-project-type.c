/* Scaffold
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgnome/gnome-macros.h>
#include "scaffold-project-type.h"

GNOME_CLASS_BOILERPLATE (ScaffoldProjectType, scaffold_project_type, GObject, G_TYPE_OBJECT);

const char *
scaffold_project_type_get_category_name (ScaffoldProjectType *type)
{
	g_return_val_if_fail (type != NULL, NULL);
	g_return_val_if_fail (SCAFFOLD_IS_PROJECT_TYPE (type), NULL);

	return SCAFFOLD_PROJECT_TYPE_GET_CLASS (type)->get_category_name (type);
}

const char *
scaffold_project_type_get_project_name (ScaffoldProjectType *type)
{
	g_return_val_if_fail (type != NULL, NULL);
	g_return_val_if_fail (SCAFFOLD_IS_PROJECT_TYPE (type), NULL);

	return SCAFFOLD_PROJECT_TYPE_GET_CLASS (type)->get_project_name (type);
}

const char *
scaffold_project_type_get_description (ScaffoldProjectType *type)
{
	g_return_val_if_fail (type != NULL, NULL);
	g_return_val_if_fail (SCAFFOLD_IS_PROJECT_TYPE (type), NULL);

	return SCAFFOLD_PROJECT_TYPE_GET_CLASS (type)->get_description (type);
}

const char *
scaffold_project_type_get_backend (ScaffoldProjectType *type)
{
	g_return_val_if_fail (type != NULL, NULL);
	g_return_val_if_fail (SCAFFOLD_IS_PROJECT_TYPE (type), NULL);

	return SCAFFOLD_PROJECT_TYPE_GET_CLASS (type)->get_backend (type);
}

int
scaffold_project_type_get_druid_page_count (ScaffoldProjectType *type,
					  GError **error)
{
	g_return_val_if_fail (type != NULL, -1);
	g_return_val_if_fail (SCAFFOLD_IS_PROJECT_TYPE (type), -1);
	g_return_val_if_fail (error == NULL || *error == NULL, -1);

	return SCAFFOLD_PROJECT_TYPE_GET_CLASS (type)->get_druid_page_count (type,
									   error);
}

GtkWidget *
scaffold_project_type_get_druid_page (ScaffoldProjectType *type,
				    int page,
				    GError **error)
{
	g_return_val_if_fail (type != NULL, NULL);
	g_return_val_if_fail (SCAFFOLD_IS_PROJECT_TYPE (type), NULL);
	g_return_val_if_fail (error == NULL || *error == NULL, NULL);

	return SCAFFOLD_PROJECT_TYPE_GET_CLASS (type)->get_druid_page (type,
								     page,
								     error);
}

void
scaffold_project_type_create_project (ScaffoldProjectType *type,
				    ScaffoldProjectInfo *info,
				    GError **error)
{
	g_return_if_fail (type != NULL);
	g_return_if_fail (SCAFFOLD_IS_PROJECT_TYPE (type));
	g_return_if_fail (info != NULL);
	g_return_if_fail (error == NULL || *error == NULL);

	SCAFFOLD_PROJECT_TYPE_GET_CLASS (type)->create_project (type,
							      info,
							      error);
}

static void
scaffold_project_type_class_init (ScaffoldProjectTypeClass *klass)
{
	parent_class = g_type_class_peek_parent (klass);
}

static void
scaffold_project_type_instance_init (ScaffoldProjectType *type)
{
}
