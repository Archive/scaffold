/* Scaffold
 * Copyright (C) 2002 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef SCAFFOLD_PROJECT_TYPE_H
#define SCAFFOLD_PROJECT_TYPE_H

#include <glib-object.h>
#include <gtk/gtkwidget.h>

G_BEGIN_DECLS

#define SCAFFOLD_TYPE_PROJECT_TYPE		(scaffold_project_type_get_type ())
#define SCAFFOLD_PROJECT_TYPE(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), SCAFFOLD_TYPE_PROJECT_TYPE, ScaffoldProjectType))
#define SCAFFOLD_PROJECT_TYPE_CLASS(obj)		(G_TYPE_CHECK_CLASS_CAST ((klass), SCAFFOLD_TYPE_PROJECT_TYPE, ScaffoldProjectTypeClass))
#define SCAFFOLD_IS_PROJECT_TYPE(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), SCAFFOLD_TYPE_PROJECT_TYPE))
#define SCAFFOLD_IS_PROJECT_TYPE_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((obj), SCAFFOLD_TYPE_PROJECT_TYPE))
#define SCAFFOLD_PROJECT_TYPE_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS ((obj), SCAFFOLD_TYPE_PROJECT_TYPE, ScaffoldProjectTypeClass))

typedef struct _ScaffoldProjectInfo		ScaffoldProjectInfo;
typedef struct _ScaffoldProjectType		ScaffoldProjectType;
typedef struct _ScaffoldProjectTypeClass		ScaffoldProjectTypeClass;

struct _ScaffoldProjectInfo {
	char *name;
	char *package;
	char *description;
	char *location;
	ScaffoldProjectType *type;
};

struct _ScaffoldProjectType {
	GObject parent;
};

struct _ScaffoldProjectTypeClass {
	GObjectClass parent_class;

	/* Virtual Table */
	const char * (*get_category_name) (ScaffoldProjectType *type);
	const char * (*get_project_name)  (ScaffoldProjectType *type);
	const char * (*get_description)   (ScaffoldProjectType *type);
	const char * (*get_backend)       (ScaffoldProjectType *type);

	int (*get_druid_page_count)       (ScaffoldProjectType *type,
					   GError           **error);
	GtkWidget * (*get_druid_page)     (ScaffoldProjectType *type,
					   int                page,
					   GError           **error);

	void (*create_project)            (ScaffoldProjectType *type,
					   ScaffoldProjectInfo *info,
					   GError           **error);
};

GType scaffold_project_type_get_type                (void);

const char *scaffold_project_type_get_category_name (ScaffoldProjectType *type);
const char *scaffold_project_type_get_project_name  (ScaffoldProjectType *type);
const char *scaffold_project_type_get_description   (ScaffoldProjectType *type);
const char *scaffold_project_type_get_backend       (ScaffoldProjectType *type);
int scaffold_project_type_get_druid_page_count      (ScaffoldProjectType *type,
						   GError           **error);
GtkWidget *scaffold_project_type_get_druid_page     (ScaffoldProjectType *type,
						   int                page,
						   GError           **error);
void scaffold_project_type_create_project           (ScaffoldProjectType *type,
						   ScaffoldProjectInfo *info,
						   GError           **error);

G_END_DECLS

#endif /* SCAFFOLD_PROJECT_TYPE_H */
