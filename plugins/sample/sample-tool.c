/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * sample-tool.c
 * 
 * Copyright (C) 2000 Dave Camp
 * Copyright (C) 2003 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <bonobo/bonobo-ui-component.h>
#include <libscaffold/scaffold-tool.h>
#include <libscaffold/glue-plugin.h>
#include "../document-manager/scaffold-document-manager.h"

typedef struct {
	ScaffoldTool parent;
} SampleTool;

typedef struct {
	ScaffoldToolClass parent;
} SampleToolClass;

/*
 * Insert the text "Hello, World!" in the current document
 */
static void
hello_world_cb (BonoboUIComponent *uic, ScaffoldTool *tool)
{
	scaffold_insert_text_at_cursor (tool, "Hello, world!\n");
}

static BonoboUIVerb verbs[] = {
	BONOBO_UI_UNSAFE_VERB ("HelloWorld", hello_world_cb),
	BONOBO_UI_VERB_END
};

static void
shell_set (ScaffoldTool *tool)
{
	ScaffoldNotebookDocumentManager *dm;

	scaffold_tool_merge_ui (tool,
				"scaffold-sample-tool",
				SCAFFOLD_DATADIR,
				"scaffold-sample-plugin.xml",
				verbs,
				tool);

	scaffold_shell_get (tool->shell, "DocumentManager",
			    SCAFFOLD_TYPE_NOTEBOOK_DOCUMENT_MANAGER, &dm, NULL);

	scaffold_notebook_document_manager_add_editor_ui (dm,
							  "scaffold-sample-tool",
							  NULL,
							  SCAFFOLD_DATADIR,
							  "scaffold-sample-plugin.xml",
							  verbs,
							  tool);
}

static void
dispose (GObject *obj)
{
	scaffold_tool_unmerge_ui (SCAFFOLD_TOOL (obj));
}

static void
sample_tool_instance_init (GObject *object)
{
}

static void
sample_tool_class_init (GObjectClass *klass) 
{
	ScaffoldToolClass *tool_class = SCAFFOLD_TOOL_CLASS (klass);
	
	tool_class->shell_set = shell_set;
	klass->dispose = dispose;
}

SCAFFOLD_TOOL_BOILERPLATE (SampleTool, sample_tool);

SCAFFOLD_SIMPLE_PLUGIN (SampleTool, sample_tool);
