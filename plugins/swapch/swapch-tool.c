/*
 * Scaffold swapch plugin
 *
 * Switches besteen .c and .h file
 */

#include <config.h>

#include <libscaffold/libscaffold.h>
#include <unistd.h>
#include <sys/stat.h>
#include <ctype.h>

#define SWAPCH_COMPONENT_IID "OAFIID:GNOME_Development_Plugin:swapch"
#define PLUGIN_NAME			"scaffold-swapch-plugin"
#define PLUGIN_XML			"scaffold-swapch-plugin.xml"

static void
swapch(
	GtkWidget*			widget,
	gpointer			data
)
{
	ScaffoldTool*			tool = (ScaffoldTool*)data;
	size_t				len;
	gchar*				newfname;
	gchar*				filename;

	filename = scaffold_get_current_filename(tool);
	if(!filename)
	{
		return;
	}

	len = strlen(filename) - 1;
	while(len)
	{
		if(filename[len] == '.')
		{
			break;
		}

		--len;
	}
	if(!len)
	{
		g_free (filename);
		return;
	}

	if(strcasecmp(&filename[len], ".h") == 0)
	{
		len++;
		newfname = g_malloc(len + 4);
		strcpy(newfname, filename);
		strcpy(&newfname[len], "cc");
		if(!scaffold_file_check_if_exists(newfname, FALSE))
		{
			strcpy(&newfname[len], "cpp");
			if(!scaffold_file_check_if_exists(newfname, FALSE))
			{
				newfname[len + 1] = 0;
				if(!scaffold_file_check_if_exists(newfname, TRUE))
				{
					g_free (newfname);
					g_free (filename);
					return;
				}
			}
		}
	}
	else if(strncasecmp(&filename[len], ".c", 2) == 0)
	{
		len++;
		newfname = g_strdup(filename);
		newfname[len] = 'h';
		newfname[len + 1] = 0;
		if(!scaffold_file_check_if_exists(newfname, TRUE))
		{
			g_free (filename);
			g_free (newfname);
			return;
		}
	}
	else
	{
		return;
	}

	scaffold_show_file(tool, newfname);
	g_free (filename);
	g_free(newfname);
}

/*
 * Define the verbs in this plugin
 */
static BonoboUIVerb verbs[] = {
	BONOBO_UI_UNSAFE_VERB("SwapCH", swapch),
	BONOBO_UI_VERB_END
};

SCAFFOLD_SHLIB_TOOL_SIMPLE (SWAPCH_COMPONENT_IID, "Scaffold Swap C/H Plugin",
			  PLUGIN_NAME, SCAFFOLD_DATADIR, PLUGIN_XML,
			  verbs, NULL);
