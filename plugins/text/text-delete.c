/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * text-delete.c
 * 
 * Copyright (C) 2003 JP Rosevear (jpr@ximian.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <libscaffold/libscaffold.h>
#include "text-delete.h"

void
text_delete_to_bof (BonoboUIComponent *uic, ScaffoldTool *tool)
{
	scaffold_delete_text (tool, 0, scaffold_get_cursor_pos (tool));
}

void
text_delete_to_eof (BonoboUIComponent *uic, ScaffoldTool *tool)
{
	scaffold_delete_text (tool,
			      scaffold_get_cursor_pos (tool),
			      scaffold_get_document_length (tool));
}

static glong
get_end_of_line (ScaffoldTool *tool, glong point)
{
	static glong eol;

	for (eol = point; eol < scaffold_get_document_length (tool); eol++) {
		if (scaffold_get_document_chars (tool, eol, eol + 1 )[0] == '\n') {
			break;
		}
	}

	if (eol < 0) {
		eol = 0;
	} else if (eol > scaffold_get_document_length (tool)) {
		eol = scaffold_get_document_length (tool);
	}

	return eol;
}

static glong
get_begin_of_line (ScaffoldTool *tool, glong point)
{
	static glong bol;
	gchar *ch;

	for (bol = point; bol > 0; bol--) {
		ch = scaffold_get_document_chars (tool, bol, bol - 1); 
		if (ch[0] == '\n')
			break;
	}

	if (bol < 0) {
		bol = 0;
	} else if (bol > scaffold_get_document_length (tool)) {
		bol = scaffold_get_document_length (tool);
	}

	return bol;
}

void
text_delete_to_bol (BonoboUIComponent *uic, ScaffoldTool *tool)
{
	scaffold_delete_text (tool,
			      scaffold_get_cursor_pos (tool),
			      get_begin_of_line (tool,
						 scaffold_get_cursor_pos (tool)));
}

void
text_delete_to_eol (BonoboUIComponent *uic, ScaffoldTool *tool)
{
	scaffold_delete_text (tool,
			      scaffold_get_cursor_pos (tool),
			      get_end_of_line (tool,
					       scaffold_get_cursor_pos (tool)));
}
