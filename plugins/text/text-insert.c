/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * text-insert.c
 * 
 * Copyright (C) 2003 JP Rosevear (jpr@ximian.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <errno.h>
#include <stdio.h>
#include <time.h>
#include <bonobo/bonobo-file-selector-util.h>
#include <libgnome/gnome-i18n.h>
#include <libscaffold/libscaffold.h>
#include "text-insert.h"

#define STRLEN 255

void
text_insert_date (BonoboUIComponent *uic, ScaffoldTool *tool)
{
	time_t t;
	struct tm *st;
	gchar *timestr;

	time (&t);
	st = localtime (&t);
	timestr = asctime (st);

	scaffold_insert_text_at_cursor (tool, timestr);
}

void
text_insert_gpl_c (BonoboUIComponent *uic, ScaffoldTool *tool)
{
	static gchar *GPL_Notice =
	    "/* Program Name\n"
	    " * Copyright (C) 1999 Author Name\n"
	    " *\n"
	    " * This program is free software; you can redistribute it and/or modify\n"
	    " * it under the terms of the GNU General Public License as published by\n"
	    " * the Free Software Foundation; either version 2 of the License, or\n"
	    " * (at your option) any later version.\n"
	    " *\n"
	    " * This program is distributed in the hope that it will be useful,\n"
	    " * but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
	    " * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
	    " * GNU General Public License for more details.\n"
	    " *\n"
	    " * You should have received a copy of the GNU General Public License\n"
	    " * along with this program; if not, write to the Free Software\n"
	    " * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.\n"
	    " */\n"
	    "\n";

	scaffold_insert_text_at_cursor (tool, GPL_Notice);
}

void
text_insert_gpl_cpp (BonoboUIComponent *uic, ScaffoldTool *tool)
{
	static gchar *GPL_Notice =
	    "// Program Name\n"
	    "// Copyright (C) 1999 Author Name\n"
	    "//\n"
	    "// This program is free software; you can redistribute it and/or modify\n"
	    "// it under the terms of the GNU General Public License as published by\n"
	    "// the Free Software Foundation; either version 2 of the License, or\n"
	    "// (at your option) any later version.\n"
	    "//\n"
	    "// This program is distributed in the hope that it will be useful,\n"
	    "// but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
	    "// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
	    "// GNU General Public License for more details.\n"
	    "//\n"
	    "// You should have received a copy of the GNU General Public License\n"
	    "// along with this program; if not, write to the Free Software\n"
	    "// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.\n"
	    "\n";

	scaffold_insert_text_at_cursor (tool, GPL_Notice);
}

void
text_insert_file (BonoboUIComponent *uic, ScaffoldTool *tool)
{
	FILE *f;
	gchar *filename;
	gchar buf[STRLEN];
	
	filename = bonobo_file_selector_open (GTK_WINDOW (tool->shell), FALSE,
					      _("Insert File..."), NULL, NULL);
	if (filename) {
		f = fopen(filename, "r");
		if (!f) {
			g_snprintf (buf, sizeof (buf), _("File Error: %s"),
				    strerror(errno));
		        scaffold_dialog_error (buf);
			g_free (filename);
			return;
		}

		while (fgets (buf, sizeof (buf), f)) {
			scaffold_insert_text_at_cursor (tool, buf);
		}

		fclose (f);
		g_free (filename);
	}
}
