/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * text-replace.c
 * 
 * Copyright (C) 2003 JP Rosevear (jpr@ximian.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <libgnome/gnome-i18n.h>
#include <libscaffold/libscaffold.h>
#include <glade/glade-xml.h>
#include <gtk/gtkentry.h>
#include "text-replace.h"

static void
replace_spaces_with_tabs (ScaffoldTool *tool, const char *str)
{
	gint i;
	gchar *c;
	glong tabsize;
	gint column = 1;
	gchar *filler;
	gint numspaces;

	if (!str || scaffold_is_empty_string (str))
		return;

	tabsize = atol (str);

	for (i = 1; i <= scaffold_get_document_length (tool); i++) {
		c = scaffold_get_document_chars (tool, i - 1, i);
		if (*c == '\n') {
			column = 1;
		} else if (*c == '\t') {
			scaffold_delete_text (tool, i - 1, i);
			numspaces = ((column + tabsize - 1) / tabsize) *
				    tabsize + 1 - column;

			filler = g_strnfill (numspaces, ' ');
			scaffold_insert_text_at_pos (tool, i - 1, filler);
			g_free (filler);

			i += numspaces - 1;
			column += numspaces;
		} else {
			column++;
		}
		g_free (c);
	}
}

void
text_replace_tab_spaces (BonoboUIComponent *uic, ScaffoldTool *tool)
{
	GladeXML *gui;
	GtkWidget *dialog, *spaces_entry;
	GtkResponseType response;

	gui = glade_xml_new (GLADEDIR "scaffold-text.glade",
			     "convert-spaces-dialog", NULL);

	if (!gui) {
		g_warning ("Could not load scaffold-text.glade, reinstall scaffold");
		return;
	}

	dialog = glade_xml_get_widget (gui, "convert-spaces-dialog");
	spaces_entry = glade_xml_get_widget (gui, "spaces-entry");
	g_object_unref (gui);

	gtk_window_set_transient_for (GTK_WINDOW (dialog),
				      GTK_WINDOW (tool->shell));
	gtk_widget_show_all (dialog);
	response = gtk_dialog_run (GTK_DIALOG (dialog));
	if (response == GTK_RESPONSE_OK) {
		g_warning ("FIXME: add a proper EditorBuffer interface for doing this");
		/*const char *value = gtk_entry_get_text (GTK_ENTRY (spaces_entry));
		replace_spaces_with_tabs (tool, value);*/
	}

	gtk_widget_destroy (dialog);
}
