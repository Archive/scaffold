/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * text-replace.h
 * 
 * Copyright (C) 2003 JP Rosevear (jpr@ximian.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __PLUGIN_TEXT_REPLACE_H__
#define __PLUGIN_TEXT_REPLACE_H__

#include <bonobo/bonobo-ui-component.h>
#include <libscaffold/scaffold-tool.h>

G_BEGIN_DECLS

void text_replace_tab_spaces (BonoboUIComponent *uic, ScaffoldTool *tool);

G_END_DECLS

#endif /* __PLUGIN_TEXT_REPLACE_H__ */
