/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * text-tool.c
 * 
 * Copyright (C) 2003 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <libscaffold/libscaffold.h>
#include "text-insert.h"
#include "text-delete.h"
#include "text-replace.h"
#include "text-trim.h"

typedef struct {
	ScaffoldTool parent;
} TextTool;

typedef struct {
	ScaffoldToolClass parent;
} TextToolClass;

static BonoboUIVerb verbs[] = {
	BONOBO_UI_UNSAFE_VERB ("TextInsertDateTime", text_insert_date),
	BONOBO_UI_UNSAFE_VERB ("TextInsertGplC", text_insert_gpl_c),
	BONOBO_UI_UNSAFE_VERB ("TextInsertGplCpp", text_insert_gpl_cpp),
	BONOBO_UI_UNSAFE_VERB ("TextInsertFile", text_insert_file),
	BONOBO_UI_UNSAFE_VERB ("TextDeleteBOF", text_delete_to_bof),
	BONOBO_UI_UNSAFE_VERB ("TextDeleteEOF", text_delete_to_eof),
	BONOBO_UI_UNSAFE_VERB ("TextDeleteBOL", text_delete_to_bol),
	BONOBO_UI_UNSAFE_VERB ("TextDeleteEOL", text_delete_to_eol),
	BONOBO_UI_UNSAFE_VERB ("TextReplaceTabs", text_replace_tab_spaces),
	BONOBO_UI_UNSAFE_VERB ("TextTrimTrailingSpaces", text_trim_trailing_spaces),
	BONOBO_UI_VERB_END
};

static void
shell_set (ScaffoldTool *tool)
{
	scaffold_tool_merge_ui (tool,
				"scaffold-text-tool",
				SCAFFOLD_DATADIR,
				"scaffold-text-plugin.xml",
				verbs, tool);
}

static void
dispose (GObject *obj)
{
	scaffold_tool_unmerge_ui (SCAFFOLD_TOOL (obj));
}

static void
text_tool_instance_init (GObject *object)
{
}

static void
text_tool_class_init (GObjectClass *klass)
{
	ScaffoldToolClass *tool_class = SCAFFOLD_TOOL_CLASS (klass);
	
	tool_class->shell_set = shell_set;
	klass->dispose = dispose;
}

SCAFFOLD_TOOL_BOILERPLATE (TextTool, text_tool);

SCAFFOLD_SIMPLE_PLUGIN (TextTool, text_tool);
