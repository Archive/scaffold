/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * text-trim.c
 * 
 * Copyright (C) 2003 Rui Lopes <rui@ruilopes.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <libscaffold/libscaffold.h>
#include "text-trim.h"

void
text_trim_trailing_spaces (BonoboUIComponent *uic, ScaffoldTool *tool)
{
	long space_pos;
	long pos;
	long length;

	/* TODO: If there is a selection, trim only the lines on that selection. */

	/* Trim trailing spaces from all document lines */
	space_pos = -1;
	length = scaffold_get_document_length (tool);
	for (pos = 0; pos < length; ++pos) {
		char *ch = scaffold_get_document_chars (tool, pos, pos + 1);

		if (*ch == '\n') {
			if (space_pos >= 0) { /* only trim if needed */				scaffold_delete_text (tool, space_pos, pos);
				space_pos = -1;
				length = scaffold_get_document_length (tool);
			}
		} else if (g_ascii_isspace (*ch)) {
			/* only update space_pos if the last character was a
			 * non-space */
			if (space_pos < 0)
				space_pos = pos;
		} else {
			space_pos = -1;
		}

		g_free (ch);
	}
}
