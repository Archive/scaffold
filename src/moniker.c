/* Scaffold moniker: a moniker for simplifying access the scaffold object model 
 * 
 * Author:
 *    Dave Camp <dave@ximian.com>
 * 
 * Copyright 2001 Dave Camp
 */

#include <bonobo/bonobo-exception.h>
#include <bonobo/bonobo-moniker-util.h>

#include "moniker.h"
#include <shell.h>
#include <gdl/gdl.h>

#include <string.h>

extern gboolean dogfood_mode;
extern char *scaffold_moniker_prefix;

static Bonobo_Unknown
resolve (BonoboMoniker *moniker,
	 const Bonobo_ResolveOptions *options,
	 const CORBA_char *requested_interface,
	 CORBA_Environment *ev)
{
	ScaffoldShell *shell;
	
	shell = scaffold_get_shell (bonobo_moniker_get_name_full (moniker));

	if (!shell) {
		return CORBA_OBJECT_NIL;
	}

	if (!strcmp (requested_interface, "IDL:GNOME/Development/Shell:1.0")) {		
		return bonobo_object_dup_ref (BONOBO_OBJREF (shell), ev);
	} else if (!strcmp (requested_interface, "IDL:Bonobo/ItemContainer:1.0")) {
		return bonobo_object_dup_ref (BONOBO_OBJREF (shell->item_handler), ev);
	} else if (!strcmp (requested_interface, "IDL:Bonobo/EventSource:1.0")) {
		return bonobo_object_dup_ref (BONOBO_OBJREF (shell->event_source), ev);
	}
	return CORBA_OBJECT_NIL;
}

static BonoboObject *
moniker_factory (BonoboGenericFactory *fact,
		 const char *object_id,
		 void *data)
{
	return BONOBO_OBJECT (bonobo_moniker_simple_new
			      (scaffold_moniker_prefix, resolve));
}

void
scaffold_moniker_init (void)
{
	BonoboGenericFactory *factory;

	if (!dogfood_mode) {
		factory = bonobo_generic_factory_new ("OAFIID:Bonobo_Moniker_scaffold_Factory", moniker_factory, NULL);
	} else {
		factory = bonobo_generic_factory_new ("OAFIID:Bonobo_Moniker_scaffold_dogfood_Factory", moniker_factory, NULL);
	}
}
