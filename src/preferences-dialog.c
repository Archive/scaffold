/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * preferences-dialog.c
 * 
 * Copyright (C) 2002 Dave Camp
 * Copyright (C) 2003 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <gtk/gtk.h>
#include <libgnome/gnome-i18n.h>
#include <libgnome/gnome-macros.h>
#include <glade/glade-xml.h>
#include "preferences-dialog.h"

enum {
	COL_CATEGORY,
	COL_POSITION,
	COL_WIDGET,
	LAST_COL
};

struct _ScaffoldPreferencesDialogPrivate {
	GtkWidget *treeview;
	GtkTreeStore *store;
	GtkWidget *notebook;
};

GNOME_CLASS_BOILERPLATE (ScaffoldPreferencesDialog, 
			 scaffold_preferences_dialog,
			 GtkDialog, GTK_TYPE_DIALOG);

static void
scaffold_preferences_dialog_dispose (GObject *object)
{
	ScaffoldPreferencesDialog *dlg = SCAFFOLD_PREFERENCES_DIALOG (object);

	if (dlg->priv->store) {
		g_object_unref (dlg->priv->store);
		dlg->priv->store = NULL;
	}
}

static void
scaffold_preferences_dialog_finalize (GObject *object)
{
	ScaffoldPreferencesDialog *dlg = SCAFFOLD_PREFERENCES_DIALOG (object);	

	if (dlg->priv) {
		g_free (dlg->priv);
		dlg->priv = NULL;
	}

	GNOME_CALL_PARENT (G_OBJECT_CLASS, finalize, (object));
}

static void
scaffold_preferences_dialog_response (GtkDialog *dialog, int response_id)
{
	switch (response_id) {
		case GTK_RESPONSE_HELP:
			g_warning ("FIXME: Implement help");
			break;
		default:
			gtk_widget_hide (GTK_WIDGET (dialog));
			break;
	}
}

static void
scaffold_preferences_dialog_class_init (ScaffoldPreferencesDialogClass *class)
{
	GObjectClass *object_class = G_OBJECT_CLASS (class);
	GtkDialogClass *dialog_class = GTK_DIALOG_CLASS (class);

	parent_class = g_type_class_peek_parent (class);
	
	object_class->dispose = scaffold_preferences_dialog_dispose;
	object_class->finalize = scaffold_preferences_dialog_finalize;

	dialog_class->response = scaffold_preferences_dialog_response;
}

static gboolean
delete_event_cb (GtkWidget *dialog, gpointer data)
{
	gtk_widget_hide (GTK_WIDGET (dialog));

	return TRUE;
}

static void
category_set_text (GtkTreeViewColumn *tree_column,
		   GtkCellRenderer *cell,
		   GtkTreeModel *model,
		   GtkTreeIter *iter,
		   gpointer data)
{
	char *category;

	gtk_tree_model_get (model, iter, COL_CATEGORY, &category, -1);
	g_object_set (GTK_CELL_RENDERER (cell), "text", category, NULL);
	g_free (category);
}

static void
category_selection_cb (GtkTreeSelection *treesel,
		       gpointer user_data)
{
	ScaffoldPreferencesDialog *dlg = SCAFFOLD_PREFERENCES_DIALOG (user_data);
	GtkTreeIter iter;
	GtkWidget *page;
	int tab_pos;

	if (gtk_tree_selection_get_selected (treesel, NULL, &iter)) {
		gtk_tree_model_get (GTK_TREE_MODEL (dlg->priv->store), &iter, 
				    COL_WIDGET, &page, COL_POSITION, &tab_pos, -1);
		if (page == NULL)
			return;

		gtk_notebook_set_current_page (GTK_NOTEBOOK (dlg->priv->notebook),
					       tab_pos);
	}
}

static void
scaffold_preferences_dialog_instance_init (ScaffoldPreferencesDialog *dlg)
{
	GladeXML *gui;
	GtkWidget *widget;
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;

	dlg->priv = g_new0 (ScaffoldPreferencesDialogPrivate, 1);

	gui = glade_xml_new (GLADEDIR "scaffold.glade", "prefs-hbox", NULL);

	if (!gui) {
		g_warning ("Could not load scaffold.glade, reinstall scaffold");
		return;
	}

	widget = glade_xml_get_widget (gui, "prefs-hbox");
	dlg->priv->treeview = glade_xml_get_widget (gui, "prefs-tree");
	dlg->priv->notebook = glade_xml_get_widget (gui, "notebook");
	g_object_unref (gui);

	gtk_widget_set_size_request (dlg->priv->treeview, 160, 240);
	dlg->priv->store = gtk_tree_store_new (LAST_COL, G_TYPE_STRING, 
					       G_TYPE_INT, G_TYPE_POINTER);
	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (dlg->priv->store),
					      COL_CATEGORY, GTK_SORT_ASCENDING);
	gtk_tree_view_set_model (GTK_TREE_VIEW (dlg->priv->treeview),
				 GTK_TREE_MODEL (dlg->priv->store));

	column = gtk_tree_view_column_new ();
	gtk_tree_view_column_set_title (column, "Categories");
	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (column, renderer, TRUE);
	gtk_tree_view_column_set_cell_data_func (column, renderer,
						 category_set_text,
						 NULL, NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (dlg->priv->treeview), column);

	g_signal_connect (G_OBJECT (gtk_tree_view_get_selection (GTK_TREE_VIEW (dlg->priv->treeview))),
			  "changed", G_CALLBACK (category_selection_cb), dlg);
	g_signal_connect (G_OBJECT (dlg), "delete_event", 
			  G_CALLBACK (delete_event_cb), NULL);

	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dlg)->vbox), widget,
			    TRUE, TRUE, 0);

	gtk_window_set_title (GTK_WINDOW (dlg), _("Preferences"));
	gtk_dialog_add_button (GTK_DIALOG (dlg), GTK_STOCK_CLOSE, 
			       GTK_RESPONSE_CLOSE);
	gtk_dialog_add_button (GTK_DIALOG (dlg), GTK_STOCK_HELP,
			       GTK_RESPONSE_HELP);
	gtk_dialog_set_has_separator (GTK_DIALOG (dlg), FALSE);
	gtk_container_set_border_width (GTK_CONTAINER (dlg), 5);
	gtk_box_set_spacing (GTK_BOX (GTK_DIALOG (dlg)->vbox), 2);
}

void
scaffold_preferences_dialog_add_page (ScaffoldPreferencesDialog *dlg,
				      const char *category,
				      const char *name,
				      GtkWidget  *page)
{
	GtkWidget *vbox;
	GtkTreeIter root, iter;
	char *cat;
	int page_num;

	g_return_if_fail (SCAFFOLD_IS_PREFERENCES_DIALOG (dlg));	
	g_return_if_fail (category != NULL);
	g_return_if_fail (name != NULL);
	g_return_if_fail (GTK_IS_WIDGET (page));

	page_num = gtk_notebook_get_n_pages (GTK_NOTEBOOK (dlg->priv->notebook));

	vbox = gtk_vbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (vbox), page, TRUE, TRUE, 0);
	gtk_widget_show (page);
	gtk_widget_show (vbox);
	gtk_notebook_append_page (GTK_NOTEBOOK (dlg->priv->notebook), vbox, NULL);

	/* Check to see if the category already exists and if not create a new one. */
	if (gtk_tree_model_get_iter_first (GTK_TREE_MODEL (dlg->priv->store),
					   &root)) {
		gtk_tree_model_get (GTK_TREE_MODEL (dlg->priv->store),
				    &root, COL_CATEGORY, &cat, -1);
		if (!strcmp (cat, category)) {
			gtk_tree_store_append (dlg->priv->store, &iter, &root);
			gtk_tree_store_set (dlg->priv->store, &iter,
					    COL_CATEGORY, name,
					    COL_POSITION, page_num,
					    COL_WIDGET, vbox,
					    -1);
		} else if (!gtk_tree_model_iter_next (GTK_TREE_MODEL (dlg->priv->store),
						      &root)) {
			gtk_tree_store_append (dlg->priv->store, &iter, NULL);
			gtk_tree_store_set (dlg->priv->store, &iter,
					    COL_CATEGORY, category,
					    -1);
			gtk_tree_store_append (dlg->priv->store, &iter, &iter);
			gtk_tree_store_set (dlg->priv->store, &iter,
					    COL_CATEGORY, name,
					    COL_POSITION, page_num,
					    COL_WIDGET, vbox,
					    -1);
		}

		g_free (cat);
	} else {
		gtk_tree_store_append (dlg->priv->store, &iter, NULL);
		gtk_tree_store_set (dlg->priv->store, &iter,
				    COL_CATEGORY, category,
				    -1);
		gtk_tree_store_append (dlg->priv->store, &iter, &iter);
		gtk_tree_store_set (dlg->priv->store, &iter,
				    COL_CATEGORY, name,
				    COL_POSITION, 0,
				    COL_WIDGET, vbox,
				    -1);
	}
}

void
scaffold_preferences_dialog_remove_page (ScaffoldPreferencesDialog *dlg,
				         const char *category,
				         const char *name)
{
	GtkTreeModel *model;
	GtkTreeIter iter, child;
	char *cat, *nam;
	GtkWidget *vbox;

	g_return_if_fail (SCAFFOLD_IS_PREFERENCES_DIALOG (dlg));
	g_return_if_fail (category != NULL);
	g_return_if_fail (name != NULL);

	model = GTK_TREE_MODEL (dlg->priv->store);

	if (gtk_tree_model_get_iter_first (model, &iter)) {
		gtk_tree_model_get (model, &iter, COL_CATEGORY, &cat, -1);
		if (!strcmp (cat, category)) {
			if (gtk_tree_model_iter_children (model, &child, &iter)) {
				do {
					gtk_tree_model_get (model, &child,
							    COL_CATEGORY, &nam,
							    COL_WIDGET, &vbox, -1);
					if (!strcmp (name, nam)) {
						gtk_container_remove (
							GTK_CONTAINER (dlg->priv->notebook),
							vbox);
						gtk_tree_store_remove (dlg->priv->store,
								       &child);
						g_free (nam);
						break;
					}
					g_free (nam);
				} while (gtk_tree_model_iter_next (model, &child));
			}		
		}
		g_free (cat);
	} else {
		g_warning ("Could not remove preference page '%s,%s': page not found",
			   category, name);
	}
}

void
scaffold_preferences_dialog_show_page (ScaffoldPreferencesDialog *dlg,
				       const char *category,
				       const char *name)
{
	GtkTreeModel *model;
	GtkTreeIter iter;
	char *cat;
	GtkTreePath *path;

	g_return_if_fail (SCAFFOLD_IS_PREFERENCES_DIALOG (dlg));	
	g_return_if_fail (category != NULL);
	g_return_if_fail (name != NULL);

	model = GTK_TREE_MODEL (dlg->priv->store);

	gtk_tree_view_collapse_all (GTK_TREE_VIEW (dlg->priv->treeview));

	/* Check to see if the category already exists and if not create a new one. */
	if (gtk_tree_model_get_iter_first (model, &iter)) {
		gtk_tree_model_get (model, &iter, COL_CATEGORY, &cat, -1);
		if (!strcmp (cat, category)) {
			path = gtk_tree_model_get_path (model, &iter);
			gtk_tree_view_expand_row (GTK_TREE_VIEW (dlg->priv->treeview),
						  path, FALSE);
			gtk_tree_path_free (path);

			/*if (gtk_tree_model_iter_children (model, &child, &iter)) {
				do {
					gtk_tree_model_get (model, &child,
							    COL_CATEGORY, &nam,
							    -1);
					if (!strcmp (name, nam)) {
						gtk_tree_selection_select_path (
						break;
					}
					g_free (nam);
				} while (gtk_tree_model_iter_next (model, &child));
			}*/
		}

		g_free (cat);
	}
}
