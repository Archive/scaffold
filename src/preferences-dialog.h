/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * preferences-dialog.h
 * 
 * Copyright (C) 2002 Dave Camp
 *
 * Copyright (C) 2003 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __SCAFFOLD_PREFERENCES_DIALOG_H__
#define __SCAFFOLD_PREFERENCES_DIALOG_H__

#include <gtk/gtkdialog.h>

G_BEGIN_DECLS

#define SCAFFOLD_TYPE_PREFERENCES_DIALOG        (scaffold_preferences_dialog_get_type ())
#define SCAFFOLD_PREFERENCES_DIALOG(o)          (G_TYPE_CHECK_INSTANCE_CAST ((o), SCAFFOLD_TYPE_PREFERENCES_DIALOG, ScaffoldPreferencesDialog))
#define SCAFFOLD_PREFERENCES_DIALOG_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST((k), SCAFFOLD_TYPE_PREFERENCES_DIALOG, ScaffoldPreferencesDialogClass))
#define SCAFFOLD_IS_PREFERENCES_DIALOG(o)       (G_TYPE_CHECK_INSTANCE_TYPE ((o), SCAFFOLD_TYPE_PREFERENCES_DIALOG))
#define SCAFFOLD_IS_PREFERENCES_DIALOG_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), SCAFFOLD_TYPE_PREFERENCES_DIALOG))

typedef struct _ScaffoldPreferencesDialog        ScaffoldPreferencesDialog;
typedef struct _ScaffoldPreferencesDialogClass   ScaffoldPreferencesDialogClass;
typedef struct _ScaffoldPreferencesDialogPrivate ScaffoldPreferencesDialogPrivate;

struct _ScaffoldPreferencesDialog {
	GtkDialog parent;
	
	ScaffoldPreferencesDialogPrivate *priv;
};

struct _ScaffoldPreferencesDialogClass {
	GtkDialogClass parent;
};

GType      scaffold_preferences_dialog_get_type    (void);

void       scaffold_preferences_dialog_add_page	   (ScaffoldPreferencesDialog *dlg,
						    const char *category,
						    const char *name,
						    GtkWidget  *page);
void       scaffold_preferences_dialog_remove_page (ScaffoldPreferencesDialog *dlg,
						    const char *category,
						    const char *name);
void       scaffold_preferences_dialog_show_page   (ScaffoldPreferencesDialog *dlg,
						    const char *category,
						    const char *name);

G_END_DECLS

#endif /* __SCAFFOLD_PREFERENCES_DIALOG_H__ */
