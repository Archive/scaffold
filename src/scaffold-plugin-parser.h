/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * scaffold-plugin-parser.h
 * 
 * Copyright (C) 2002 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __SCAFFOLD_PLUGIN_PARSER_H__
#define __SCAFFOLD_PLUGIN_PARSER_H__

#include <glib.h>

G_BEGIN_DECLS

typedef struct _ScaffoldPluginFile ScaffoldPluginFile;

typedef void (* ScaffoldPluginFileSectionFunc) (ScaffoldPluginFile *df,
						const char         *name,
						gpointer            data);

/* If @key is %NULL, @value is a comment line. */
/* @value is raw, unescaped data. */
typedef void (* ScaffoldPluginFileLineFunc) (ScaffoldPluginFile *df,
					     const char         *key,
					     const char         *locale,
					     const char         *value,
					     gpointer            data);

typedef enum  {
	SCAFFOLD_PLUGIN_FILE_PARSE_ERROR_INVALID_SYNTAX,
	SCAFFOLD_PLUGIN_FILE_PARSE_ERROR_INVALID_ESCAPES,
	SCAFFOLD_PLUGIN_FILE_PARSE_ERROR_INVALID_CHARS
} ScaffoldPluginFileParseError;

#define SCAFFOLD_PLUGIN_FILE_PARSE_ERROR scaffold_plugin_file_parse_error_quark()

GQuark              scaffold_plugin_file_parse_error_quark (void);

ScaffoldPluginFile *scaffold_plugin_file_new_from_string (char                         *data,
							  GError                      **error);
char               *scaffold_plugin_file_to_string       (ScaffoldPluginFile           *df);
void                scaffold_plugin_file_free            (ScaffoldPluginFile           *df);


void                scaffold_plugin_file_foreach_section (ScaffoldPluginFile           *df,
							  ScaffoldPluginFileSectionFunc func,
							  gpointer                      user_data);
void                scaffold_plugin_file_foreach_key     (ScaffoldPluginFile           *df,
							  const char                   *section,
							  gboolean                      include_localized,
							  ScaffoldPluginFileLineFunc    func,
							  gpointer                      user_data);


/* Gets the raw text of the key, unescaped */
gboolean scaffold_plugin_file_get_raw           (ScaffoldPluginFile *df,
						 const char         *section,
						 const char         *keyname,
						 const char         *locale,
						 char              **val);
gboolean scaffold_plugin_file_get_integer       (ScaffoldPluginFile *df,
						 const char         *section,
						 const char         *keyname,
						 int                *val);
gboolean scaffold_plugin_file_get_string        (ScaffoldPluginFile *df,
						 const char         *section,
						 const char         *keyname,
						 char              **val);
gboolean scaffold_plugin_file_get_locale_string (ScaffoldPluginFile *df,
						 const char         *section,
						 const char         *keyname,
						 char              **val);

G_END_DECLS

#endif /* __SCAFFOLD_PLUGIN_PARSER_H__ */
