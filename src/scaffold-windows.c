/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * scaffold-windows.c
 * 
 * Copyright (C) 1998-2000 Steffen Kern
 * Copyright (C) 2001, 2002 Dave Camp
 * Copyright (C) 2003 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glade/glade-xml.h>
#include <gconf/gconf-client.h>
#include "scaffold-windows.h"
#include "tools.h"

#define SCAFFOLD_WINDOW_STATE_PREFIX "/apps/scaffold/state"
#define SCAFFOLD_WINDOW_STATE_LAYOUT SCAFFOLD_WINDOW_STATE_PREFIX "/layout"

static GList *window_list = NULL;

static GtkWidget *
get_dock_preferences (ScaffoldWindow *window)
{
	GladeXML *gui;
	GtkWidget *vbox, *hbox, *image, *dock_items, *layout_hbox, *layouts;

	gui = glade_xml_new (GLADEDIR "scaffold.glade", "dock-preferences", NULL);

	if (!gui) {
		g_warning ("Could not load scaffold.glade, reinstall scaffold");
		return NULL;
	}

	vbox = glade_xml_get_widget (gui, "dock-preferences");
	hbox = glade_xml_get_widget (gui, "dockitems-hbox");
	image = glade_xml_get_widget (gui, "dock-image");
	layout_hbox = glade_xml_get_widget (gui, "layouts-hbox");
	g_object_unref (gui);

	gtk_image_set_from_file (GTK_IMAGE (image), SCAFFOLD_IMAGES "/dock.png");

	dock_items = gdl_dock_layout_get_items_ui (window->layout_manager);
	gtk_box_pack_end (GTK_BOX (hbox), dock_items, TRUE, TRUE, 0);

	layouts = gdl_dock_layout_get_layouts_ui (window->layout_manager);
	gtk_box_pack_end (GTK_BOX (layout_hbox), layouts, TRUE, TRUE, 0);

	gtk_widget_show_all (vbox);

	return vbox;
}

static gboolean
scaffold_window_close (ScaffoldWindow *window, gpointer data)
{
	/* Save session before unloading any plugins. */
	g_signal_emit_by_name (window, "session_save", window);

	/* Check if there are plugins which don't want to quit (unsaved data). */
	if (!scaffold_tool_unload (window))
		return FALSE;

	scaffold_shell_remove_value (SCAFFOLD_SHELL (window),
				     "Window::Preferences", NULL);

	/* Remove the window from the window list & destroy it. */
	window_list = g_list_remove (window_list, window);
	gtk_widget_destroy (GTK_WIDGET (window));

	/* Quit if no more windows left. */
	if (g_list_length (window_list) == 0) {
		g_list_free (window_list);
		gtk_main_quit ();
	}

	return TRUE;
}

static gboolean
scaffold_window_delete (ScaffoldWindow *window, gpointer data)
{
	return !scaffold_window_close (window, data);
}

static void
scaffold_window_quit (ScaffoldWindow *window, gpointer data)
{
	GList *l;

	/* Save session before unloading any plugins. */
	g_signal_emit_by_name (window, "session_save", window);
	
	/* Check if there are plugins which don't want to quit (unsaved data). */
	if (!scaffold_tool_unload (window))
		return;

	scaffold_shell_remove_value (SCAFFOLD_SHELL (window),
				     "Window::Preferences", NULL);

	/* Remove the window from the window list & destroy it. */
	window_list = g_list_remove (window_list, window);
	gtk_widget_destroy (GTK_WIDGET (window));

	/* Do the same for the other windows. */
	for (l = window_list; l != NULL; l = l->next) {
		window = SCAFFOLD_WINDOW (l->data);
		
		if (!scaffold_tool_unload (window))
			return;

		gtk_widget_destroy (GTK_WIDGET (window));
	}

	g_list_free (window_list);
	gtk_main_quit ();
}

static void
scaffold_window_new_window (ScaffoldWindow *window, gpointer data)
{
	ScaffoldWindow *new_wnd;
	GtkWidget *dock_prefs;

	new_wnd = SCAFFOLD_WINDOW (scaffold_window_new ());
	window_list = g_list_append (window_list, new_wnd);

	scaffold_tool_set_load (new_wnd, NULL, "default");

	if (window->layout)
		scaffold_window_load_layout (new_wnd, window->layout);
	else
		scaffold_window_load_layout (new_wnd, "__default__");

	/* Add dock preferences. */
	dock_prefs = get_dock_preferences (new_wnd);
	scaffold_shell_add_preferences (SCAFFOLD_SHELL (new_wnd), dock_prefs,
				        "Window::Preferences", _("General"),
				        _("Dock"), NULL);

	g_signal_connect (G_OBJECT (new_wnd), "new_window",
			  G_CALLBACK (scaffold_window_new_window), NULL);
	g_signal_connect (G_OBJECT (new_wnd), "close_window",
			  G_CALLBACK (scaffold_window_close), NULL);
	g_signal_connect (G_OBJECT (new_wnd), "delete_event",
			  G_CALLBACK (scaffold_window_delete), NULL);
	g_signal_connect (G_OBJECT (new_wnd), "quit",
			  G_CALLBACK (scaffold_window_quit), NULL);

	gtk_widget_show (GTK_WIDGET (new_wnd));
}

ScaffoldWindow *
scaffold_new_window (ESplash *splash)
{
	ScaffoldWindow *window;
	GtkWidget *dock_prefs;
	GConfClient *client;
	gchar *layout;

	window = SCAFFOLD_WINDOW (scaffold_window_new ());
	window_list = g_list_append (window_list, window);

	scaffold_tool_set_load (window, splash, "default");

	/* Restore last layout. This needs to be done here, after the
	 * ScaffoldWindow has been fully initialized. Doing this from the
	 * instance_init method in ScaffoldWindow won't work (don't recall why
	 * exactly). */
	client = gconf_client_get_default ();
	layout = gconf_client_get_string (client,
					  SCAFFOLD_WINDOW_STATE_LAYOUT,
					  NULL);
	g_object_unref (G_OBJECT (client));
	if (!layout)
		layout = g_strdup ("__default__");
	scaffold_window_load_layout (SCAFFOLD_WINDOW (window), layout);
	g_free (layout);

	/* Add dock preferences. */
	dock_prefs = get_dock_preferences (window);
	scaffold_shell_add_preferences (SCAFFOLD_SHELL (window), dock_prefs,
				        "Window::Preferences", _("General"),
				        _("Dock"), NULL);

	g_signal_connect (G_OBJECT (window), "new_window",
			  G_CALLBACK (scaffold_window_new_window), NULL);
	g_signal_connect (G_OBJECT (window), "close_window",
			  G_CALLBACK (scaffold_window_close), NULL);
	g_signal_connect (G_OBJECT (window), "delete_event",
			  G_CALLBACK (scaffold_window_delete), NULL);
	g_signal_connect (G_OBJECT (window), "quit",
			  G_CALLBACK (scaffold_window_quit), NULL);

	gtk_widget_show (GTK_WIDGET (window));
	
	return window;
}

GList *
scaffold_get_all_windows (void)
{
	return g_list_copy (window_list);
}
