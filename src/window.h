/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- 
 * window.h
 * 
 * Copyright (C) 2001, 2002 Dave Camp
 * Copyright (C) 2003 Jeroen Zwartepoorte
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __SCAFFOLD_WINDOW_H__
#define __SCAFFOLD_WINDOW_H__

#include <bonobo.h>
#include <libscaffold/libscaffold.h>
#include <libxml/tree.h>
#include <gdl/gdl-dock-layout.h>
#include "preferences-dialog.h"

G_BEGIN_DECLS

#define SCAFFOLD_TYPE_WINDOW        (scaffold_window_get_type ())
#define SCAFFOLD_WINDOW(o)          (G_TYPE_CHECK_INSTANCE_CAST ((o), SCAFFOLD_TYPE_WINDOW, ScaffoldWindow))
#define SCAFFOLD_WINDOW_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST((k), SCAFFOLD_TYPE_WINDOW, ScaffoldWindowClass))
#define SCAFFOLD_IS_WINDOW(o)       (G_TYPE_CHECK_INSTANCE_TYPE ((o), SCAFFOLD_TYPE_WINDOW))
#define SCAFFOLD_IS_WINDOW_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), SCAFFOLD_TYPE_WINDOW))

typedef struct _ScaffoldWindow		ScaffoldWindow;
typedef struct _ScaffoldWindowClass	ScaffoldWindowClass;

struct _ScaffoldWindow {
	BonoboWindow app;

	BonoboUIComponent *uic;
	BonoboUIContainer *ui_container;

	GtkWidget *dock;
	GdlDockLayout *layout_manager;
	gchar *layout;

	GHashTable *values;
	GHashTable *widgets;
	GHashTable *preference_pages;

	ScaffoldSession *session;

	ScaffoldPreferencesDialog *prefs_dialog;
};

struct _ScaffoldWindowClass {
	BonoboWindowClass parent_class;

	/* Signals. */
	void (* close_window)                 (ScaffoldWindow *window);
	void (* new_window)                   (ScaffoldWindow *window);
	void (* quit)                         (ScaffoldWindow *window);
};

GType      scaffold_window_get_type	      (void);
GtkWidget *scaffold_window_new		      (void);

void       scaffold_window_save_layout	      (ScaffoldWindow *window,
					       const gchar    *name);
void       scaffold_window_load_layout	      (ScaffoldWindow *window,
					       const gchar    *name);

G_END_DECLS

#endif /* __SCAFFOLD_WINDOW_H__ */
